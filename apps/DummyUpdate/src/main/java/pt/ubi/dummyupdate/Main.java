package pt.ubi.dummyupdate;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import pt.ubi.dbentities.*;

import java.util.Scanner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.eclipse.persistence.sessions.Session;

public class Main {

    private static final String OS = System.getProperty("os.name").toLowerCase();
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        sc = new Scanner(System.in);

        System.out.println("> The program is started: ");
        System.out.println("> App DummyUpdate: " + OS + " " + dtf.format(now));
        System.out.println("> Working Directory = " + System.getProperty("user.dir"));
        System.out.println("> --------------------------------------------  ");
        
        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("pt.ubi_DummyUpdate");
            TableEntities tb;
            if (args.length==0)
                 tb = new TableEntities(emf);
            else{
                 Map<String, String> properties = properties = new HashMap<>();
                 String urlDb = "jdbc:postgresql://" + args[0];
                properties.put("javax.persistence.jdbc.url", urlDb);
                properties.put("javax.persistence.jdbc.user", args[1]);
                properties.put("javax.persistence.jdbc.password", args[2]);
               
                tb = new TableEntities(emf,properties );
            }

            EntityManager em =  tb.getEntityManager(); 
            em.getTransaction().begin();
            java.sql.Connection connection = em.unwrap(java.sql.Connection.class);
            try {
                DatabaseMetaData metaData = connection.getMetaData();
                System.out.println("url:"+metaData.getURL());
                System.out.println("username:"+metaData.getUserName());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            em.getTransaction().commit();
            
            System.out.println("Possible Record Updates with non zero Time Entries ");
            tb.PrintSizeLista(Condition.class);
            tb.PrintSizeLista(InstrumentCollocation.class);
            tb.PrintSizeLista(Station.class);
            tb.PrintSizeLista(StationItem.class);
            tb.PrintSizeLista(ItemAttribute.class);

            int opcao = 10;
            while (opcao > 0) {
                System.out.println("-----Updates-------------");
                System.out.println("Choose Table or 0 to EXIT");
                System.out.println("1 Condition");
                System.out.println("2 Instrument Collocation\n3 Station");
                System.out.println("4 Station Item\n5 Item Attribute");
                String linha = sc.nextLine();
                opcao = Integer.parseInt(linha);
                if (0 == opcao) {
                    break;
                }
                if (!areYouSure()) {
                    continue;
                }
                switch (opcao) {
                    case 1:
                        tb.DummyUpdateLista(Condition.class);
                        break;
                    case 2:
                        tb.DummyUpdateLista(InstrumentCollocation.class);
                        break;
                    case 3:
                        tb.DummyUpdateLista(Station.class);
                        break;
                    case 4:
                        tb.DummyUpdateLista(StationItem.class);
                        break;
                    case 5:
                        tb.DummyUpdateLista(ItemAttribute.class);
                        break;
                    default:
                        System.out.println("Option Invalid");
                }
            }
            emf.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }

    static boolean areYouSure() {
        System.out.println("Are You Sure  ? Type y/n");
        String res = sc.nextLine();
        System.out.println("Reply: <" + res + ">");
        return res.equals("y");
    }

    public static boolean isWindows() {
        return (OS.contains("win"));
    }

    public static boolean isMac() {
        return (OS.contains("mac"));
    }

    public static boolean isUnix() {
        return (OS.contains("nix") || OS.contains("nux") || OS.contains("aix"));
    }

    public static boolean isLinux() {
        return (OS.contains("linux"));
    }

    public static boolean isSolaris() {
        return (OS.contains("sunos"));
    }

    public static void persist(Object object) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("pt.ubi_DummyUpdate");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.persist(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

}

// List<Condition> results;
//     Condition c = new Condition();
//        ConditionJpaController conditionJpaController = new ConditionJpaController(emf);
//        results = conditionJpaController.findConditionEntities();
//        results.forEach((result) -> {
//            System.out.println(result.toString());
//        });
//        Query query = em.createNamedQuery("Condition.findAll");
//        results = query.getResultList();
//        results.forEach((result) -> {
//            System.out.println(result.toString());
//        });
//        Query q = em.createNativeQuery(MyConstants.MYQ2NOT, Condition.class);
//        results = q.getResultList();
//        
//        results.forEach( (Condition result) -> {
//            System.out.println(result.toString());
//        });
//
//        System.out.println("---------------------");
//        em.setFlushMode(FlushModeType.COMMIT);  //force changes ..dont work
// 
//        results.forEach((Condition result) -> {
//
//            System.out.println(result.toString());
//            //persist( (Object)result ) ;
//            result.setIdStation(result.getIdStation());
//   
//            //result.setComments(result.getComments()+" "); //this definitely triggers update
//            
//            em.getTransaction().begin();
//            try {
//                em.getTransaction().commit();
//            } catch (Exception e) {
//                System.out.println("e="+e.getMessage()+e.toString());
//                System.out.println("Bronca");
//                em.getTransaction().rollback();
//            }
//        });
//        results.forEach((Condition result) -> {
//
//            System.out.println(result.toString());
//
//            em.getTransaction().begin();
//         
//            // Names of the persistent attributes are case sensitive 
//            //in JPQL queries and those should be written with exactly 
//            //same case as they appear in entity.
//            String myQ3="UPDATE Condition SET idEffect = " +result.getIdEffect()+ " where id = " + result.getId();
//                
//            Query query = em.createQuery( myQ3 );
//            
//            try {
//                //em.persist(result);
//      
//                //query.setParameter(1, result.getId());
//                
//                System.out.println(query);
//                
//                int updateCount  = query.executeUpdate();
//                System.out.println("Executed: "+ updateCount );
//                
//                em.getTransaction().commit();
//            } catch (Exception e) {
//                System.out.println("e="+e.getMessage()+e.toString());
//                System.out.println("Bronca");
//                em.getTransaction().rollback();
//            }
//
//        });
