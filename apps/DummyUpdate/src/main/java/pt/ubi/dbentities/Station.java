/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ubi.dbentities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paul Crocker
 */
@Entity
@Table(name = "station")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Station.findAll", query = "SELECT s FROM Station s"),
    @NamedQuery(name = "Station.findById", query = "SELECT s FROM Station s WHERE s.id = :id"),
    @NamedQuery(name = "Station.findByName", query = "SELECT s FROM Station s WHERE s.name = :name"),
    @NamedQuery(name = "Station.findByMarker", query = "SELECT s FROM Station s WHERE s.marker = :marker"),
    @NamedQuery(name = "Station.findByDescription", query = "SELECT s FROM Station s WHERE s.description = :description"),
    @NamedQuery(name = "Station.findByDateFrom", query = "SELECT s FROM Station s WHERE s.dateFrom = :dateFrom"),
    @NamedQuery(name = "Station.findByDateTo", query = "SELECT s FROM Station s WHERE s.dateTo = :dateTo"),
    @NamedQuery(name = "Station.findByComment", query = "SELECT s FROM Station s WHERE s.comment = :comment"),
    @NamedQuery(name = "Station.findByIersDomes", query = "SELECT s FROM Station s WHERE s.iersDomes = :iersDomes"),
    @NamedQuery(name = "Station.findByCpdNum", query = "SELECT s FROM Station s WHERE s.cpdNum = :cpdNum"),
    @NamedQuery(name = "Station.findByMonumentNum", query = "SELECT s FROM Station s WHERE s.monumentNum = :monumentNum"),
    @NamedQuery(name = "Station.findByReceiverNum", query = "SELECT s FROM Station s WHERE s.receiverNum = :receiverNum"),
    @NamedQuery(name = "Station.findByCountryCode", query = "SELECT s FROM Station s WHERE s.countryCode = :countryCode"),
    @NamedQuery(name = "Station.findByIdGeological", query = "SELECT s FROM Station s WHERE s.idGeological = :idGeological"),
    @NamedQuery(name = "Station.findByIdLocation", query = "SELECT s FROM Station s WHERE s.idLocation = :idLocation"),
    @NamedQuery(name = "Station.findByIdMonument", query = "SELECT s FROM Station s WHERE s.idMonument = :idMonument"),
    @NamedQuery(name = "Station.findByIdStationType", query = "SELECT s FROM Station s WHERE s.idStationType = :idStationType")})
public class Station implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "marker")
    private String marker;
    @Column(name = "description")
    private String description;
    @Column(name = "date_from")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFrom;
    @Column(name = "date_to")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTo;
    @Column(name = "comment")
    private String comment;
    @Column(name = "iers_domes")
    private String iersDomes;
    @Column(name = "cpd_num")
    private String cpdNum;
    @Column(name = "monument_num")
    private Integer monumentNum;
    @Column(name = "receiver_num")
    private Integer receiverNum;
    @Column(name = "country_code")
    private String countryCode;
    @Column(name = "id_geological")
    private Integer idGeological;
    @Column(name = "id_location")
    private Integer idLocation;
    @Column(name = "id_monument")
    private Integer idMonument;
    @Column(name = "id_station_type")
    private Integer idStationType;

    public Station() {
    }

    public Station(Integer id) {
        this.id = id;
    }

    public Station(Integer id, String name, String marker) {
        this.id = id;
        this.name = name;
        this.marker = marker;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getIersDomes() {
        return iersDomes;
    }

    public void setIersDomes(String iersDomes) {
        this.iersDomes = iersDomes;
    }

    public String getCpdNum() {
        return cpdNum;
    }

    public void setCpdNum(String cpdNum) {
        this.cpdNum = cpdNum;
    }

    public Integer getMonumentNum() {
        return monumentNum;
    }

    public void setMonumentNum(Integer monumentNum) {
        this.monumentNum = monumentNum;
    }

    public Integer getReceiverNum() {
        return receiverNum;
    }

    public void setReceiverNum(Integer receiverNum) {
        this.receiverNum = receiverNum;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Integer getIdGeological() {
        return idGeological;
    }

    public void setIdGeological(Integer idGeological) {
        this.idGeological = idGeological;
    }

    public Integer getIdLocation() {
        return idLocation;
    }

    public void setIdLocation(Integer idLocation) {
        this.idLocation = idLocation;
    }

    public Integer getIdMonument() {
        return idMonument;
    }

    public void setIdMonument(Integer idMonument) {
        this.idMonument = idMonument;
    }

    public Integer getIdStationType() {
        return idStationType;
    }

    public void setIdStationType(Integer idStationType) {
        this.idStationType = idStationType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Station)) {
            return false;
        }
        Station other = (Station) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Station[ id=" + id + " ]";
    }
    
}
