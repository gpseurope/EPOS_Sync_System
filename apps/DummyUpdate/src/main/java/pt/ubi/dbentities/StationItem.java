/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ubi.dbentities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paul Crocker
 */
@Entity
@Table(name = "station_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StationItem.findAll", query = "SELECT s FROM StationItem s"),
    @NamedQuery(name = "StationItem.findById", query = "SELECT s FROM StationItem s WHERE s.id = :id"),
    @NamedQuery(name = "StationItem.findByDateFrom", query = "SELECT s FROM StationItem s WHERE s.dateFrom = :dateFrom"),
    @NamedQuery(name = "StationItem.findByDateTo", query = "SELECT s FROM StationItem s WHERE s.dateTo = :dateTo"),
    @NamedQuery(name = "StationItem.findByIdItem", query = "SELECT s FROM StationItem s WHERE s.idItem = :idItem"),
    @NamedQuery(name = "StationItem.findByIdStation", query = "SELECT s FROM StationItem s WHERE s.idStation = :idStation")})
public class StationItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "date_from")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFrom;
    @Column(name = "date_to")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTo;
    @Basic(optional = false)
    @Column(name = "id_item")
    private int idItem;
    @Basic(optional = false)
    @Column(name = "id_station")
    private int idStation;

    public StationItem() {
    }

    public StationItem(Integer id) {
        this.id = id;
    }

    public StationItem(Integer id, Date dateFrom, int idItem, int idStation) {
        this.id = id;
        this.dateFrom = dateFrom;
        this.idItem = idItem;
        this.idStation = idStation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    public int getIdStation() {
        return idStation;
    }

    public void setIdStation(int idStation) {
        this.idStation = idStation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StationItem)) {
            return false;
        }
        StationItem other = (StationItem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "StationItem[ id=" + id + " ]";
    }
    
}
