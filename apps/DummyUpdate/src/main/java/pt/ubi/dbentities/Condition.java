/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ubi.dbentities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paul Crocker
 */
@Entity
@Table(name = "condition")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Condition.findAll", query = "SELECT c FROM Condition c order by c.id"),
    @NamedQuery(name = "Condition.findById", query = "SELECT c FROM Condition c WHERE c.id = :id"),
    @NamedQuery(name = "Condition.findByDateFrom", query = "SELECT c FROM Condition c WHERE c.dateFrom = :dateFrom"),
    @NamedQuery(name = "Condition.findByDateTo", query = "SELECT c FROM Condition c WHERE c.dateTo = :dateTo"),
    @NamedQuery(name = "Condition.findByDegradation", query = "SELECT c FROM Condition c WHERE c.degradation = :degradation"),
    @NamedQuery(name = "Condition.findByComments", query = "SELECT c FROM Condition c WHERE c.comments = :comments"),
    @NamedQuery(name = "Condition.findByIdEffect", query = "SELECT c FROM Condition c WHERE c.idEffect = :idEffect"),
    @NamedQuery(name = "Condition.findByIdStation", query = "SELECT c FROM Condition c WHERE c.idStation = :idStation")})
public class Condition implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "date_from")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFrom;
    @Column(name = "date_to")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTo;
    @Column(name = "degradation")
    private String degradation;
    @Column(name = "comments")
    private String comments;
    @Basic(optional = false)
    @Column(name = "id_effect")
    private int idEffect;
    @Basic(optional = false)
    @Column(name = "id_station")
    private int idStation;

    public Condition() {
    }

    public Condition(Integer id) {
        this.id = id;
    }

    public Condition(Integer id, int idEffect, int idStation) {
        this.id = id;
        this.idEffect = idEffect;
        this.idStation = idStation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getDegradation() {
        return degradation;
    }

    public void setDegradation(String degradation) {
        this.degradation = degradation;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getIdEffect() {
        return idEffect;
    }

    public void setIdEffect(int idEffect) {
        this.idEffect = idEffect;
    }

    public int getIdStation() {
        return idStation;
    }

    public void setIdStation(int idStation) {
        this.idStation = idStation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Condition)) {
            return false;
        }
        Condition other = (Condition) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

//    @Override
//    public String toString() {
//        return "Condition{" + "id=" + id + ", dateFrom=<" + dateFrom + ">, dateTo=<" + dateTo + ">, degradation=" + degradation + ", comments=" + comments + ", idEffect=" + idEffect + ", idStation=" + idStation + '}';
//    }
  
    public String toString1() {
        return "Condition{" + "id=" + id + ", dateFrom=<" + dateFrom + ">, dateTo=<" + dateTo + ">, degradation=" + degradation + ", idEffect=" + idEffect + ", idStation=" + idStation + '}';
    }
    @Override
    public String toString() {
        return "Condition{" + "id=" + id + '}';
    }


}
