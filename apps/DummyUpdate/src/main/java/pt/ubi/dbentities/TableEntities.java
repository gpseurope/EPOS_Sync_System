package pt.ubi.dbentities;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Paul Crocker
 */
public class TableEntities {

    public final static String DUMMYCONDITION = "( ( ( c.date_from is null or (  date_part('hour',  c.date_from) = 0 "
            + "and date_part('minute',c.date_from) = 0 "
            + "and date_part('second',c.date_from) = 0 )) "
            + "and ( c.date_to is null or( date_part('hour',  c.date_to) = 0 "
            + "and date_part('minute',c.date_to) = 0 "
            + "and date_part('second',c.date_to) = 0 ) ) )) "
            + "order by c.id";

    public final static String SELECTER = "select  * from ";
    public final static String CAND = " c where ";
    public final static String CNOT = " c where not ";

    private EntityManagerFactory emf = null;
    private Map<String, String> properties=null;

    private int total = 0;

    private String lookup(String s) {
        if (s.equals("Condition")) {
            return s;
        }
        if (s.equals("Station")) {
            return "Station";
        }
        if (s.equals("InstrumentCollocation")) {
            return "Instrument_Collocation";
        }
        if (s.equals("ItemAttribute")) {
            return "Item_Attribute";
        }
        if (s.equals("StationItem")) {
            return "Station_Item";
        }

        System.out.println("BRONCAAAAAAAAAAAAAAAAA");
        return "";
    }

    public TableEntities(EntityManagerFactory emf) {
        this.emf = emf;
    }
    public TableEntities(EntityManagerFactory emf, Map<String, String> properties ) {
        this.emf = emf;
        this.properties = properties;
    }

    public EntityManager getEntityManager() {
        if (properties!=null) 
            return emf.createEntityManager(properties);
        return emf.createEntityManager();
    }

    public void PrintLista(Class<?> aClass) {
        System.out.println(aClass.getSimpleName());
        EntityManager em = getEntityManager();
        String myQ = SELECTER + lookup(aClass.getSimpleName()) + CNOT + DUMMYCONDITION;
        Query q = em.createNativeQuery(myQ, aClass);
        List results;
        results = q.getResultList();
        System.out.println("Table " + aClass.getSimpleName());
        results.forEach((Object result) -> {
            System.out.println(aClass.cast(result).toString());
        });
    }

    public void PrintSizeLista(Class<?> aClass) {
        EntityManager em = getEntityManager();
        String myQ = SELECTER + lookup(aClass.getSimpleName()) + CNOT + DUMMYCONDITION;
        Query q = em.createNativeQuery(myQ, aClass);
        List results;
        results = q.getResultList();
        System.out.println("Table " + aClass.getSimpleName() + ": There are <" + +results.size() + "> records to update ");
    }

    public void DummyUpdateLista(Class<?> aClass) {

        EntityManager em = getEntityManager();
        String myQ = SELECTER + lookup(aClass.getSimpleName()) + CNOT + DUMMYCONDITION;
        Query q = em.createNativeQuery(myQ, aClass);
        List results;
        results = q.getResultList();
        System.out.print("Table " + aClass.getSimpleName() + "  :");
        total = 0;
        em.getTransaction().begin();

        results.forEach((Object result) -> {

            Object cast = aClass.cast(result);
            //System.out.println(cast.toString());

            //use reflection
            // https://www.baeldung.com/java-method-reflection
            Method getidInstanceMethod = null;
            try {
                getidInstanceMethod = aClass.getMethod("getId");
            } catch (NoSuchMethodException | SecurityException ex) {
                System.err.println(cast.toString());
                Logger.getLogger(TableEntities.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(1);
            }

            int theId = 0;
            try {

                theId = (int) getidInstanceMethod.invoke(cast);

                // Names of the persistent attributes are case sensitive 
                //in JPQL queries and those should be written with exactly 
                //same case as they appear in entity.
                String myQ3 = "UPDATE " + aClass.getSimpleName() + " SET id = " + theId + " where id = " + theId;

                Query query = em.createQuery(myQ3);

                //System.out.println(query);
                int updateCount = query.executeUpdate();
                //System.out.println("Executed: " + updateCount);
                if (updateCount != 1) {
                    System.out.println("Brona");
                }
                total++;

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                System.err.println("e=" + ex.getMessage() + ex.toString());
                System.err.println("Bronca");
                System.err.println(cast.toString());
                Logger.getLogger(TableEntities.class.getName()).log(Level.SEVERE, null, ex);
                em.getTransaction().rollback();
            }
        });
        System.out.println("Total to be Commited " + total);

        em.getTransaction().commit();

        System.out.println("Committed to database ");
    }

}
