/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ubi.dbentities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paul Crocker
 */
@Entity
@Table(name = "instrument_collocation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InstrumentCollocation.findAll", query = "SELECT i FROM InstrumentCollocation i"),
    @NamedQuery(name = "InstrumentCollocation.findById", query = "SELECT i FROM InstrumentCollocation i WHERE i.id = :id"),
    @NamedQuery(name = "InstrumentCollocation.findByType", query = "SELECT i FROM InstrumentCollocation i WHERE i.type = :type"),
    @NamedQuery(name = "InstrumentCollocation.findByStatus", query = "SELECT i FROM InstrumentCollocation i WHERE i.status = :status"),
    @NamedQuery(name = "InstrumentCollocation.findByDateFrom", query = "SELECT i FROM InstrumentCollocation i WHERE i.dateFrom = :dateFrom"),
    @NamedQuery(name = "InstrumentCollocation.findByDateTo", query = "SELECT i FROM InstrumentCollocation i WHERE i.dateTo = :dateTo"),
    @NamedQuery(name = "InstrumentCollocation.findByComment", query = "SELECT i FROM InstrumentCollocation i WHERE i.comment = :comment"),
    @NamedQuery(name = "InstrumentCollocation.findByIdStation", query = "SELECT i FROM InstrumentCollocation i WHERE i.idStation = :idStation")})
public class InstrumentCollocation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "type")
    private String type;
    @Column(name = "status")
    private String status;
    @Column(name = "date_from")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFrom;
    @Column(name = "date_to")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTo;
    @Column(name = "comment")
    private String comment;
    @Basic(optional = false)
    @Column(name = "id_station")
    private int idStation;

    public InstrumentCollocation() {
    }

    public InstrumentCollocation(Integer id) {
        this.id = id;
    }

    public InstrumentCollocation(Integer id, String type, int idStation) {
        this.id = id;
        this.type = type;
        this.idStation = idStation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getIdStation() {
        return idStation;
    }

    public void setIdStation(int idStation) {
        this.idStation = idStation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstrumentCollocation)) {
            return false;
        }
        InstrumentCollocation other = (InstrumentCollocation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InstrumentCollocation[ id=" + id + " ]";
    }
    
}
