/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ubi.dbentities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paul Crocker
 */
@Entity
@Table(name = "item_attribute")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ItemAttribute.findAll", query = "SELECT i FROM ItemAttribute i"),
    @NamedQuery(name = "ItemAttribute.findById", query = "SELECT i FROM ItemAttribute i WHERE i.id = :id"),
    @NamedQuery(name = "ItemAttribute.findByDateFrom", query = "SELECT i FROM ItemAttribute i WHERE i.dateFrom = :dateFrom"),
    @NamedQuery(name = "ItemAttribute.findByDateTo", query = "SELECT i FROM ItemAttribute i WHERE i.dateTo = :dateTo"),
    @NamedQuery(name = "ItemAttribute.findByValueVarchar", query = "SELECT i FROM ItemAttribute i WHERE i.valueVarchar = :valueVarchar"),
    @NamedQuery(name = "ItemAttribute.findByValueDate", query = "SELECT i FROM ItemAttribute i WHERE i.valueDate = :valueDate"),
    @NamedQuery(name = "ItemAttribute.findByValueNumeric", query = "SELECT i FROM ItemAttribute i WHERE i.valueNumeric = :valueNumeric"),
    @NamedQuery(name = "ItemAttribute.findByIdAttribute", query = "SELECT i FROM ItemAttribute i WHERE i.idAttribute = :idAttribute"),
    @NamedQuery(name = "ItemAttribute.findByIdItem", query = "SELECT i FROM ItemAttribute i WHERE i.idItem = :idItem")})
public class ItemAttribute implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "date_from")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFrom;
    @Column(name = "date_to")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTo;
    @Column(name = "value_varchar")
    private String valueVarchar;
    @Column(name = "value_date")
    @Temporal(TemporalType.DATE)
    private Date valueDate;
    @Column(name = "value_numeric")
    private BigInteger valueNumeric;
    @Basic(optional = false)
    @Column(name = "id_attribute")
    private int idAttribute;
    @Basic(optional = false)
    @Column(name = "id_item")
    private int idItem;

    public ItemAttribute() {
    }

    public ItemAttribute(Integer id) {
        this.id = id;
    }

    public ItemAttribute(Integer id, int idAttribute, int idItem) {
        this.id = id;
        this.idAttribute = idAttribute;
        this.idItem = idItem;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getValueVarchar() {
        return valueVarchar;
    }

    public void setValueVarchar(String valueVarchar) {
        this.valueVarchar = valueVarchar;
    }

    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    public BigInteger getValueNumeric() {
        return valueNumeric;
    }

    public void setValueNumeric(BigInteger valueNumeric) {
        this.valueNumeric = valueNumeric;
    }

    public int getIdAttribute() {
        return idAttribute;
    }

    public void setIdAttribute(int idAttribute) {
        this.idAttribute = idAttribute;
    }

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemAttribute)) {
            return false;
        }
        ItemAttribute other = (ItemAttribute) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ItemAttribute[ id=" + id + " ]";
    }
    
}
