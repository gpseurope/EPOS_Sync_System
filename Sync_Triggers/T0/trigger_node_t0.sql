create or replace function trigger_node_t0() returns trigger
	language plpgsql
as $$
DECLARE
    noderecord RECORD;

    value_query     text;
    value_values    text;
    one_row         RECORD;
    value_metadata text;
    value_stationID integer;
    t1_exists integer;
    value_first boolean;

    id_destiny integer; 
    id_source integer;
    local_host text;

    id_node integer;
    v_name text;
    v_host text;
    v_old_host text;
    v_port text;
    v_dbname text;
    v_username text;
    v_password text;
    v_url text;
    v_email text;
    v_contact_name text;
    BEGIN

        -- First node should be local node thus its ID should be 1.
        -- We ensure this condition in line 38.
        id_source = 1;

        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_destiny = NEW.id;

            IF NEW.id != 1 THEN
                -- GET host of the server
                select CAST(inet_server_addr() AS text into local_host);

                -- Compare host of the server with the new one being inserted.
                -- If equal, then the node being inserted is the local node and we
                -- must ensure that the id of the local node on the database is always equal to 1;
                IF local_host = (NEW.host || '/32') THEN
                    IF (select COUNT(id) from node where id=1) = 0 THEN
                        -- Ensure that the local node will be equal to 1
                        UPDATE node set id = 1 WHERE id = NEW.ID;
                        id_source  = 1;
                        id_destiny = 1; -- Being the current node a local node, the destiny id must also be equal to 1
                    ELSE
                        -- Avoid multiple insertions of local nodes
                        raise EXCEPTION 'A local node was already added.';
                        RETURN NULL;
                    END IF;
                END IF;
            END IF;

            IF (id_source = id_destiny) THEN -- Local node has no need of connection information
                RETURN NULL;
            END IF;

            IF (select COUNT(id) from node where id=1) = 0 THEN
                raise EXCEPTION 'A local node is missing. You must add a local node first before inserting a remote one.';
                RETURN NULL;
            END IF;


            IF EXISTS (SELECT tgname FROM pg_trigger WHERE NOT tgisinternal AND tgname = 'sync_agency_t1') THEN
                -- Inserts default connection for T1 metadata
                INSERT INTO connections (source, destiny, station, metadata) VALUES (id_source, id_destiny, null, 'T1');

                -- Inserts remote node information into remote node DB as if it is the local node
                FOR one_row IN  
                    SELECT id, name, host, port, dbname, username, password, url, email, contact_name FROM node WHERE id = id_destiny
                LOOP

                    value_query = 'INSERT INTO node (name, host, port, dbname, username, password';
                    value_values = 'VALUES (' || quote_literal('Local Node') || ', ' || quote_literal(one_row.host)|| ', ' || quote_literal(one_row.port)|| ', ' || quote_literal(one_row.dbname)|| ', ' || quote_literal(one_row.username)|| ', ' || quote_literal(one_row.password);

                    IF one_row.url IS NOT NULL THEN
                        value_query =  value_query || ', url';
                        value_values = value_values || ', ' || quote_literal(one_row.url);
                    END IF;

                    IF one_row.email IS NOT NULL THEN
                        value_query =  value_query || ', email';
                        value_values = value_values || ', ' || quote_literal(one_row.email);
                    END IF;

                    IF one_row.contact_name IS NOT NULL THEN
                        value_query =  value_query || ', contact_name';
                        value_values = value_values || ', ' || quote_literal(one_row.contact_name);
                    END IF;

                    value_query = value_query || ') ' || value_values || ');';

                    INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', null, id_destiny);

                END LOOP;

                -- Inserts local node information into remote node DB as if it is the remote node
                FOR one_row IN  
                    SELECT id, name, host, port, dbname, username, password, url, email, contact_name FROM node WHERE id = id_source
                LOOP

                    value_query = 'INSERT INTO node (name, host, port, dbname, username, password';
                    value_values = 'VALUES (' || quote_literal('DGW Node') || ', ' || quote_literal(one_row.host)|| ', ' || quote_literal(one_row.port)|| ', ' || quote_literal(one_row.dbname)|| ', ' || quote_literal(one_row.username)|| ', ' || quote_literal(one_row.password);

                    IF one_row.url IS NOT NULL THEN
                        value_query =  value_query || ', url';
                        value_values = value_values || ', ' || quote_literal(one_row.url);
                    END IF;

                    IF one_row.email IS NOT NULL THEN
                        value_query =  value_query || ', email';
                        value_values = value_values || ', ' || quote_literal(one_row.email);
                    END IF;

                    IF one_row.contact_name IS NOT NULL THEN
                        value_query =  value_query || ', contact_name';
                        value_values = value_values || ', ' || quote_literal(one_row.contact_name);
                    END IF;

                    value_query = value_query || ') ' || value_values || ');';

                    INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', null, id_destiny);

                END LOOP;

            END IF;


            -- This is already done in the trigger sync_station_file_t2
            -- IF EXISTS (SELECT tgname FROM pg_trigger WHERE NOT tgisinternal AND tgname = 'sync_file_type_t2') THEN
                -- Inserts default connection for T2 metadata
              --  INSERT INTO connections (source, destiny, station, metadata) VALUES (id_source, id_destiny, null, 'T2');
            -- END IF;

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_destiny = OLD.id;

            -- Deletes default connection for T1 and T2 metadata
            DELETE FROM connections WHERE destiny = id_destiny;

            -- You should not be able to remove nodes, only edit.
            -- This is due to the implementation of the Sync Tool and the current database.
            -- We need the node information, if its removed the process of synchronization fails.
            RETURN NULL;
         END IF;

         -- New implementation
         IF (TG_OP = 'UPDATE') THEN

            -- Check if T1 triggers are installed
            -- If yes, store update queries for sync
            -- This means that nodes that are not the the DGW should not be able to sync T1 stuff
            SELECT count(tgname) INTO t1_exists FROM pg_trigger WHERE NOT tgisinternal AND tgname ILIKE '%_t1';

            IF (t1_exists = 0) THEN
                RETURN NULL;
            END IF;

            id_node           = OLD.id;
            value_first       = true;
            -- Columns
            v_old_host        = OLD.host;
            v_name            = NEW.name;
            v_host            = NEW.host;
            v_port            = NEW.port;
            v_dbname          = NEW.dbname;
            v_username        = NEW.username;
            v_password        = NEW.password;
            v_url             = NEW.url;
            v_email           = NEW.email;
            v_contact_name    = NEW.contact_name;

            IF NEW.host != OLD.host THEN
                raise notice 'Be aware that, if the nodes are not properly configured, changing the host can lead to several issues in the synchronization tool.';
                raise notice 'The new version of the Node Manager ensures the validation of valid hosts when adding or editing a node.';
            END IF;

            -- Create query
            value_query = 'UPDATE node SET ';

            -- Handle NULL values

            -- column name

            -- The name should not be updated in the remote node
            -- Uncomment if you really need this
            -- IF NEW.name IS NOT NULL THEN
                -- value_query = value_query || 'name=' || quote_literal(v_name);
                -- value_first = false;
            --END IF;

            -- column host
            IF NEW.host IS NOT NULL THEN
                value_query = value_query || 'host=' || quote_literal(v_host);
                value_first = false;
            END IF;

            -- column port
            IF NEW.port IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                --
                value_query = value_query || 'port=' || quote_literal(v_port);
            END IF;

            -- column dbname
            IF NEW.dbname IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                --
                value_query = value_query || 'dbname=' || quote_literal(v_dbname);
            END IF;

            -- column username
            IF NEW.username IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                --
                value_query = value_query || 'username=' || quote_literal(v_username);
            END IF;

            -- column password
            IF NEW.password IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                --
                value_query = value_query || 'password=' || quote_literal(v_password);
            END IF;

            -- column url
            IF NEW.url IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                --
                value_query = value_query || 'url=' || quote_literal(v_url);
            END IF;

            -- column email
            IF NEW.email IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                --
                value_query = value_query || 'email=' || quote_literal(v_email);
            END IF;

            -- column contact_name
            IF NEW.contact_name IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                --
                value_query = value_query || 'contact_name=' || quote_literal(v_contact_name);
            END IF;

            value_query = value_query || ' WHERE host =' || quote_literal(OLD.host);

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            IF (id_source = 1 AND id_node = id_source) THEN
				--This is the master node updatimg itself
				--It needs to propagate these changes to all connected nodes.
                --need to loop over all nodes and create quieries
                FOR noderecord IN SELECT * from node WHERE id > 1
                LOOP
                    --insert query into remote nodes
                    INSERT INTO queries (query, metadata, station_id, destiny)
                    VALUES (value_query, value_metadata, value_stationID, noderecord.id);
                END LOOP;
                RETURN null;
            END IF ;

            -- Only store the query if there is a connection to the current id_node
            IF (SELECT count(id) as n FROM connections WHERE destiny = id_node OR source = id_node) != 0 THEN
                INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, value_metadata, value_stationID, id_node);
            END IF;
            --
            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS trigger_node_t0 on node;
CREATE TRIGGER trigger_node_t0 AFTER INSERT OR UPDATE OR DELETE ON node
    FOR EACH ROW EXECUTE PROCEDURE trigger_node_t0();
    

    