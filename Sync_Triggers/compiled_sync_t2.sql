-- Remove previous version of the T2 auto connections feature (fail safe) - This feature was moved to sync_connections_t1 years ago
DROP TRIGGER IF EXISTS sync_station_t2 on station;

DROP TRIGGER  IF EXISTS  sync_quality_file_t2 on quality_file;
DROP FUNCTION IF EXISTS  sync_quality_file_t2();

DROP TRIGGER IF EXISTS  sync_other_files_t2 on other_files;
DROP FUNCTION IF EXISTS sync_other_files_t2();

DROP TRIGGER  IF EXISTS sync_rinex_error_types_t2 on rinex_error_types;
DROP FUNCTION IF EXISTS sync_rinex_error_types_t2();

DROP TRIGGER  IF EXISTS sync_rinex_errors_t2 on rinex_errors;
DROP FUNCTION IF EXISTS sync_rinex_errors_t2();

DROP TRIGGER  IF EXISTS  sync_file_type_t2 on file_type;
DROP FUNCTION IF EXISTS sync_file_type_t2();

CREATE OR REPLACE FUNCTION sync_rinex_file_t2() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
    -- Trigger Version 1.6.82 Date 15 October 2024
	value_query text;
    value_metadata text;
    value_stationID integer;
    value_first boolean;
    one_row RECORD;

	id_rinex_file integer;
	name text;
    id_station_ integer;
    id_data_center_structure integer;
    file_size integer;
    id_file_type integer;
    relative_path text;
    reference_date text;
    creation_date text;
    published_date text;
    revision_date text;
    md5checksum text;
    md5uncompressed text;
    status integer;

    aux_format text;
    aux_sampling_window text;
    aux_sampling_frequency text;
    old_md5checksum text;
    old_id_station integer;
    old_status integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_rinex_file = NEW.id;
            name = NEW.name;
            id_station_ = NEW.id_station;
            id_data_center_structure = NEW.id_data_center_structure;
            file_size = NEW.file_size;
            id_file_type = NEW.id_file_type;
            relative_path = NEW.relative_path;
            reference_date = NEW.reference_date;
            creation_date = NEW.creation_date;
            published_date = NEW.published_date;
            revision_date = NEW.revision_date;
            md5checksum = NEW.md5checksum;
            md5uncompressed = NEW.md5uncompressed;
            status = NEW.status;

            -- Handle rinex QC status ON INSERT
            -- IF status <= 0 THEN NOT synchronize
            -- IF status > 0  THEN synchronize
            if (status <= 0) THEN
                RAISE NOTICE '[rinex_file] QC Status = % - Not creating synchronizing operations for inserted rinex metadata ', status;
                RETURN NULL;
            END IF;
            -- Continues creating the remote query (i.e., status > 0)

                -- Gets data from file_type
                FOR one_row IN
                    SELECT format, sampling_window, sampling_frequency FROM file_type WHERE id = id_file_type
                LOOP
                    aux_format = one_row.format;
                    aux_sampling_window = one_row.sampling_window;
                    aux_sampling_frequency = one_row.sampling_frequency;
                END LOOP;

    			-- Create query
                value_query = 'INSERT INTO rinex_file (name, id_station, id_data_center_structure, id_file_type, relative_path, reference_date';

    			IF file_size IS NOT NULL THEN
                    value_query = value_query || ', file_size';
                END IF;

    			IF creation_date IS NOT NULL THEN
                    value_query = value_query || ', creation_date';
                END IF;

    			IF published_date IS NOT NULL THEN
                    value_query = value_query || ', published_date';
                END IF;

    			IF revision_date IS NOT NULL THEN
                    value_query = value_query || ', revision_date';
                END IF;

                IF md5checksum IS NOT NULL THEN
                    value_query = value_query || ', md5checksum';
                END IF;

                IF md5uncompressed IS NOT NULL THEN
                    value_query = value_query || ', md5uncompressed';
                END IF;

                IF status IS NOT NULL THEN
                    value_query = value_query || ', status';
                END IF;

    			value_query = value_query || ') VALUES (' || quote_literal(name) || ', ' || id_station_ || ', ' || id_data_center_structure || ', (SELECT id from file_type WHERE format = ' || quote_literal(aux_format) || ' AND sampling_window=' || quote_literal(aux_sampling_window) || ' AND sampling_frequency=' || quote_literal(aux_sampling_frequency) || '), ' || quote_literal(relative_path) || ', ' || quote_literal(reference_date);

    			IF file_size IS NOT NULL THEN
                    value_query = value_query || ', ' || file_size;
                END IF;

    			IF creation_date IS NOT NULL THEN
                    value_query = value_query || ', ' || quote_literal(creation_date);
                END IF;

    			IF published_date IS NOT NULL THEN
                    value_query = value_query || ', ' || quote_literal(published_date);
                END IF;

    			IF revision_date IS NOT NULL THEN
                    value_query = value_query || ', ' || quote_literal(revision_date);
                END IF;

                IF md5checksum IS NOT NULL THEN
                    value_query = value_query || ', ' || quote_literal(md5checksum);
                END IF;

                IF md5uncompressed IS NOT NULL THEN
                    value_query = value_query || ', ' || quote_literal(md5uncompressed);
                END IF;

                IF status IS NOT NULL THEN
                    value_query = value_query || ', ' || status;
                END IF;

    			value_query = value_query || ');';

                -- Set metadata type
                value_metadata = 'T2';

                -- Set station code
                value_stationID = id_station_;

                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_rinex_file = OLD.id;
            name = NEW.name;
            id_station_ = NEW.id_station;
            id_data_center_structure = NEW.id_data_center_structure;
            file_size = NEW.file_size;
            id_file_type = NEW.id_file_type;
            relative_path = NEW.relative_path;
            reference_date = NEW.reference_date;
            creation_date = NEW.creation_date;
            published_date = NEW.published_date;
            revision_date = NEW.revision_date;
            md5checksum = NEW.md5checksum;
            md5uncompressed = NEW.md5uncompressed;
            status = NEW.status;
            old_md5checksum = OLD.md5checksum;
            old_id_station = OLD.id_station;
            old_status = OLD.status;

            -- Handle rinex QC status ON UPDATE
            -- Let's begin:
              IF (NEW.status <= 0 AND OLD.status <=0) then
                 RAISE NOTICE '[rinex_file] QC new Status = % and old = % - Not creating synchronizing operations for inserted rinex metadata ', status, old_status;
                -- Exit, do not generate any remote update query
                RETURN NULL;
              END IF;

            -- IF status is now <= 0  AND previous status was >0 THEN delete rinex file from the DGW
            IF (NEW.status <= 0 AND OLD.status >0 ) THEN
                -- Create query
                value_query = 'DELETE FROM rinex_file WHERE id_station=' || OLD.id_station || ' AND md5checksum=' || quote_literal(OLD.md5checksum) || ' AND id_data_center_structure=' || OLD.id_data_center_structure  || ';';
                -- Set metadata type
                value_metadata = 'T2';
                -- Set station code
                value_stationID = OLD.id_station;
                -- Debug
                -- RAISE NOTICE '%', value_query;
                -- Query
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
                -- Exit, do not generate any remote update queries - The file will be removed from the remote node
                RETURN NULL;
            END IF;

            -- IF new status is now >0 and old status was <=0 THEN synchronize INSERT this rinex metadata
            IF (NEW.status > 0 AND OLD.status<=0) THEN

                    -- Gets data from file_type
                    FOR one_row IN
                        SELECT format, sampling_window, sampling_frequency FROM file_type WHERE id = id_file_type
                    LOOP
                        aux_format = one_row.format;
                        aux_sampling_window = one_row.sampling_window;
                        aux_sampling_frequency = one_row.sampling_frequency;
                    END LOOP;

    			    -- Create query
                    value_query = 'INSERT INTO rinex_file (name, id_station, id_data_center_structure, id_file_type, relative_path, reference_date';

    			    IF file_size IS NOT NULL THEN
                        value_query = value_query || ', file_size';
                    END IF;

    		    	IF creation_date IS NOT NULL THEN
                        value_query = value_query || ', creation_date';
                    END IF;

    			    IF published_date IS NOT NULL THEN
                        value_query = value_query || ', published_date';
                    END IF;

    			    IF revision_date IS NOT NULL THEN
                        value_query = value_query || ', revision_date';
                    END IF;

                    IF md5checksum IS NOT NULL THEN
                        value_query = value_query || ', md5checksum';
                    END IF;

                    IF md5uncompressed IS NOT NULL THEN
                        value_query = value_query || ', md5uncompressed';
                    END IF;

                    IF status IS NOT NULL THEN
                        value_query = value_query || ', status';
                    END IF;

    			    value_query = value_query || ') VALUES (' || quote_literal(name) || ', ' || id_station_ || ', ' || id_data_center_structure || ', (SELECT id from file_type WHERE format = ' || quote_literal(aux_format) || ' AND sampling_window=' || quote_literal(aux_sampling_window) || ' AND sampling_frequency=' || quote_literal(aux_sampling_frequency) || '), ' || quote_literal(relative_path) || ', ' || quote_literal(reference_date);

    			    IF file_size IS NOT NULL THEN
                        value_query = value_query || ', ' || file_size;
                    END IF;

    			    IF creation_date IS NOT NULL THEN
                        value_query = value_query || ', ' || quote_literal(creation_date);
                    END IF;

    			    IF published_date IS NOT NULL THEN
                        value_query = value_query || ', ' || quote_literal(published_date);
                    END IF;

    			    IF revision_date IS NOT NULL THEN
                        value_query = value_query || ', ' || quote_literal(revision_date);
                    END IF;

                    IF md5checksum IS NOT NULL THEN
                        value_query = value_query || ', ' || quote_literal(md5checksum);
                    END IF;

                    IF md5uncompressed IS NOT NULL THEN
                        value_query = value_query || ', ' || quote_literal(md5uncompressed);
                    END IF;

                    IF status IS NOT NULL THEN
                        value_query = value_query || ', ' || status;
                    END IF;

    			    value_query = value_query || ');';

                    -- Set metadata type
                    value_metadata = 'T2';

                    -- Set station code
                    value_stationID = id_station_;

                    -- Insert data to queries
                    INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

                    -- Exit, do not generate any remote update queries - The file will be inserted in the remote node
                    RETURN NULL;
            END IF;

            -- Generate UPDATE queries if the status file was not changed or if status file changed from one positive value to another

            -- Gets data from file_type
            FOR one_row IN
                SELECT format, sampling_window, sampling_frequency FROM file_type WHERE id = id_file_type
            LOOP
                aux_format = one_row.format;
                aux_sampling_window = one_row.sampling_window;
                aux_sampling_frequency = one_row.sampling_frequency;
            END LOOP;

    		-- Create query
            value_query = 'UPDATE rinex_file SET name =' || quote_literal(name) || ', id_station=' || id_station_  || ', id_data_center_structure=' || OLD.id_data_center_structure || ', id_file_type=(SELECT id from file_type WHERE format = ' || quote_literal(aux_format) || ' AND sampling_window=' || quote_literal(aux_sampling_window) || ' AND sampling_frequency=' || quote_literal(aux_sampling_frequency) || '), relative_path=' || quote_literal(relative_path) || ', reference_date=' || quote_literal(reference_date);

    		-- Handle NULL values
            IF NEW.file_size IS NOT NULL THEN
               value_query = value_query || ', file_size=' || file_size;
            END IF;

    		-- Handle NULL values
            IF NEW.creation_date IS NOT NULL THEN
               value_query = value_query || ', creation_date=' || quote_literal(creation_date);
            END IF;

    		-- Handle NULL values
            IF NEW.published_date IS NOT NULL THEN
               value_query = value_query || ', published_date=' || quote_literal(published_date);
            END IF;

    			-- Handle NULL values
            IF NEW.revision_date IS NOT NULL THEN
               value_query = value_query || ', revision_date=' || quote_literal(revision_date);
            END IF;

    	    -- Handle NULL values
            IF NEW.md5checksum IS NOT NULL THEN
               value_query = value_query || ', md5checksum=' || quote_literal(md5checksum);
            END IF;

            IF NEW.md5uncompressed IS NOT NULL THEN
               value_query = value_query || ', md5uncompressed=' || quote_literal(md5uncompressed);
            END IF;

            IF NEW.status IS NOT NULL THEN
               value_query = value_query || ', status=' || status;
            END IF;

    	    -- Create query
            value_query = value_query || ' WHERE id_station=' || old_id_station || ' AND md5checksum=' || quote_literal(old_md5checksum) ||' AND id_data_center_structure=' || OLD.id_data_center_structure  || ';';

            -- Set metadata type
            value_metadata = 'T2';

            -- Set station code
            value_stationID = id_station_;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_station_ = OLD.id_station;
            md5checksum = OLD.md5checksum;
            status = OLD.status;

            -- Handle rinex QC status ON DELETE
            -- IF status <= 0 means that the file was not synchronized to the remote node, so, no
            -- delete is necessary to be performed on the remote node (i.e., DGW)
            IF (status <= 0) THEN
                RAISE NOTICE '[rinex_file] QC Status = % - Not creating synchronizing operations for deleted rinex metadata ', status;
                RETURN NULL;
            END IF;

            -- Create query
            value_query = 'DELETE FROM rinex_file WHERE id_station=' || id_station_ || ' AND md5checksum=' || quote_literal(md5checksum) ||  ' AND id_data_center_structure=' || OLD.id_data_center_structure  || ';';

            -- Set metadata type
            value_metadata = 'T2';

            -- Set station code
            value_stationID = id_station_;

            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;

            RETURN NULL;
        END IF;
    END;
$$;

DROP TRIGGER IF EXISTS sync_rinex_file_t2 on rinex_file;
CREATE TRIGGER sync_rinex_file_t2 AFTER INSERT OR UPDATE OR DELETE ON rinex_file
    FOR EACH ROW EXECUTE PROCEDURE sync_rinex_file_t2();




