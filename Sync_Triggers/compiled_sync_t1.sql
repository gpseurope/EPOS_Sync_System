CREATE OR REPLACE FUNCTION sync_agency_t1() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
	  value_query text;
    value_metadata text;
    value_stationID integer;
    value_first boolean;

	  id_agency integer;
	  name text;
	  abbreviation text;
	  address text;
	  www text;
	  infos text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_agency = NEW.id;
            name = NEW.name;
            abbreviation = NEW.abbreviation;
            address = NEW.address;
            www = NEW.www;
            infos = NEW.infos;

			-- Create query
            value_query = 'INSERT INTO agency (id, name';

			IF abbreviation IS NOT NULL THEN
                value_query = value_query || ', abbreviation';
                value_first = false;
            ELSE
                 raise notice 'Warning On Inserting new Agency - abbreviation Can Not Be NUll';
            END IF;

			IF address IS NOT NULL THEN
                value_query = value_query || ', address';
                value_first = false;
            END IF;

			IF www IS NOT NULL THEN
                value_query = value_query || ', www';
                value_first = false;
            END IF;

			IF infos IS NOT NULL THEN
                value_query = value_query || ', infos';
                value_first = false;
            END IF;

			value_query = value_query || ') VALUES (' || id_agency || ', ' || quote_literal(name);

			IF abbreviation IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(abbreviation);
            END IF;

			IF address IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(address);
            END IF;

			IF www IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(www);
            END IF;

			IF infos IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(infos);
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        -- 7/2/24 Permite NULLS for non mandatory fields
        -- 7/2/42 raise error message when trying to change mandatory or unique fields to null
        IF (TG_OP = 'UPDATE') THEN
            id_agency = OLD.id;
			name = NEW.name;
            abbreviation = NEW.abbreviation;
            address = NEW.address;
            www = NEW.www;
            infos = NEW.infos;
			value_first = true;

			-- Create query
            value_query = 'UPDATE agency SET ';

			 -- Handle NULL values
             -- Warn on NULL name
            IF NEW.name != OLD.name then
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
                IF NEW.name IS NULL THEN
                    raise notice 'Warning On Updating Agency - null for then name of an agency is not advised';
                END IF;
            END IF;

			-- Handle NULL values
            -- Warn on NULL abreviation
            IF NEW.abbreviation IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'abbreviation=' || quote_literal(abbreviation);
            ELSE
                 raise notice 'Error On Updating Agency - null value for abbreviation not allowed';
            END IF;

			-- Permit NULL values and alter only on change of value
            IF NEW.address != OLD.address then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'address=' || quote_literal(address);
            END IF;

			-- Handle NULL values
			-- Possibility to update id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

			-- Handle NULL values
            -- Permit NULL values and alter only on change of value
            IF NEW.www != OLD.www then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'www=' || quote_literal(www);
            END IF;

			-- Handle NULL values
              -- Permit NULL values and alter only on change of value
            IF NEW.infos != OLD.infos then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'infos=' || quote_literal(infos);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_agency || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_agency = OLD.id;

            -- Create query
            value_query = 'DELETE FROM agency WHERE id=' || id_agency || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$;

DROP TRIGGER IF EXISTS sync_agency_t1 on agency;
CREATE TRIGGER sync_agency_t1 AFTER INSERT OR UPDATE OR DELETE ON agency
    FOR EACH ROW EXECUTE PROCEDURE sync_agency_t1();


create or replace function sync_attribute_t1() returns trigger
language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_attribute integer;
	    name text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_attribute = NEW.id;
            name = NEW.name;

            -- Create query
            value_query = 'INSERT INTO attribute (id, name) VALUES ' ||
                '(' || id_attribute || ', ' || quote_literal(name) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_attribute = OLD.id;
			name = NEW.name;
			value_first = true;

			-- Create query
			-- Added possibility to update id
            value_query = 'UPDATE attribute SET id=' || NEW.id || ',';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
            END IF;

             -- Create query
            value_query = value_query || ' WHERE id=' || id_attribute || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_attribute = OLD.id;

            -- Create query
            value_query = 'DELETE FROM attribute WHERE id=' || id_attribute || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_attribute_t1 on attribute;
CREATE TRIGGER sync_attribute_t1 AFTER INSERT OR UPDATE OR DELETE ON attribute
    FOR EACH ROW EXECUTE PROCEDURE sync_attribute_t1();

    create or replace function sync_bedrock_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_bedrock integer;
	    condition text;
	    type text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_bedrock = NEW.id;
            condition = NEW.condition;
            type = NEW.type;

			-- Create query
            value_query = 'INSERT INTO bedrock (id, condition';

            IF type IS NOT NULL THEN
                value_query = value_query || ', type';
            END IF;

			value_query = value_query || ') VALUES (' || id_bedrock || ', ' || quote_literal(condition);

			IF type IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(type);
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_bedrock = OLD.id;
			condition = NEW.condition;
            type = NEW.type;
			value_first = true;

			-- Create query
            value_query = 'UPDATE bedrock SET ';

			-- Handle NULL values
            IF NEW.condition IS NOT NULL THEN
                value_query = value_query || 'condition=' || quote_literal(condition);
                value_first = false;
            END IF;

			-- Handle NULL values
			-- Added possibility to edit id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

			-- Handle NULL values
            IF NEW.type IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'type =' || quote_literal(type);
            END IF;

			 -- Create query
            value_query = value_query || ' WHERE id=' || id_bedrock || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_bedrock = OLD.id;

            -- Create query
            value_query = 'DELETE FROM bedrock WHERE id=' || id_bedrock || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_bedrock_t1 on bedrock;
CREATE TRIGGER sync_bedrock_t1 AFTER INSERT OR UPDATE OR DELETE ON bedrock
    FOR EACH ROW EXECUTE PROCEDURE sync_bedrock_t1();

    create or replace function sync_city_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_city integer;
	    id_state integer;
	    name text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_city = NEW.id;
            id_state = NEW.id_state;
            name = NEW.name;

            -- Create query
            value_query = 'INSERT INTO city (id, id_state, name) VALUES ' ||
                '(' || id_city || ', ' || id_state || ', ' || quote_literal(name) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_city = OLD.id;
            id_state = NEW.id_state;
            name = NEW.name;
			value_first = true;

            -- Create query
            value_query = 'UPDATE city SET ';

			-- Handle NULL values
            IF NEW.id_state IS NOT NULL THEN
                value_query = value_query || 'id_state=' || id_state;
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Added possibility to update id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id =' || NEW.id;
            END IF;

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'name =' || quote_literal(name);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_city || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_city = OLD.id;

            -- Create query
            value_query = 'DELETE FROM city WHERE id=' || id_city || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_city_t1 on city;
CREATE TRIGGER sync_city_t1 AFTER INSERT OR UPDATE OR DELETE ON city
    FOR EACH ROW EXECUTE PROCEDURE sync_city_t1();
    

    create or replace function sync_colocation_offset_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_colocation_offset integer;
	    id_station_colocation integer;
	    offset_x numeric;
	    offset_y numeric;
	    offset_z numeric;
	    date_measured text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_colocation_offset = NEW.id;
            id_station_colocation = NEW.id_station_colocation;
			offset_x = NEW.offset_x;
			offset_y = NEW.offset_y;
			offset_z = NEW.offset_z;
			date_measured = NEW.date_measured;

			-- Create query
            value_query = 'INSERT INTO colocation_offset (id, id_station_colocation, date_measured';

			IF offset_x IS NOT NULL THEN
                value_query = value_query || ', offset_x';
            END IF;

			IF offset_y IS NOT NULL THEN
                value_query = value_query || ', offset_y';
            END IF;

			IF offset_z IS NOT NULL THEN
                value_query = value_query || ', offset_z';
            END IF;

			value_query = value_query || ') VALUES (' || id_colocation_offset || ', ' || id_station_colocation || ', ' || quote_literal(date_measured);

			IF offset_x IS NOT NULL THEN
                value_query = value_query || ', ' || offset_x;
            END IF;

			IF offset_y IS NOT NULL THEN
                value_query = value_query || ', ' || offset_y;
            END IF;

			IF offset_z IS NOT NULL THEN
                value_query = value_query || ', ' || offset_z;
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            -- value_stationID = NULL; It cannot be NULL because the station may not exist on the destiny node
            -- so, if we try to insert with a station that is not available on the destiny node an error will
            -- occur.
            -- Lets assign to value_stationID the right one
            SELECT id_station INTO value_stationID FROM station_colocation WHERE id = id_station_colocation;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_colocation_offset = OLD.id;
			id_station_colocation = NEW.id_station_colocation;
			offset_x = NEW.offset_x;
			offset_y = NEW.offset_y;
			offset_z = NEW.offset_z;
			date_measured = NEW.date_measured;
			value_first = true;

			-- Create query
            value_query = 'UPDATE colocation_offset SET ';

			-- Handle NULL values
            IF NEW.id_station_colocation IS NOT NULL THEN
                value_query = value_query || 'id_station_colocation=' || id_station_colocation;
                value_first = false;
            END IF;

			-- Handle NULL values
            IF NEW.offset_x IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'offset_x =' || offset_x;
            END IF;

			-- Handle NULL values
            IF NEW.offset_y IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'offset_y =' || offset_y;
            END IF;

			-- Handle NULL values
            IF NEW.offset_z IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'offset_z =' || offset_z;
            END IF;

			-- Handle NULL values
            IF NEW.date_measured IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_measured =' || quote_literal(date_measured);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_colocation_offset || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            -- value_stationID = NULL;
            -- It cannot be NULL because the station may not exist on the destiny node
            -- so, if we try to insert with a station that is not available on the destiny node an error will
            -- occur.
            -- Lets assign to value_stationID the right one
            SELECT id_station INTO value_stationID FROM station_colocation WHERE id = id_station_colocation;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_colocation_offset = OLD.id;
            id_station_colocation = OLD.id_station_colocation;

            -- Create query
            value_query = 'DELETE FROM colocation_offset WHERE id=' || id_colocation_offset || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            -- value_stationID = NULL;
            -- It cannot be NULL because the station may not exist on the destiny node
            SELECT id_station INTO value_stationID FROM station_colocation WHERE id = id_station_colocation;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_colocation_offset_t1 on colocation_offset;
CREATE TRIGGER sync_colocation_offset_t1 AFTER INSERT OR UPDATE OR DELETE ON colocation_offset
    FOR EACH ROW EXECUTE PROCEDURE sync_colocation_offset_t1();
    

    create or replace function sync_condition_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_condition integer;
	    id_station integer;
	    id_effect integer;
	    date_from timestamp;
	    date_to timestamp;
	    degradation text;
	    comments text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_condition = NEW.id;
            id_station = NEW.id_station;
            id_effect = NEW.id_effect;
			date_from = NEW.date_from;
			date_to = NEW.date_to;
			degradation = NEW.degradation;
			comments = NEW.comments;

			-- Create query
            value_query = 'INSERT INTO condition (id, id_station, id_effect';

			IF date_from IS NOT NULL THEN
                value_query = value_query || ', date_from';
            END IF;

			IF date_to IS NOT NULL THEN
                value_query = value_query || ', date_to';
            END IF;

			IF degradation IS NOT NULL THEN
                value_query = value_query || ', degradation';
            END IF;

			IF comments IS NOT NULL THEN
                value_query = value_query || ', comments';
            END IF;

			value_query = value_query || ') VALUES (' || id_condition || ', ' || id_station || ', ' || id_effect;

			IF date_from IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_from);
            END IF;

			IF date_to IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_to);
            END IF;

			IF degradation IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(degradation);
            END IF;

			IF comments IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(comments);
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_condition = OLD.id;
			id_station = NEW.id_station;
            id_effect = NEW.id_effect;
			date_from = NEW.date_from;
			date_to = NEW.date_to;
			degradation = NEW.degradation;
			comments = NEW.comments;
			value_first = true;

			-- Create query
            value_query = 'UPDATE condition SET ';

			-- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station =' || id_station;
                value_first = false;
            END IF;

			-- Handle NULL values
            IF NEW.id_effect IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_effect =' || id_effect;
            END IF;

			-- Handle NULL values
            IF NEW.date_from IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_from =' || quote_literal(date_from);
            END IF;

			-- Handle NULL values
            IF NEW.date_to IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_to =' || quote_literal(date_to);
            END IF;

			-- Handle NULL values
            IF NEW.degradation IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'degradation =' || quote_literal(degradation);
            END IF;

			-- Handle NULL values
            IF NEW.comments IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'comments =' || quote_literal(comments);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_condition || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_condition = OLD.id;

            -- Create query
            value_query = 'DELETE FROM condition WHERE id=' || id_condition || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            -- this is wrong, the id_condition is not the same as the id of the station
            -- value_stationID = id_condition;
            -- set this to correct value
            value_stationID = OLD.id_station;

            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;


            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_condition_t1 on condition;
CREATE TRIGGER sync_condition_t1 AFTER INSERT OR UPDATE OR DELETE ON condition
    FOR EACH ROW EXECUTE PROCEDURE sync_condition_t1();

CREATE OR REPLACE FUNCTION sync_connections_t1() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
	value_query 	text;
	value_values	text;
	one_row    		RECORD;
    BEGIN

        -- Handle insert
        IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') then

		    -- Handle station not NULL and metadata = T1
			IF (NEW.station IS NOT NULL AND NEW.metadata = 'T1') THEN
				-- IF NOT EXISTS (SELECT * FROM queries WHERE query ILIKE '%insert into station % values (' || NEW.station || ',%') THEN
                -- BUG FIXES
                IF NOT EXISTS(SELECT * FROM queries WHERE query ILIKE '%insert into station % values ('|| NEW.station ||', %)') THEN

                    raise notice 'Query to insert a new station is MIA for station %', NEW.station;
                    -- STATION TABLE BEGIN --
				    	FOR one_row IN 
					        SELECT id, name, marker, description, date_from, date_to, id_station_type, comment, id_location, id_monument, id_geological, iers_domes, cpd_num, monument_num, receiver_num, country_code FROM station WHERE id = NEW.station
					    LOOP 
					    	value_query = 'INSERT INTO station (id, name, marker';
					    	value_values = 'VALUES (' || one_row.id || ', ' || quote_literal(one_row.name) || ', ' || quote_literal(one_row.marker);

					    	IF one_row.description IS NOT NULL THEN
								value_query =  value_query || ', description';
								value_values = value_values || ', ' || quote_literal(one_row.description);
				            END IF;

					    	IF one_row.date_from IS NOT NULL THEN
								value_query =  value_query || ', date_from';
								value_values = value_values || ', ' || quote_literal(one_row.date_from);
				            END IF;

				            IF one_row.date_to IS NOT NULL THEN
								value_query =  value_query || ', date_to';
								value_values = value_values || ', ' || quote_literal(one_row.date_to);
				            END IF;

				            IF one_row.id_station_type IS NOT NULL THEN
								value_query =  value_query || ', id_station_type';
								value_values = value_values || ', ' || one_row.id_station_type;
				            END IF;

				            IF one_row.comment IS NOT NULL THEN
								value_query =  value_query || ', comment';
								value_values = value_values || ', ' || quote_literal(one_row.comment);
				            END IF;

				            IF one_row.id_location IS NOT NULL THEN
								value_query =  value_query || ', id_location';
								value_values = value_values || ', ' || one_row.id_location;
				            END IF;

				            IF one_row.id_monument IS NOT NULL THEN
								value_query =  value_query || ', id_monument';
								value_values = value_values || ', ' || quote_literal(one_row.id_monument);
				            END IF;

				            IF one_row.id_geological IS NOT NULL THEN
								value_query =  value_query || ', id_geological';
								value_values = value_values || ', ' || quote_literal(one_row.id_geological);
				            END IF;

				            IF one_row.iers_domes IS NOT NULL THEN
								value_query =  value_query || ', iers_domes';
								value_values = value_values || ', ' || quote_literal(one_row.iers_domes);
				            END IF;

				            IF one_row.cpd_num IS NOT NULL THEN
								value_query =  value_query || ', cpd_num';
								value_values = value_values || ', ' || quote_literal(one_row.cpd_num);
				            END IF;

				            IF one_row.monument_num IS NOT NULL THEN
								value_query =  value_query || ', monument_num';
								value_values = value_values || ', ' || one_row.monument_num;
				            END IF;

				            IF one_row.receiver_num IS NOT NULL THEN
								value_query =  value_query || ', receiver_num';
								value_values = value_values || ', ' || one_row.receiver_num;
				            END IF;

				            IF one_row.country_code IS NOT NULL THEN
								value_query =  value_query || ', country_code';
								value_values = value_values || ', ' || quote_literal(one_row.country_code);
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);
                            raise notice 'Station mapped to destiny node id : %', NEW.destiny;
					    END LOOP;
			    	-- STATION TABLE END --

			    	-- STATIONS BEGIN --
		    			-- station_colocation
		    			FOR one_row IN
				        	SELECT id, id_station, id_station_colocated FROM station_colocation WHERE id_station = NEW.station
					    LOOP

					    	value_query = 'INSERT INTO station_colocation (id, id_station, id_station_colocated) VALUES (' || one_row.id || ', ' || one_row.id_station || ', ' || one_row.id_station_colocated || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;

					    -- local_ties
					    FOR one_row IN 
					        SELECT id, id_station, name, usage, cpd_num, iers_domes, dx, dy, dz, accuracy, survey_method, date_at, comment FROM local_ties WHERE id_station = NEW.station
					    LOOP 
					    	value_query = 'INSERT INTO local_ties (id, id_station, name';
					    	value_values = 'VALUES (' || one_row.id || ', ' || one_row.id_station || ', ' || quote_literal(one_row.name);

					    	IF one_row.usage IS NOT NULL THEN
								value_query =  value_query || ', usage';
								value_values = value_values || ', ' || quote_literal(one_row.usage);
				            END IF;

					    	IF one_row.cpd_num IS NOT NULL THEN
								value_query =  value_query || ', cpd_num';
								value_values = value_values || ', ' || quote_literal(one_row.cpd_num);
				            END IF;

				            IF one_row.iers_domes IS NOT NULL THEN
								value_query =  value_query || ', iers_domes';
								value_values = value_values || ', ' || quote_literal(one_row.iers_domes);
				            END IF;

				            IF one_row.dx IS NOT NULL THEN
								value_query =  value_query || ', dx';
								value_values = value_values || ', ' || one_row.dx;
				            END IF;

				            IF one_row.dy IS NOT NULL THEN
								value_query =  value_query || ', dy';
								value_values = value_values || ', ' || one_row.dy;
				            END IF;

				            IF one_row.dz IS NOT NULL THEN
								value_query =  value_query || ', dz';
								value_values = value_values || ', ' || one_row.dz;
				            END IF;

				            IF one_row.accuracy IS NOT NULL THEN
								value_query =  value_query || ', accuracy';
								value_values = value_values || ', ' || one_row.accuracy;
				            END IF;

				            IF one_row.survey_method IS NOT NULL THEN
								value_query =  value_query || ', survey_method';
								value_values = value_values || ', ' || quote_literal(one_row.survey_method);
				            END IF;

				            IF one_row.date_at IS NOT NULL THEN
								value_query =  value_query || ', date_at';
								value_values = value_values || ', ' || quote_literal(one_row.date_at);
				            END IF;

				            IF one_row.comment IS NOT NULL THEN
								value_query =  value_query || ', comment';
								value_values = value_values || ', ' || quote_literal(one_row.comment);
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;

					    -- instrument_collocation
					    FOR one_row IN 
					        SELECT id, id_station, type, status, date_from, date_to, comment FROM instrument_collocation WHERE id_station = NEW.station
					    LOOP 
					    	value_query = 'INSERT INTO instrument_collocation (id, id_station, type';
					    	value_values = 'VALUES (' || one_row.id || ', ' || one_row.id_station || ', ' || quote_literal(one_row.type);

					    	IF one_row.status IS NOT NULL THEN
								value_query =  value_query || ', status';
								value_values = value_values || ', ' || quote_literal(one_row.status);
				            END IF;

				            IF one_row.date_from IS NOT NULL THEN
								value_query =  value_query || ', date_from';
								value_values = value_values || ', ' || quote_literal(one_row.date_from);
				            END IF;

				            IF one_row.date_to IS NOT NULL THEN
								value_query =  value_query || ', date_to';
								value_values = value_values || ', ' || quote_literal(one_row.date_to);
				            END IF;

				            IF one_row.comment IS NOT NULL THEN
								value_query =  value_query || ', comment';
								value_values = value_values || ', ' || quote_literal(one_row.comment);
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;
			    	-- STATIONS END --

			    	-- LOCAL CONDITIONS AND EFFECTS BEGIN --
			    		-- condition
						FOR one_row IN 
					        SELECT id, id_station, id_effect, date_from, date_to, degradation, comments FROM condition WHERE id_station = NEW.station
					    LOOP 
					    	value_query = 'INSERT INTO condition (id, id_station, id_effect';
					    	value_values = 'VALUES (' || one_row.id || ', ' || one_row.id_station || ', ' || one_row.id_effect;

				            IF one_row.date_from IS NOT NULL THEN
								value_query =  value_query || ', date_from';
								value_values = value_values || ', ' || quote_literal(one_row.date_from);
				            END IF;

				            IF one_row.date_to IS NOT NULL THEN
								value_query =  value_query || ', date_to';
								value_values = value_values || ', ' || quote_literal(one_row.date_to);
				            END IF;

							IF one_row.degradation IS NOT NULL THEN
								value_query =  value_query || ', degradation';
								value_values = value_values || ', ' || quote_literal(one_row.degradation);
				            END IF;

				            IF one_row.comments IS NOT NULL THEN
								value_query =  value_query || ', comments';
								value_values = value_values || ', ' || quote_literal(one_row.comments);
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;
			    	-- LOCAL CONDITIONS AND EFFECTS END --

			    	-- ACCESS CONTROL BEGIN --
			    		-- user_group_station
		    			FOR one_row IN 
				            -- SELECT id, id_user_group, id_station FROM user_group_station WHERE id_stations = NEW.station
					        -- BUG fixes
					        SELECT id, id_user_group, id_station FROM user_group_station WHERE id_station = NEW.station
					    LOOP

					    	value_query = 'INSERT INTO user_group_station (id, id_user_group, id_station) VALUES (' || one_row.id || ', ' || one_row.id_user_group || ', ' || one_row.id_station || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;
			    	-- ACCESS CONTROL END --

			    	-- SITE LOGS BEGIN --
			    		-- log
		    			FOR one_row IN 
				        	SELECT id, title, date, id_log_type, id_station, modified, previous, id_contact FROM log WHERE id_station = NEW.station
					    LOOP 

					    	value_query = 'INSERT INTO log (id, title, id_log_type, id_station';
					    	value_values = 'VALUES (' || one_row.id || ', ' || quote_literal(one_row.title) || ', ' || one_row.id_log_type || ', ' || one_row.id_station;

				            IF one_row.date IS NOT NULL THEN
								value_query =  value_query || ', date';
								value_values = value_values || ', ' || quote_literal(one_row.date);
				            END IF;

				            IF one_row.modified IS NOT NULL THEN
								value_query =  value_query || ', modified';
								value_values = value_values || ', ' || quote_literal(one_row.modified);
				            END IF;

							IF one_row.previous IS NOT NULL THEN
								value_query =  value_query || ', previous';
								value_values = value_values || ', ' || quote_literal(one_row.previous);
				            END IF;

				            IF one_row.id_contact IS NOT NULL THEN
								value_query =  value_query || ', id_contact';
								value_values = value_values || ', ' || one_row.id_contact;
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;
			    	-- SITE LOGS END --

			    	-- ORGANIZATIONAL BEGIN --
			    		-- station_contact
			    		FOR one_row IN 
				        	SELECT id, id_station, id_contact, role FROM station_contact WHERE id_station = NEW.station
					    LOOP 

					    	value_query = 'INSERT INTO station_contact (id, id_station, id_contact';
					    	value_values = 'VALUES (' || one_row.id || ', ' || one_row.id_station || ', ' || one_row.id_contact;

				            IF one_row.role IS NOT NULL THEN
								value_query =  value_query || ', role';
								value_values = value_values || ', ' || quote_literal(one_row.role);
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;

					    -- station_network
					    FOR one_row IN 
				        	SELECT id, id_station, id_network FROM station_network WHERE id_station = NEW.station
					    LOOP 

					    	value_query = 'INSERT INTO station_network (id, id_station, id_network) VALUES (' || one_row.id || ', ' || one_row.id_station || ', ' || one_row.id_network || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;
			    	-- ORGANIZATIONAL END --

			    	-- DOCUMENTS BEGIN --
			    		-- document
			    		FOR one_row IN 
				        	SELECT id, date, title, description, link, id_station, id_item, id_document_type FROM document WHERE id_station = NEW.station
					    LOOP 

					    	value_query = 'INSERT INTO document (id, date, title, link, id_document_type, id_station';
					    	value_values = 'VALUES (' || one_row.id || ', ' || quote_literal(one_row.date) || ', ' || quote_literal(one_row.title) || ', ' || quote_literal(one_row.link) || ', ' || one_row.id_document_type || ', ' || one_row.id_station;

				            IF one_row.description IS NOT NULL THEN
								value_query =  value_query || ', description';
								value_values = value_values || ', ' || quote_literal(one_row.description);
				            END IF;

				            IF one_row.id_item IS NOT NULL THEN
								value_query =  value_query || ', id_item';
								value_values = value_values || ', ' || one_row.id_item;
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;
			    	-- DOCUMENTS END --

			    	-- ITEMS BEGIN --
			    		-- station_item
			    		FOR one_row IN 
				        	SELECT id, id_station, id_item, date_from, date_to FROM station_item WHERE id_station = NEW.station
					    LOOP 

					    	value_query = 'INSERT INTO station_item (id, id_station, id_item, date_from';
					    	value_values = 'VALUES (' || one_row.id || ', ' || one_row.id_station || ', ' || one_row.id_item || ', ' || quote_literal(one_row.date_from);

				            IF one_row.date_to IS NOT NULL THEN
								value_query =  value_query || ', date_to';
								value_values = value_values || ', ' || quote_literal(one_row.date_to);
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;
			    	-- ITEMS END --



				END IF; -- END IF NOT EXISTS STATION IN QUERIES --

                -----------------------------------------------------
				-- AUTO CREATE T2 CONNECTIONS ON THE NODE SIDE BASED ON THE T1 CONNECTIONS OF THE DGW
				-----------------------------------------------------
                -- After adding a new T1 station-connection a T2 station-connection should be established in the remote node

                -- Handle CREATE CONNECTIONS
                IF (TG_OP = 'INSERT') THEN
                    -- Add a new T2 connection between the local node and the remote one
                    value_query = 'DO ' || chr(36) || '' || chr(36) || ' BEGIN';
				    -- Only proceed if the newly sync station is available on the remote node (i.e., successfully synchronized)
                    value_query = value_query || ' IF EXISTS (SELECT * FROM station WHERE id='|| NEW.station ||') THEN';
				    value_query = value_query || ' IF NOT EXISTS(SELECT * FROM connections WHERE station=' || NEW.station ||') THEN';
				    value_query = value_query || ' INSERT INTO connections (source, destiny, station, metadata) VALUES (1, (SELECT id FROM node where id!=1 LIMIT 1), ' || NEW.station || ', ''T2'');';
                    value_query = value_query || ' END IF; END IF; END; ' || chr(36) || '' || chr(36) || ';';
                    -- raise notice 'INSERT: %', value_query;
                    INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

                    -- A generic connection between the current node and the DGW should also be established
                    value_query = 'DO ' || chr(36) || '' || chr(36) || ' BEGIN';
				    -- Only proceed if there is at least one station successfully sync to the remote node
                    value_query = value_query || ' IF EXISTS (SELECT * FROM station) THEN';
				    value_query = value_query || ' IF NOT EXISTS(SELECT * FROM connections WHERE metadata=''T2'' AND station IS NULL AND destiny IS NOT NULL) THEN';
				    value_query = value_query || ' INSERT INTO connections (source, destiny, station, metadata) VALUES (1, (SELECT id FROM node where id!=1 LIMIT 1), NULL, ''T2'');';
                    value_query = value_query || ' END IF; END IF; END; ' || chr(36) || '' || chr(36) || ';';
                    -- raise notice 'INSERT: %', value_query;
                    INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);
                END IF;

                -- Handle UPDATE CONNECTIONS
                IF (TG_OP = 'UPDATE') THEN
                    value_query = 'DO ' || chr(36) || '' || chr(36) || ' BEGIN';
                    value_query = value_query || ' IF EXISTS (SELECT * FROM station WHERE id='|| NEW.station ||') THEN';
				    value_query = value_query || ' IF EXISTS (SELECT * FROM station WHERE id='|| OLD.station ||') THEN';
				    value_query = value_query || ' UPDATE connections SET station =' || NEW.station ||' WHERE station =' || OLD.station || ';';
                    value_query = value_query || ' END IF; END IF; END; ' || chr(36) || '' || chr(36) || ';';
                    -- raise notice 'UPDATE: %', value_query;
                    INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);
                END IF;

                RETURN NULL;
		    END IF; -- END IF STATION IS NOT NULL AND METADATA = T1

	        -- Handle not null values and metadata != T1
		    IF (NEW.station IS NOT NULL OR NEW.metadata != 'T1') THEN
		        RETURN NULL;
		    END IF;
        
        	-- ORGANIZATIONAL BEGIN --
        		-- agency
		        FOR one_row IN 
			        SELECT id, name, abbreviation, address, www, infos FROM agency
			    LOOP  
				   	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE agency IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO agency (id, name';

					value_values = ' SELECT ' || one_row.id || ', ' || quote_literal(one_row.name);

					IF one_row.abbreviation IS NOT NULL THEN
						value_query =  value_query || ', abbreviation';
						value_values = value_values || ', ' || quote_literal(one_row.abbreviation);
		            END IF;

		            IF one_row.address IS NOT NULL THEN
						value_query =  value_query || ', address';
						value_values = value_values || ', ' || quote_literal(one_row.address);
		            END IF;

		            IF one_row.www IS NOT NULL THEN
						value_query =  value_query || ', www';
						value_values = value_values || ', ' || quote_literal(one_row.www);
		            END IF;

		            IF one_row.infos IS NOT NULL THEN
						value_query =  value_query || ', infos';
						value_values = value_values || ', ' || quote_literal(one_row.infos);
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '	SELECT * FROM agency WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';
		
		            -- Insert data to queries
		            INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
	
				-- contact
		        FOR one_row IN 
			        SELECT id, name, title, email, phone, gsm, comment, id_agency, role FROM contact
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE contact IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO contact (id, name, id_agency';

					value_values = ' SELECT ' || one_row.id || ', ' || quote_literal(one_row.name) || ', ' || one_row.id_agency;

					IF one_row.title IS NOT NULL THEN
						value_query =  value_query || ', title';
						value_values = value_values || ', ' || quote_literal(one_row.title);
		            END IF;

					IF one_row.email IS NOT NULL THEN
						value_query =  value_query || ', email';
						value_values = value_values || ', ' || quote_literal(one_row.email);
		            END IF;

		            IF one_row.phone IS NOT NULL THEN
						value_query =  value_query || ', phone';
						value_values = value_values || ', ' || quote_literal(one_row.phone);
		            END IF;

					IF one_row.gsm IS NOT NULL THEN
						value_query =  value_query || ', gsm';
						value_values = value_values || ', ' || quote_literal(one_row.gsm);
		            END IF;

					IF one_row.comment IS NOT NULL THEN
						value_query =  value_query || ', comment';
						value_values = value_values || ', ' || quote_literal(one_row.comment);
		            END IF;

		            IF one_row.role IS NOT NULL THEN
						value_query =  value_query || ', role';
						value_values = value_values || ', ' || quote_literal(one_row.role);
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '	SELECT * FROM contact WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
	        		
				-- network
		        FOR one_row IN 
			        SELECT id, name, id_contact FROM network
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE network IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO network (id, name';

					value_values = ' SELECT ' || one_row.id || ', ' || quote_literal(one_row.name);

					IF one_row.id_contact IS NOT NULL THEN
						value_query =  value_query || ', id_contact';
						value_values = value_values || ', ' || one_row.id_contact;
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '	SELECT * FROM network WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
			-- ORGANIZATIONAL END --

			-- ITEMS BEGIN --
	
				-- attribute
		        FOR one_row IN 
			        SELECT id, name FROM attribute
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE attribute IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO attribute (id, name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM attribute WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';
			        
			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- item_type
		        FOR one_row IN 
			        SELECT id, name FROM item_type
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE item_type IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO item_type (id, name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM item_type WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- item_type_attribute
		        FOR one_row IN 
			        SELECT id, id_item_type, id_attribute FROM item_type_attribute
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE item_type_attribute IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO item_type_attribute (id, id_item_type, id_attribute) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.id_item_type) || ', ' || quote_literal(one_row.id_attribute) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM item_type_attribute WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- item
		        FOR one_row IN 
			        SELECT id, id_item_type, id_contact_as_producer, id_contact_as_owner, comment FROM item
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE item IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO item (id, id_item_type';

					value_values = ' SELECT ' || one_row.id || ', ' || one_row.id_item_type;

					IF one_row.id_contact_as_producer IS NOT NULL THEN
						value_query =  value_query || ', id_contact_as_producer';
						value_values = value_values || ', ' || one_row.id_contact_as_producer;
		            END IF;

		            IF one_row.id_contact_as_owner IS NOT NULL THEN
						value_query =  value_query || ', id_contact_as_owner';
						value_values = value_values || ', ' || one_row.id_contact_as_owner;
		            END IF;

		            IF one_row.comment IS NOT NULL THEN
						value_query =  value_query || ', comment';
						value_values = value_values || ', ' || quote_literal(one_row.comment);
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM item WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- item_attribute
		        FOR one_row IN 
			        SELECT id, id_item, id_attribute, date_from, date_to, value_varchar, value_date, value_numeric FROM item_attribute
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE item_attribute IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO item_attribute (id, id_item, id_attribute';

					value_values = ' SELECT ' || one_row.id || ', ' || one_row.id_item || ', ' || one_row.id_attribute;

					IF one_row.date_from IS NOT NULL THEN
						value_query =  value_query || ', date_from';
						value_values = value_values || ', ' || quote_literal(one_row.date_from);
		            END IF;

		            IF one_row.date_to IS NOT NULL THEN
						value_query =  value_query || ', date_to';
						value_values = value_values || ', ' || quote_literal(one_row.date_to);
		            END IF;

		            IF one_row.value_varchar IS NOT NULL THEN
						value_query =  value_query || ', value_varchar';
						value_values = value_values || ', ' || quote_literal(one_row.value_varchar);
		            END IF;

		            IF one_row.value_date IS NOT NULL THEN
						value_query =  value_query || ', value_date';
						value_values = value_values || ', ' || quote_literal(one_row.value_date);
		            END IF;

		            IF one_row.value_numeric IS NOT NULL THEN
						value_query =  value_query || ', value_numeric';
						value_values = value_values || ', ' || one_row.value_numeric;
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM item_attribute WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
			
			-- SITE LOGS BEGIN --
				-- log_type
		        FOR one_row IN 
			        SELECT id, name FROM log_type
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE log_type IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO log_type (id, name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM log_type WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
			-- SITE LOGS END --
			
			-- DOCUMENTS BEGIN --
				-- document_type
		        FOR one_row IN 
			        SELECT id, name FROM document_type
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE document_type IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO document_type (id, name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM document_type WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
			-- DOCUMENTS END --
			
			-- ACCESS CONTROL BEGIN --
				-- user_group
		        FOR one_row IN 
			        SELECT id, name FROM user_group
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE user_group IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO user_group (id, name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM user_group WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
			-- ACCESS CONTROL END --
			
			-- STATIONS BEGIN --			
				-- bedrock
		        FOR one_row IN 
			        SELECT id, condition, type FROM bedrock
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE bedrock IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO bedrock (id, condition';

					value_values = ' SELECT ' || one_row.id || ', ' || quote_literal(one_row.condition);

					IF one_row.type IS NOT NULL THEN
						value_query =  value_query || ', type';
						value_values = value_values || ', ' || quote_literal(one_row.type);
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM bedrock WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- geological
		        FOR one_row IN 
			        SELECT id, id_bedrock, characteristic, fracture_spacing, fault_zone, distance_to_fault FROM geological
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE geological IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO geological (id, characteristic ';

					value_values = ' SELECT ' || one_row.id || ', ' || quote_literal(one_row.characteristic);

					IF one_row.id_bedrock IS NOT NULL THEN
						value_query =  value_query || ', id_bedrock';
						value_values = value_values || ', ' || one_row.id_bedrock;
		            END IF;

		            IF one_row.fracture_spacing IS NOT NULL THEN
						value_query =  value_query || ', fracture_spacing';
						value_values = value_values || ', ' || quote_literal(one_row.fracture_spacing);
		            END IF;

		            IF one_row.fault_zone IS NOT NULL THEN
						value_query =  value_query || ', fault_zone';
						value_values = value_values || ', ' || quote_literal(one_row.fault_zone);
		            END IF;

		            IF one_row.distance_to_fault IS NOT NULL THEN
						value_query =  value_query || ', distance_to_fault';
						value_values = value_values || ', ' || quote_literal(one_row.distance_to_fault);
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM geological WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- station_type
		        FOR one_row IN 
			        SELECT id, name, type FROM station_type
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE station_type IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO station_type (id, name';

					value_values = ' SELECT ' || one_row.id || ', ' || quote_literal(one_row.name);

					IF one_row.type IS NOT NULL THEN
						value_query =  value_query || ', type';
						value_values = value_values || ', ' || quote_literal(one_row.type);
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM station_type WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- monument
		        FOR one_row IN 
			        SELECT id, description, inscription, height, foundation, foundation_depth FROM monument
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE monument IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO monument (id, description';

					value_values = ' SELECT ' || one_row.id || ', ' || quote_literal(one_row.description);

					IF one_row.inscription IS NOT NULL THEN
						value_query =  value_query || ', inscription';
						value_values = value_values || ', ' || quote_literal(one_row.inscription);
		            END IF;

		            IF one_row.height IS NOT NULL THEN
						value_query =  value_query || ', height';
						value_values = value_values || ', ' || one_row.height;
		            END IF;

					IF one_row.foundation IS NOT NULL THEN
						value_query =  value_query || ', foundation';
						value_values = value_values || ', ' || quote_literal(one_row.foundation);
		            END IF;

		            IF one_row.foundation_depth IS NOT NULL THEN
						value_query =  value_query || ', foundation_depth';
						value_values = value_values || ', ' || one_row.foundation_depth;
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM monument WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- coordinates
		        FOR one_row IN 
			        SELECT id, x, y, z, lat, lon, altitude FROM coordinates
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE coordinates IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO coordinates (id, x, y, z';
					
					value_values = ' SELECT ' || one_row.id || ', ' || one_row.x || ', ' || one_row.y || ', ' || one_row.z;

					IF one_row.lat IS NOT NULL THEN
						value_query =  value_query || ', lat';
						value_values = value_values || ', ' || one_row.lat;
		            END IF;

					IF one_row.lon IS NOT NULL THEN
						value_query =  value_query || ', lon';
						value_values = value_values || ', ' || one_row.lon;
		            END IF;

					IF one_row.altitude IS NOT NULL THEN
						value_query =  value_query || ', altitude';
						value_values = value_values || ', ' || one_row.altitude;
		            END IF;		            

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM coordinates WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- tectonic
		        FOR one_row IN 
			        SELECT id, plate_name FROM tectonic
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE tectonic IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO tectonic (id, plate_name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.plate_name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM tectonic WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- country
		        FOR one_row IN 
			        SELECT id, name, iso_code FROM country
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE country IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO country (id, name, iso_code) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.name) || ', ' || quote_literal(one_row.iso_code) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM country WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- state
		        FOR one_row IN 
			        SELECT id, id_country, name FROM state
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE state IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO state (id, id_country, name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || one_row.id_country || ', ' || quote_literal(one_row.name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM state WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- city
		        FOR one_row IN 
			        SELECT id, id_state, name FROM city
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE city IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO city (id, id_state, name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || one_row.id_state || ', ' || quote_literal(one_row.name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM city WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- location
		        FOR one_row IN 
			        SELECT id, id_city, id_coordinates, id_tectonic, description FROM location
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE location IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO location ( id, id_city, id_coordinates';

					value_values = ' SELECT ' || one_row.id || ', ' || one_row.id_city || ', ' || one_row.id_coordinates;

					IF one_row.id_tectonic IS NOT NULL THEN
						value_query =  value_query || ', id_tectonic';
						value_values = value_values || ', ' || one_row.id_tectonic;
		            END IF;

					IF one_row.description IS NOT NULL THEN
						value_query =  value_query || ', description';
						value_values = value_values || ', ' || quote_literal(one_row.description);
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM location WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
			-- STATIONS END --
			
			-- LOCAL CONDITIONS AND EFFECTS BEGIN --
				-- effects
		        FOR one_row IN 
			        SELECT id, type FROM effects
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE effects IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO effects (id, type) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.type) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM effects WHERE id = ' || quote_literal(one_row.id) || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
			-- LOCAL CONDITIONS AND EFFECTS END --

           	RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            -- If the connection is removed, also remove the T2 connection on the remote node;
            IF OLD.station IS NOT NULL THEN
                -- After removing the connection, you should also delete the station from the DGW
                -- The removal of the station, in the remote node, should not be done automatically
                value_query = 'DELETE FROM connections WHERE station =' || OLD.station;
                INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NULL, OLD.destiny);
            END IF;
            RETURN NULL;
        END IF;

    END;
$$;

DROP TRIGGER IF EXISTS sync_connections_t1 on connections;
CREATE TRIGGER sync_connections_t1 AFTER INSERT OR UPDATE OR DELETE ON connections
    FOR EACH ROW EXECUTE PROCEDURE sync_connections_t1();create or replace function sync_contact_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_contact integer;
	    name text;
	    title text;
	    email text;
	    phone text;
	    gsm text;
	    comment text;
	    id_agency integer;
	    role text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_contact = NEW.id;
            name = NEW.name;
            title = NEW.title;
            email = NEW.email;
            phone = NEW.phone;
            gsm = NEW.gsm;
            comment = NEW.comment;
            id_agency = NEW.id_agency;
            role = NEW.role;

			-- Create query
            value_query = 'INSERT INTO contact (id, name, id_agency';

			IF title IS NOT NULL THEN
                value_query = value_query || ', title';
            END IF;

			IF email IS NOT NULL THEN
                value_query = value_query || ', email';
            ELSE
                 raise notice 'Warning On Inserting new Contact - Email Can Not Be NUll';
            END IF;

			IF phone IS NOT NULL THEN
                value_query = value_query || ', phone';
            END IF;

			IF gsm IS NOT NULL THEN
                value_query = value_query || ', gsm';
            END IF;

			IF comment IS NOT NULL THEN
                value_query = value_query || ', comment';
            END IF;

			IF role IS NOT NULL THEN
                value_query = value_query || ', role';
            END IF;

            --start to add the values
			value_query = value_query || ') VALUES (' || id_contact || ', ' || quote_literal(name) || ', ' || id_agency;

			IF title IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(title);
            END IF;

			IF email IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(email);
            END IF;

			IF phone IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(phone);
            END IF;

			IF gsm IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(gsm);
            END IF;

			IF comment IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(comment);
            END IF;

			IF role IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(role);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        -- 7/2/24 Permite NULLS for non mandatory fields
        -- 7/2/42 raise error message when trying to change mandatory or unique fields to null
        IF (TG_OP = 'UPDATE') THEN
            id_contact = OLD.id;
			name = NEW.name;
            title = NEW.title;
            email = NEW.email;
            phone = NEW.phone;
            gsm = NEW.gsm;
            comment = NEW.comment;
            id_agency = NEW.id_agency;
            role = NEW.role;
			value_first = true;

			-- Create query
            value_query = 'UPDATE contact SET ';

			-- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name= ' || quote_literal(name);
                value_first = false;
            ELSE
                raise notice 'Warning On Update Contact - name Can Not Be NUll';
            END IF;

			-- Handle NULL values
			-- Added possibility to edit id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

			-- Permit NULL values and alter only on change of value
            IF NEW.title != OLD.title then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'title = ' || quote_literal(title);
            END IF;

			-- Handle NULL values
            IF NEW.email IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'email = ' || quote_literal(email);
            ELSE
                raise notice 'Warning On Update Contact - Email Can Not Be NUll';
            END IF;

			-- Permit NULL values and alter only on change of value
            IF NEW.phone != OLD.phone then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'phone = ' || quote_literal(phone);
            END IF;

			-- Permit NULL values and alter only on change of value
            IF NEW.gsm != OLD.gsm then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'gsm = ' || quote_literal(gsm);
            END IF;

			-- Permit NULL values and alter only on change of value
            IF NEW.comment != OLD.comment then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'comment = ' || quote_literal(comment);
            END IF;

			-- Handle NULL values
            IF NEW.id_agency IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_agency =' || id_agency;
            ELSE
                raise notice 'Warning On Update Contact - Email if agency can not be null';
            END IF;

			-- Permit NULL values and alter only on change of value
            IF NEW.role != OLD.role then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'role = ' || quote_literal(role);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_contact || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_contact = OLD.id;

            -- Create query
            value_query = 'DELETE FROM contact WHERE id=' || id_contact || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_contact_t1 on contact;
CREATE TRIGGER sync_contact_t1 AFTER INSERT OR UPDATE OR DELETE ON contact
    FOR EACH ROW EXECUTE PROCEDURE sync_contact_t1();

create or replace function sync_coordinates_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_coordinates integer;
	    x numeric;
	    y numeric;
	    z numeric;
	    lat numeric;
	    lon numeric;
	    altitude numeric;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_coordinates = NEW.id;
            x = NEW.x;
            y = NEW.y;
            z = NEW.z;
            lat = NEW.lat;
            lon = NEW.lon;
            altitude = NEW.altitude;

			-- Create query
            value_query = 'INSERT INTO coordinates (id, x, y, z';

			IF lat IS NOT NULL THEN
                value_query = value_query || ', lat';
            END IF;

			IF lon IS NOT NULL THEN
                value_query = value_query || ', lon';
            END IF;

			IF altitude IS NOT NULL THEN
                value_query = value_query || ', altitude';
            END IF;

            value_query = value_query || ') VALUES (' || id_coordinates || ', ' || x || ', ' || y || ', ' || z;

			IF lat IS NOT NULL THEN
                value_query = value_query || ', ' || lat;
            END IF;

			IF lon IS NOT NULL THEN
                value_query = value_query || ', ' || lon;
            END IF;

			IF altitude IS NOT NULL THEN
                value_query = value_query || ', ' || altitude;
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_coordinates = OLD.id;
			x = NEW.x;
            y = NEW.y;
            z = NEW.z;
            lat = NEW.lat;
            lon = NEW.lon;
            altitude = NEW.altitude;
			value_first = true;

			-- Create query
            value_query = 'UPDATE coordinates SET ';

			-- Handle NULL values
            IF NEW.x IS NOT NULL THEN
                value_query = value_query || 'x=' || x;
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Add possibility to change ID
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_query = false;
                END IF;
                value_query = value_query || 'id =' || NEW.id;
            END IF;

			-- Handle NULL values
            IF NEW.y IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_query = false;
                END IF;
                value_query = value_query || 'y =' || y;
            END IF;

			-- Handle NULL values
            IF NEW.z IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'z =' || z;
            END IF;

			-- Handle NULL values
            IF NEW.lat IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_query = false;
                END IF;
                value_query = value_query || 'lat =' || lat;
            END IF;

			-- Handle NULL values
            IF NEW.lon IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_query = false;
                END IF;
                value_query = value_query || 'lon =' || lon;
            END IF;

			-- Handle NULL values
            IF NEW.altitude IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_query = false;
                END IF;
                value_query = value_query || 'altitude =' || altitude;
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_coordinates || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_coordinates = OLD.id;

            -- Create query
            value_query = 'DELETE FROM coordinates WHERE id=' || id_coordinates || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_coordinates_t1 on coordinates;
CREATE TRIGGER sync_coordinates_t1 AFTER INSERT OR UPDATE OR DELETE ON coordinates
    FOR EACH ROW EXECUTE PROCEDURE sync_coordinates_t1();

create or replace function sync_country_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_country integer;
	    name text;
	    iso_code text;

	    old_name text;
        old_iso_code text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_country = NEW.id;
            name = NEW.name;
            iso_code = NEW.iso_code;

            -- Create query
            value_query = 'INSERT INTO country (id, name, iso_code) VALUES ' ||
                '(' || id_country || ', ' || quote_literal(name) || ', ' || quote_literal(iso_code) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_country = OLD.id;
			name = NEW.name;
            iso_code = NEW.iso_code;
			value_first = true;

			 -- Create query
            value_query = 'UPDATE country SET ';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
            END IF;

			-- Handle NULL values
			-- Added possibility to edit id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

			-- Handle NULL values
            IF NEW.iso_code IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'iso_code =' || quote_literal(iso_code);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_country || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_country = OLD.id;

            -- Create query
            value_query = 'DELETE FROM country WHERE id=' || id_country || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_country_t1 on country;
CREATE TRIGGER sync_country_t1 AFTER INSERT OR UPDATE OR DELETE ON country
    FOR EACH ROW EXECUTE PROCEDURE sync_country_t1();


CREATE OR REPLACE FUNCTION sync_data_center_structure_t1() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
    value_query text;
    value_metadata text;
    value_stationID integer;
    value_first boolean;

    id_data_center_structure integer;
    id_data_center_ integer;
    id_file_type integer;
    directory_naming text;
    comments text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_data_center_structure = NEW.id;
            id_data_center_ = NEW.id_data_center;
            id_file_type = NEW.id_file_type;
            directory_naming = NEW.directory_naming;
            comments = NEW.comments;

            -- Create query
            value_query = 'INSERT INTO data_center_structure (id, id_data_center, id_file_type, directory_naming';

            IF comments IS NOT NULL THEN
                value_query = value_query || ', comments';
            END IF;

            value_query = value_query || ') VALUES (' || id_data_center_structure || ', ' || id_data_center_ || ', ' || id_file_type|| ', ' || quote_literal(directory_naming);

            IF comments IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(comments);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, value_metadata, value_stationID,  (SELECT id_node FROM node_data_center WHERE id_data_center=id_data_center_));

            -- Update the previous queries related to the insertion/update/delete of a new data center
            UPDATE queries SET destiny = (SELECT id_node FROM node_data_center WHERE id_data_center=id_data_center_) WHERE query ILIKE '% data_center % VALUES (' || id_data_center_ || '%';

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_data_center_structure = OLD.id;
            id_data_center_ = NEW.id_data_center;
            id_file_type = NEW.id_file_type;
            directory_naming = NEW.directory_naming;
            comments = NEW.comments;

            -- Create query
            -- Added possibility to update id
            value_query = 'UPDATE data_center_structure SET id=' || NEW.id || ', id_data_center=' || id_data_center_ || ', id_file_type=' || id_file_type || ', directory_naming=' || quote_literal(directory_naming);

            -- Handle NULL values
            IF NEW.comments IS NOT NULL THEN
                value_query = value_query || ', comments=' || quote_literal(comments);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_data_center_structure || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, value_metadata, value_stationID,  (SELECT id_node FROM node_data_center WHERE id_data_center=id_data_center_));

            -- Update the previous queries related to the insertion/update/delete of a new data center
            UPDATE queries SET destiny = (SELECT id_node FROM node_data_center WHERE id_data_center=id_data_center_) WHERE query ILIKE '% data_center % VALUES (' || id_data_center_ || '%';


            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_data_center_structure = OLD.id;

            -- Create query
            value_query = 'DELETE FROM data_center_structure WHERE id=' || id_data_center_structure || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$;

DROP TRIGGER IF EXISTS sync_data_center_structure_t1 on data_center_structure;
CREATE TRIGGER sync_data_center_structure_t1 AFTER INSERT OR UPDATE OR DELETE ON data_center_structure
    FOR EACH ROW EXECUTE PROCEDURE sync_data_center_structure_t1();


CREATE OR REPLACE FUNCTION sync_data_center_t1() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
    value_query text;
    value_metadata text;
    value_stationID integer;
    value_first boolean;

    id_data_center integer;
    id_agency integer;
    name text;
    acronym text;
    protocol text;
    hostname text;
    root_path text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_data_center = NEW.id;
            id_agency = NEW.id_agency;
            name = NEW.name;
            acronym = NEW.acronym;
            protocol = NEW.protocol;
            hostname = NEW.hostname;
            root_path = NEW.root_path;

            -- Create query
            value_query = 'INSERT INTO data_center (id, name, acronym, protocol, hostname, root_path';

            IF id_agency IS NOT NULL THEN
                value_query = value_query || ', id_agency';
            END IF;

            value_query = value_query || ') VALUES (' || id_data_center || ', ' || quote_literal(name) || ', ' || quote_literal(acronym) || ', ' || quote_literal(protocol) || ', ' || quote_literal(hostname) || ', ' || quote_literal(root_path);

            IF id_agency IS NOT NULL THEN
                value_query = value_query || ', ' || id_agency;
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_data_center = OLD.id;
            id_agency = NEW.id_agency;
            name = NEW.name;
            acronym = NEW.acronym;
            protocol = NEW.protocol;
            hostname = NEW.hostname;
            root_path = NEW.root_path;
            value_first = true;

            -- Create query
            -- Added possibility to update id
            value_query = 'UPDATE data_center SET id=' || NEW.id || ', name=' || quote_literal(name) || ', acronym=' || quote_literal(acronym) || ', protocol=' || quote_literal(protocol) || ', hostname=' || quote_literal(hostname) || ', root_path=' || quote_literal(root_path);

            -- Handle NULL values
            IF NEW.id_agency IS NOT NULL THEN
                value_query = value_query || ', id_agency=' || id_agency;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_data_center || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_data_center = OLD.id;

            -- Create query
            value_query = 'DELETE FROM data_center WHERE id=' || id_data_center || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$;

DROP TRIGGER IF EXISTS sync_data_center_t1 on data_center;
CREATE TRIGGER sync_data_center_t1 AFTER INSERT OR UPDATE OR DELETE ON data_center
    FOR EACH ROW EXECUTE PROCEDURE sync_data_center_t1();


CREATE OR REPLACE FUNCTION sync_datacenter_station_t1() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
    value_query text;
    value_metadata text;
    value_stationID integer;
    value_first boolean;

    id_datacenter_station integer;
    id_station integer;
    id_datacenter integer;
    datacenter_type text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_datacenter_station = NEW.id;
            id_station = NEW.id_station;
            id_datacenter = NEW.id_datacenter;
            datacenter_type = NEW.datacenter_type;

            IF id_station IS NULL THEN
                RETURN NULL;
            END IF;
        
            -- Create query
            value_query = 'INSERT INTO datacenter_station (id';

            IF id_station IS NOT NULL THEN
                value_query = value_query || ', id_station';
            END IF;

            IF id_datacenter IS NOT NULL THEN
                value_query = value_query || ', id_datacenter';
            END IF;

            IF datacenter_type IS NOT NULL THEN
                value_query = value_query || ', datacenter_type';
            END IF;
            -- Bug fix the id was previously id_station
            value_query = value_query || ') VALUES (' || id_datacenter_station;

            IF id_station IS NOT NULL THEN
                value_query = value_query || ', ' || id_station;
            END IF;

            IF id_datacenter IS NOT NULL THEN
                value_query = value_query || ', ' || id_datacenter;
            END IF;

            IF datacenter_type IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(datacenter_type);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            IF id_station IS NOT NULL THEN
                -- Set station code
                value_stationID = id_station;
            END IF;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_datacenter_station = OLD.id;
            id_station = NEW.id_station;
            id_datacenter = NEW.id_datacenter;
            datacenter_type = NEW.datacenter_type;

            IF id_station IS NULL THEN
                RETURN NULL;
            END IF;

            -- If this update introduces a different station than the previous one for the current record, then we must remove
            -- the entry associated with the old station from the remote node;
            IF (OLD.id_station != NEW.id_station) THEN
                value_query = 'DELETE FROM datacenter_station WHERE id=' || id_datacenter_station || ';';
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, 'T1', OLD.id_station);
            END IF;

            -- Create query
            value_query = 'DO ' || chr(36) || '' || chr(36) || ' BEGIN IF EXISTS (SELECT * FROM datacenter_station WHERE id=' || OLD.id ||') THEN ';

            -- Added possibility to update id
            value_query = value_query || 'UPDATE datacenter_station SET id=' || NEW.id;

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || ', id_station=' || id_station;
            END IF;

            IF NEW.id_datacenter IS NOT NULL THEN
                value_query = value_query || ', id_datacenter=' || id_datacenter;
            END IF;

            IF NEW.datacenter_type IS NOT NULL THEN
                value_query = value_query || ', datacenter_type=' || quote_literal(datacenter_type);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_datacenter_station || ';';

            value_query = value_query || ' ELSE ';

            -- Create query
            value_query = value_query || 'INSERT INTO datacenter_station (id';

            IF id_station IS NOT NULL THEN
                value_query = value_query || ', id_station';
            END IF;

            IF id_datacenter IS NOT NULL THEN
                value_query = value_query || ', id_datacenter';
            END IF;

            IF datacenter_type IS NOT NULL THEN
                value_query = value_query || ', datacenter_type';
            END IF;

            value_query = value_query || ') VALUES (' || NEW.id;

            IF id_station IS NOT NULL THEN
                value_query = value_query || ', ' || id_station;
            END IF;

            IF id_datacenter IS NOT NULL THEN
                value_query = value_query || ', ' || id_datacenter;
            END IF;

            IF datacenter_type IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(datacenter_type);
            END IF;

            value_query = value_query || '); END IF; END; ' || chr(36) || '' || chr(36) || ';';

            -- Set metadata type
            value_metadata = 'T1';

            IF id_station IS NOT NULL THEN
                -- Set station code
                value_stationID = id_station;
            END IF;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);


            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_datacenter_station = OLD.id;
            id_station = OLD.id_station;

            -- Create query
            value_query = 'DELETE FROM datacenter_station WHERE id=' || id_datacenter_station || ';';

            -- Set metadata type
            value_metadata = 'T1';

            IF id_station IS NOT NULL THEN
                -- Set station code
                value_stationID = id_station;
            END IF;

            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;

            RETURN NULL;
        END IF;
    END;
$$;

DROP TRIGGER IF EXISTS sync_datacenter_station_t1 on datacenter_station;
CREATE TRIGGER sync_datacenter_station_t1 AFTER INSERT OR UPDATE OR DELETE ON datacenter_station
    FOR EACH ROW EXECUTE PROCEDURE sync_datacenter_station_t1();


create or replace function sync_document_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_document integer;
	    date_ date;
	    title text;
	    description text;
	    link text;
	    id_station integer;
	    id_item integer;
	    id_document_type integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_document = NEW.id;
            date_ = NEW.date;
            title = NEW.title;
            description = NEW.description;
            link = NEW.link;
            id_station = NEW.id_station;
            id_item = NEW.id_item;
            id_document_type = NEW.id_document_type;

            -- Create query
            value_query = 'INSERT INTO document (id, date, title, link, id_station, id_item, id_document_type';

			IF description IS NOT NULL THEN
                value_query = value_query || ', description';
            END IF;

			 value_query = value_query || ') VALUES (' || id_document || ', ' || quote_literal(date_) || ', ' || quote_literal(title) || ', ' ||
				quote_literal(link) || ', ' || id_station || ', ' || id_item || ', ' || id_document_type;

			IF description IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(description);
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_document = OLD.id;
			      date_ = NEW.date;
            title = NEW.title;
            description = NEW.description;
            link = NEW.link;
            id_station = NEW.id_station;
            id_item = NEW.id_item;
            id_document_type = NEW.id_document_type;
			      value_first = true;

      --Create query
           value_query = 'UPDATE document SET ';

			-- Handle NULL values
            IF NEW.date IS NOT NULL THEN
                value_query = value_query || 'date=' || quote_literal(date_);
                value_first = false;
            END IF;

			-- Handle NULL values
            IF NEW.title IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'title =' || quote_literal(title);
            END IF;

			-- Handle NULL values
            IF NEW.description IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'description =' || quote_literal(description);
            END IF;

			-- Handle NULL values
            IF NEW.link IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'link =' || quote_literal(link);
            END IF;

			-- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_station =' || id_station;
            END IF;

			-- Handle NULL values
            IF NEW.id_item IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_item =' || id_item;
            END IF;

			-- Handle NULL values
            IF NEW.id_document_type IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_document_type =' || id_document_type;
            END IF;

			 -- Create query
            value_query = value_query || ' WHERE id=' || id_document || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_document = OLD.id;

            -- Create query
            value_query = 'DELETE FROM document WHERE id=' || id_document || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = OLD.id_station;

            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_document_t1 on document;
CREATE TRIGGER sync_document_t1 AFTER INSERT OR UPDATE OR DELETE ON document
    FOR EACH ROW EXECUTE PROCEDURE sync_document_t1();


create or replace function sync_document_type_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_document_type integer;
	    name text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_document_type = NEW.id;
            name = NEW.name;

            -- Create query
            value_query = 'INSERT INTO document_type (id, name) VALUES ' ||
                '(' || id_document_type || ', ' || quote_literal(name) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_document_type = OLD.id;
            name = NEW.name;
			value_first = true;

            -- Create query
            -- Added possibility to update ID
            value_query = 'UPDATE document_type SET id=' || NEW.id || ', name=' || quote_literal(name) || ' WHERE id=' || id_document_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_document_type = OLD.id;

            -- Create query
            value_query = 'DELETE FROM document_type WHERE id=' || id_document_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_document_type_t1 on document_type;
CREATE TRIGGER sync_document_type_t1 AFTER INSERT OR UPDATE OR DELETE ON document_type
    FOR EACH ROW EXECUTE PROCEDURE sync_document_type_t1();

    
create or replace function sync_effects_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_effects integer;
	    type text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_effects = NEW.id;
            type = NEW.type;

            -- Create query
            value_query = 'INSERT INTO effects (id, type) VALUES ' ||
                '(' || id_effects || ', ' || quote_literal(type) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_effects = OLD.id;
            type = NEW.type;
			value_first = true;

            -- Create query
            -- Added possibility to update ID
            value_query = 'UPDATE effects SET id=' || NEW.id ||', type=' || quote_literal(type) || ' WHERE id=' || id_effects || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_effects = OLD.id;

            -- Create query
            value_query = 'DELETE FROM effects WHERE id=' || id_effects || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
			
            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_effects_t1 on effects;
CREATE TRIGGER sync_effects_t1 AFTER INSERT OR UPDATE OR DELETE ON effects
    FOR EACH ROW EXECUTE PROCEDURE sync_effects_t1();

create or replace function sync_file_type_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_file_type integer;
	    format text;
      sampling_window text;
	    sampling_frequency text;

    BEGIN
      -- Handle insert
      IF (TG_OP = 'INSERT') THEN
          id_file_type = NEW.id;
          format = NEW.format;
          sampling_window = new.sampling_window;
          sampling_frequency = new.sampling_frequency;

			-- Create query
      value_query = 'INSERT INTO file_type (id, format';

			IF sampling_window IS NOT NULL THEN
        value_query = value_query || ', sampling_window';
      END IF;

      IF sampling_frequency IS NOT NULL THEN
        value_query = value_query || ', sampling_frequency';
      END IF;

			value_query = value_query || ') VALUES (' || id_file_type || ', ' || quote_literal(format);

			IF sampling_window  IS NOT NULL THEN
        value_query = value_query || ', ' || quote_literal(sampling_window);
      END IF;

      IF sampling_frequency IS NOT NULL THEN
        value_query = value_query || ', ' || quote_literal(sampling_frequency);
      END IF;

			 value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

      -- Handle update
      IF (TG_OP = 'UPDATE') THEN
            id_file_type = OLD.id;
            format = NEW.format;
            sampling_window = NEW.sampling_window;
            sampling_frequency = NEW.sampling_frequency;
			      value_first = true;

			-- Create query
            value_query = 'UPDATE file_type SET ';

            -- bug fixes, all set columns were format ?!

            -- Handle NULL values
            IF NEW.id IS NOT NULL THEN
                value_query = value_query || 'id =' || NEW.id;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.format IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'format =' || quote_literal(format);
            END IF;

            IF sampling_window IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'sampling_window =' || quote_literal(sampling_window);
            END IF;

            IF sampling_frequency IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'sampling_frequency =' || quote_literal(sampling_frequency);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_file_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

      -- Handle delete
      IF (TG_OP = 'DELETE') THEN
            id_file_type = OLD.id;

            -- Create query
            value_query = 'DELETE FROM file_type WHERE id=' || id_file_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_file_type_t1 on file_type;
CREATE TRIGGER sync_file_type_t1 AFTER INSERT OR UPDATE OR DELETE ON file_type
    FOR EACH ROW EXECUTE PROCEDURE sync_file_type_t1();

create or replace function sync_geological_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_geological integer;
	    id_bedrock integer;
	    characteristic text;
	    fracture_spacing text;
	    fault_zone text;
	    distance_to_fault text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_geological = NEW.id;
            id_bedrock = NEW.id_bedrock;
            characteristic = NEW.characteristic;
            fracture_spacing = NEW.fracture_spacing;
            fault_zone = NEW.fault_zone;
            distance_to_fault = NEW.distance_to_fault;

			-- Create query
            value_query = 'INSERT INTO geological (id, characteristic';

			IF id_bedrock IS NOT NULL THEN
                value_query = value_query || ', id_bedrock';
            END IF;

			IF fracture_spacing IS NOT NULL THEN
                value_query = value_query || ', fracture_spacing';
            END IF;

			IF fault_zone IS NOT NULL THEN
                value_query = value_query || ', fault_zone';
            END IF;

			IF distance_to_fault IS NOT NULL THEN
                value_query = value_query || ', distance_to_fault';
            END IF;

			value_query = value_query || ') VALUES (' || id_geological || ', ' || quote_literal(characteristic);

			IF id_bedrock IS NOT NULL THEN
                value_query = value_query || ', ' || id_bedrock;
            END IF;

			IF fracture_spacing IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(fracture_spacing);
            END IF;

			IF fault_zone IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(fault_zone);
            END IF;

			IF distance_to_fault IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(distance_to_fault);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_geological = OLD.id;
			      id_bedrock = NEW.id_bedrock;
            characteristic = NEW.characteristic;
            fracture_spacing = NEW.fracture_spacing;
            fault_zone = NEW.fault_zone;
            distance_to_fault = NEW.distance_to_fault;
			      value_first = true;

            -- Create query
            value_query = 'UPDATE geological SET ';

            -- Handle NULL values
            IF NEW.id_bedrock IS NOT NULL THEN
                value_query = value_query || 'id_bedrock=' || id_bedrock;
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Added possibility to update ID
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

            -- Handle NULL values
            IF NEW.characteristic IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'characteristic =' || quote_literal(characteristic);
            END IF;

			-- Handle NULL values
            IF NEW.fracture_spacing IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'fracture_spacing =' || quote_literal(fracture_spacing);
            END IF;

			-- Handle NULL values
            IF NEW.fault_zone IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'fault_zone =' || quote_literal(fault_zone);
            END IF;

			-- Handle NULL values
            IF NEW.distance_to_fault IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'distance_to_fault =' || quote_literal(distance_to_fault);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_geological || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_geological = OLD.id;

            -- Create query
            value_query = 'DELETE FROM geological WHERE id=' || id_geological || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_geological_t1 on geological;
CREATE TRIGGER sync_geological_t1 AFTER INSERT OR UPDATE OR DELETE ON geological
    FOR EACH ROW EXECUTE PROCEDURE sync_geological_t1();

create or replace function sync_instrument_collocation_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_instrument_collocation integer;
	    id_station integer;
	    type text;
        status text;
	    date_from timestamp;
	    date_to timestamp;
	    comment text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_instrument_collocation = NEW.id;
            id_station = NEW.id_station;
            type = NEW.type;
            status = NEW.status;
			date_from = NEW.date_from;
			date_to = NEW.date_to;
			comment = NEW.comment;

			-- Create query
            value_query = 'INSERT INTO instrument_collocation (id, id_station, type';

            IF status IS NOT NULL THEN
                value_query = value_query || ', status';
            END IF;

			IF date_from IS NOT NULL THEN
                value_query = value_query || ', date_from';
            END IF;

			IF date_to IS NOT NULL THEN
                value_query = value_query || ', date_to';
            END IF;

			IF comment IS NOT NULL THEN
                value_query = value_query || ', comment';
            END IF;

			value_query = value_query || ') VALUES (' || id_instrument_collocation || ', ' || id_station || ', ' || quote_literal(type);

            IF status IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(status);
            END IF;

			IF date_from IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_from);
            END IF;

			IF date_to IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_to);
            END IF;

			IF comment IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(comment);
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_instrument_collocation = OLD.id;
			id_station = NEW.id_station;
            type = NEW.type;
            status = NEW.status;
			date_from = NEW.date_from;
			date_to = NEW.date_to;
			comment = NEW.comment;
			value_first = true;

			-- Create query
            value_query = 'UPDATE instrument_collocation SET ';

			-- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || id_station;
                value_first = false;
            END IF;

			-- Handle NULL values
            IF NEW.type IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'type =' || quote_literal(type);
            END IF;

            -- Handle NULL values
            IF NEW.status IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'status =' || quote_literal(status);
            END IF;

			-- Handle NULL values
            IF NEW.date_from IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_from =' || quote_literal(date_from);
            END IF;

			-- Handle NULL values
            IF NEW.date_to IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_to =' || quote_literal(date_to);
            END IF;

			-- Handle NULL values
            IF NEW.comment IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'comment =' || quote_literal(comment);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_instrument_collocation || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_instrument_collocation = OLD.id;
            id_station = OLD.id_station;
            -- Create query
            value_query = 'DELETE FROM instrument_collocation WHERE id=' || id_instrument_collocation || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            -- Bug fix it was id_instrument_collocation it must be id_station!
            value_stationID = id_station;


            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_instrument_collocation_t1 on instrument_collocation;
CREATE TRIGGER sync_instrument_collocation_t1 AFTER INSERT OR UPDATE OR DELETE ON instrument_collocation
    FOR EACH ROW EXECUTE PROCEDURE sync_instrument_collocation_t1();

    create or replace function sync_item_attribute_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
	    value_metadata text;
        value_stationID integer;
		value_first boolean;

        id_item_attribute integer;
	    id_item integer;
	    id_attribute integer;
	    date_from timestamp;
	    date_to timestamp;
	    value_varchar text;
	    value_date date;
	    value_numeric numeric;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_item_attribute = NEW.id;
            id_item = NEW.id_item;
            id_attribute = NEW.id_attribute;
            date_from = NEW.date_from;
            date_to = NEW.date_to;
            value_varchar = NEW.value_varchar;
            value_date = NEW.value_date;
            value_numeric = NEW.value_numeric;

			-- Create query
            value_query = 'INSERT INTO item_attribute (id, id_item, id_attribute';

            IF date_from IS NOT NULL THEN
                value_query = value_query || ', date_from';
            END IF;

			IF date_to IS NOT NULL THEN
                value_query = value_query || ', date_to';
            END IF;

			IF value_varchar IS NOT NULL THEN
                value_query = value_query || ', value_varchar';
            END IF;

			IF value_date IS NOT NULL THEN
                value_query = value_query || ', value_date';
            END IF;

			IF value_numeric IS NOT NULL THEN
                value_query = value_query || ', value_numeric';
            END IF;

			value_query = value_query || ') VALUES (' || id_item_attribute || ', ' || id_item || ', ' || id_attribute;

			IF date_from IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_from);
            END IF;

			IF date_to IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_to);
            END IF;

			IF value_varchar IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(value_varchar);
            END IF;

			IF value_date IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(value_date);
            END IF;

			IF value_numeric IS NOT NULL THEN
                value_query = value_query || ', ' || value_numeric;
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_item_attribute = OLD.id;
            id_item = OLD.id_item;
            id_attribute = OLD.id_attribute;
            date_from = NEW.date_from;
            date_to = NEW.date_to;
            value_varchar = NEW.value_varchar;
            value_date = NEW.value_date;
            value_numeric = NEW.value_numeric;
			      value_first = true;

			-- Create query
            value_query = 'UPDATE item_attribute SET ';

			-- Handle NULL values
            IF NEW.date_from IS NOT NULL THEN
                value_query = value_query || 'date_from=' || quote_literal(date_from);
                value_first = false;
            END IF;

			-- Handle NULL values
			-- Added possibility to edit id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

			-- Handle NULL values
			-- Added id_item that was missing
            IF NEW.id_item IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_item=' || NEW.id_item;
            END IF;

			-- Handle NULL values
			-- Added id_attribute that was missing
            IF NEW.id_attribute IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_attribute=' || NEW.id_attribute;
            END IF;

			-- Handle NULL values
            IF NEW.date_to IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_to =' || quote_literal(date_to);
            END IF;

			-- Handle NULL values
            IF NEW.value_varchar IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'value_varchar =' || quote_literal(value_varchar);
            END IF;

			-- Handle NULL values
            IF NEW.value_date IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'value_date =' || quote_literal(value_date);
            END IF;

			-- Handle NULL values
            IF NEW.value_numeric IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'value_numeric =' || value_numeric;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_item_attribute || ' AND id_item=' || id_item || ' AND id_attribute=' || id_attribute || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            -- id_item = OLD.id_item;
            -- id_attribute = OLD.id_attribute;

            -- Create query
            --- value_query = 'DELETE FROM item_attribute WHERE id=' || id_item_attribute || ' AND id_item=' || id_item || ' AND id_attribute=' || id_attribute || ';';

            value_query = 'DELETE FROM item_attribute WHERE id=' || OLD.id ||  ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_item_attribute_t1 on item_attribute;
CREATE TRIGGER sync_item_attribute_t1 AFTER INSERT OR UPDATE OR DELETE ON item_attribute
    FOR EACH ROW EXECUTE PROCEDURE sync_item_attribute_t1();


create or replace function sync_item_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_item integer;
	    id_item_type integer;
	    id_contact_as_producer integer;
	    id_contact_as_owner integer;
	    comment text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_item = NEW.id;
            id_item_type = NEW.id_item_type;
            id_contact_as_producer = NEW.id_contact_as_producer;
            id_contact_as_owner = NEW.id_contact_as_owner;
            comment = NEW.comment;

			 -- Create query
            value_query = 'INSERT INTO item (id, id_item_type';

			IF id_contact_as_producer IS NOT NULL THEN
                value_query = value_query || ', id_contact_as_producer';
            END IF;

			IF id_contact_as_owner IS NOT NULL THEN
                value_query = value_query || ', id_contact_as_owner';
            END IF;

			IF comment IS NOT NULL THEN
                value_query = value_query || ', comment';
            END IF;

			value_query = value_query || ') VALUES (' || id_item || ', ' || id_item_type;

			IF id_contact_as_producer IS NOT NULL THEN
                value_query = value_query || ', ' || id_contact_as_producer;
            END IF;

			IF id_contact_as_owner IS NOT NULL THEN
                value_query = value_query || ', ' || id_contact_as_owner;
            END IF;

			IF comment IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(comment);
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_item = OLD.id;
			id_item_type = NEW.id_item_type;
            id_contact_as_producer = NEW.id_contact_as_producer;
            id_contact_as_owner = NEW.id_contact_as_owner;
            comment = NEW.comment;
			value_first = true;

			-- Create query
            value_query = 'UPDATE item SET ';

			-- Handle NULL values
            IF NEW.id_item_type IS NOT NULL THEN
                value_query = value_query || 'id_item_type=' || id_item_type;
                value_first = false;
            END IF;

			-- Handle NULL values
			-- Added possibility to edit id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

			-- Handle NULL values
            IF NEW.id_contact_as_producer IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_contact_as_producer =' || id_contact_as_producer;
            END IF;

			-- Handle NULL values
            IF NEW.id_contact_as_owner IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_contact_as_owner =' || id_contact_as_owner;
            END IF;

			-- Handle NULL values
            IF NEW.comment IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'comment =' || quote_literal(comment);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_item || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Avoid duplicate queries that can happen in cascade update
            -- Remove the duplicate
            DELETE FROM queries WHERE query ILIKE 'UPDATE item % WHERE id=141%';

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_item = OLD.id;

            -- Create query
            value_query = 'DELETE FROM item WHERE id=' || id_item || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_item_t1 on item;
CREATE TRIGGER sync_item_t1 AFTER INSERT OR UPDATE OR DELETE ON item
    FOR EACH ROW EXECUTE PROCEDURE sync_item_t1();

create or replace function sync_item_type_attribute_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_item_type_attribute integer;
	    id_item_type integer;
	    id_attribute integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_item_type_attribute = NEW.id;
            id_item_type = NEW.id_item_type;
            id_attribute = NEW.id_attribute;

            -- Create query
            value_query = 'INSERT INTO item_type_attribute (id, id_item_type, id_attribute) VALUES ' ||
                '(' || id_item_type_attribute || ', ' || id_item_type || ', ' || id_attribute || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_item_type_attribute = OLD.id;
			      id_item_type = NEW.id_item_type;
            id_attribute = NEW.id_attribute;
			      value_first = true;

			-- Create query
            value_query = 'UPDATE item_type_attribute SET ';

            -- Handle NULL values
            IF NEW.id_item_type IS NOT NULL THEN
                value_query = value_query || 'id_item_type=' || id_item_type;
                value_first = false;
            END IF;

			-- Handle NULL values
            IF NEW.id_attribute IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_attribute =' || id_attribute;
            END IF;

             -- Create query
            value_query = value_query || ' WHERE id=' || id_item_type_attribute || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_item_type_attribute = OLD.id;

            -- Create query
            value_query = 'DELETE FROM item_type_attribute WHERE id=' || id_item_type_attribute || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_item_type_attribute_t1 on item_type_attribute;
CREATE TRIGGER sync_item_type_attribute_t1 AFTER INSERT OR UPDATE OR DELETE ON item_type_attribute
    FOR EACH ROW EXECUTE PROCEDURE sync_item_type_attribute_t1();


create or replace function sync_item_type_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_item_type integer;
	    name text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_item_type = NEW.id;
            name = NEW.name;

            -- Create query
            value_query = 'INSERT INTO item_type (id, name) VALUES ' ||
                '(' || id_item_type || ', ' || quote_literal(name) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_item_type = OLD.id;
			      name = NEW.name;
			      value_first = true;

            -- Create query
            value_query = 'UPDATE item_type SET id=' || NEW.id || ',';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
            END IF;

           -- Create query
            value_query = value_query || ' WHERE id=' || id_item_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_item_type = OLD.id;

            -- Create query
            value_query = 'DELETE FROM item_type WHERE id=' || id_item_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_item_type_t1 on item_type;
CREATE TRIGGER sync_item_type_t1 AFTER INSERT OR UPDATE OR DELETE ON item_type
    FOR EACH ROW EXECUTE PROCEDURE sync_item_type_t1();


create or replace function sync_local_ties_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_local_ties integer;
	    id_station integer;
	    name text;
	    usage_ text;
	    cpd_num text;
      iers_domes text;
	    dx numeric;
	    dy numeric;
	    dz numeric;
	    accuracy numeric;
	    survey_method text;
	    date_at date;
	    comment text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_local_ties = NEW.id;
            id_station = NEW.id_station;
            name = NEW.name;
            usage_ = NEW.usage;
            cpd_num = NEW.cpd_num;
            iers_domes = NEW.iers_domes;
            dx = NEW.dx;
            dy = NEW.dy;
            dz = NEW.dz;
            accuracy = NEW.accuracy;
            survey_method = NEW.survey_method;
            date_at = NEW.date_at;
            comment = NEW.comment;

            -- Create query
            value_query = 'INSERT INTO local_ties (id, id_station, name';

            IF usage_ IS NOT NULL THEN
                value_query = value_query || ', usage';
            END IF;

            IF cpd_num IS NOT NULL THEN
                value_query = value_query || ', cpd_num';
            END IF;

            IF iers_domes  IS NOT NULL THEN
                value_query = value_query || ', iers_domes';
            END IF;

            IF dx IS NOT NULL THEN
                value_query = value_query || ', dx';
            END IF;

            IF dy IS NOT NULL THEN
                value_query = value_query || ', dy';
            END IF;

            IF dz IS NOT NULL THEN
                value_query = value_query || ', dz';
            END IF;

            IF accuracy IS NOT NULL THEN
                value_query = value_query || ', accuracy';
            END IF;

            IF survey_method IS NOT NULL THEN
                value_query = value_query || ', survey_method';
            END IF;

            IF date_at IS NOT NULL THEN
                value_query = value_query || ', date_at';
            END IF;

            IF comment IS NOT NULL THEN
                value_query = value_query || ', comment';
            END IF;

            value_query = value_query || ') VALUES (' || id_local_ties || ', ' || id_station || ', ' || quote_literal(name);

            IF usage_ IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(usage_);
            END IF;

            IF cpd_num IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(cpd_num);
            END IF;

            IF iers_domes IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(iers_domes);
            END IF;

            IF dx IS NOT NULL THEN
                value_query = value_query || ', ' || dx;
            END IF;

            IF dy IS NOT NULL THEN
                value_query = value_query || ', ' || dy;
            END IF;

            IF dz IS NOT NULL THEN
                value_query = value_query || ', ' || dz;
            END IF;

            IF accuracy IS NOT NULL THEN
                value_query = value_query || ', ' || accuracy;
            END IF;

            IF survey_method IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(survey_method);
            END IF;

            IF date_at IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_at);
            END IF;

            IF comment IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(comment);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_local_ties = OLD.id;
            id_station = NEW.id_station;
            name = NEW.name;
            usage_ = NEW.usage;
            cpd_num = NEW.cpd_num;
            iers_domes = NEW.iers_domes;
            dx = NEW.dx;
            dy = NEW.dy;
            dz = NEW.dz;
            accuracy = NEW.accuracy;
            survey_method = NEW.survey_method;
            date_at = NEW.date_at;
            comment = NEW.comment;
            value_first = true;

            -- Create query
            value_query = 'UPDATE local_ties SET ';

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || id_station;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'name =' || quote_literal(name);
            END IF;

            -- Handle NULL values
            IF NEW.usage IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'usage =' || quote_literal(usage_);
            END IF;

            -- Handle NULL values
            IF NEW.cpd_num IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'cpd_num =' || quote_literal(cpd_num);
            END IF;

            IF NEW.iers_domes IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'iers_domes =' || quote_literal(iers_domes);
            END IF;

            -- Handle NULL values
            IF NEW.dx IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'dx =' || dx;
            END IF;

            -- Handle NULL values
            IF NEW.dy IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'dy =' || dy;
            END IF;

            -- Handle NULL values
            IF NEW.dz IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'dz =' || dz;
            END IF;

            -- Handle NULL values
            IF NEW.accuracy IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'accuracy =' || accuracy;
            END IF;

            -- Handle NULL values
            IF NEW.survey_method IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'survey_method =' || quote_literal(survey_method);
            END IF;

            -- Handle NULL values
            IF NEW.date_at IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_at =' || quote_literal(date_at);
            END IF;

            -- Handle NULL values
            IF NEW.comment IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'comment =' || quote_literal(comment);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_local_ties || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         -- Handle delete, we don't need to handle delete because the databases is cascade on delete
         IF (TG_OP = 'DELETE') THEN
           id_local_ties = OLD.id;
            -- Create query
            value_query = 'DELETE FROM local_ties WHERE id=' || id_local_ties || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = OLD.id_station;

            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;

            RETURN NULL;
         END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_local_ties_t1 on local_ties;
CREATE TRIGGER sync_local_ties_t1 AFTER INSERT OR UPDATE OR DELETE ON local_ties
    FOR EACH ROW EXECUTE PROCEDURE sync_local_ties_t1();

create or replace function sync_location_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_location integer;
	    id_city integer;
	    id_coordinates integer;
	    id_tectonic integer;
	    description text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_location = NEW.id;
            id_city = NEW.id_city;
            id_coordinates = NEW.id_coordinates;
            id_tectonic = NEW.id_tectonic;
            description = NEW.description;

            -- Create query
            value_query = 'INSERT INTO location (id, id_city, id_coordinates';

            IF id_tectonic IS NOT NULL THEN
                value_query = value_query || ', id_tectonic';
            END IF;

            IF description IS NOT NULL THEN
                value_query = value_query || ', description';
            END IF;

            value_query = value_query || ') VALUES (' || id_location || ', ' || id_city || ', ' || id_coordinates;

            IF id_tectonic IS NOT NULL THEN
                value_query = value_query || ', ' || id_tectonic;
            END IF;

            IF description IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(description);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_location = OLD.id;
            id_city = NEW.id_city;
            id_coordinates = NEW.id_coordinates;
            id_tectonic = NEW.id_tectonic;
            description = NEW.description;
            value_first = true;

            -- Create query
            value_query = 'UPDATE location SET ';

            -- Handle NULL values
            IF NEW.id_city IS NOT NULL THEN
                value_query = value_query || 'id_city=' || id_city;
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Added possibility to edit ID
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

            -- Handle NULL values
            IF NEW.id_coordinates IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_coordinates =' || id_coordinates;
            END IF;

            -- Handle NULL values
            IF NEW.id_tectonic IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_tectonic =' || id_tectonic;
            END IF;

            -- Handle NULL values
            IF NEW.description IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'description =' || quote_literal(description);
            END IF;

             -- Create query
            value_query = value_query || ' WHERE id=' || id_location || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_location = OLD.id;

            -- Create query
            value_query = 'DELETE FROM location WHERE id=' || id_location || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_location_t1 on location;
CREATE TRIGGER sync_location_t1 AFTER INSERT OR UPDATE OR DELETE ON location
    FOR EACH ROW EXECUTE PROCEDURE sync_location_t1();

create or replace function sync_log_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_log integer;
	    title text;
	    date_ date;
	    id_log_type integer;
	    id_station integer;
	    modified text;
      previous text;
	    id_contact integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_log = NEW.id;
            title = NEW.title;
            date_ = NEW.date;
            id_log_type = NEW.id_log_type;
            id_station = NEW.id_station;
            modified = NEW.modified;
            previous = NEW.previous;
            id_contact = NEW.id_contact;

            -- Create query
            value_query = 'INSERT INTO log (id, title, id_log_type, id_station';

            IF date_ IS NOT NULL THEN
                value_query = value_query || ', date';
            END IF;

            IF modified IS NOT NULL THEN
                value_query = value_query || ', modified';
            END IF;

            IF previous IS NOT NULL THEN
                value_query = value_query || ', previous';
            END IF;

            IF id_contact IS NOT NULL THEN
                value_query = value_query || ', id_contact';
            END IF;

            value_query = value_query || ') VALUES (' || id_log || ', ' || quote_literal(title) || ', ' || id_log_type || ', ' || id_station;

            IF date_ IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_);
            END IF;

            IF modified IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(modified);
            END IF;

            IF previous IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(previous);
            END IF;

            IF id_contact IS NOT NULL THEN
                value_query = value_query || ', ' || id_contact;
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_log = OLD.id;
            title = NEW.title;
            date_ = NEW.date;
            id_log_type = NEW.id_log_type;
            id_station = NEW.id_station;
            modified = NEW.modified;
            previous = NEW.previous;
            id_contact = NEW.id_contact;
            value_first = true;

            -- Create query
            value_query = 'UPDATE log SET ';

            -- Handle NULL values
            IF NEW.title IS NOT NULL THEN
                value_query = value_query || 'title=' || quote_literal(title);
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.date IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date =' || quote_literal(date_);
            END IF;

            -- Handle NULL values
            IF NEW.id_log_type IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_log_type =' || id_log_type;
            END IF;

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_station =' || id_station;
            END IF;

            -- Handle NULL values
            IF NEW.modified IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'modified =' || quote_literal(modified);
            END IF;

            IF NEW.previous IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'previous =' || quote_literal(previous);
            END IF;

            -- Handle NULL values
            IF NEW.id_contact IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_contact =' || id_contact;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_log || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_log = OLD.id;
            id_station = OLD.id_station;

            -- Create query
            value_query = 'DELETE FROM log WHERE ';

            -- By setting value_station ID to NULL we need to ensure that the correct entry is removed
            -- in the remote node (not use only ID)
            -- Handle NULL values
            IF OLD.id IS NOT NULL THEN
                value_query = value_query || 'id=' || quote_literal(OLD.id);
                value_first = false;
            END IF;

            -- Handle NULL values
            IF OLD.title IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'title =' || quote_literal(OLD.title);
            END IF;

            -- Handle NULL values
            IF OLD.date IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date =' || quote_literal(OLD.date);
            END IF;

            -- Handle NULL values
            IF OLD.id_log_type IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_log_type =' || OLD.id_log_type;
            END IF;

            -- Handle NULL values
            IF OLD.id_station IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_station =' || OLD.id_station;
            END IF;

            -- Handle NULL values
            IF OLD.modified IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'modified =' || quote_literal(OLD.modified);
            END IF;

            IF OLD.previous IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'previous =' || quote_literal(OLD.previous);
            END IF;

            -- Handle NULL values
            IF OLD.id_contact IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_contact =' || OLD.id_contact;
            END IF;


            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
                -- We can't use station_id anymore in the new version of the database
                -- because now station_id is a foreign key, in table queries, that belongs to table station
                -- We can't use a key that was previously removed from the station table
            -- value_stationID = old.id_station;

            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_log_t1 on log;
CREATE TRIGGER sync_log_t1 AFTER INSERT OR UPDATE OR DELETE ON log
    FOR EACH ROW EXECUTE PROCEDURE sync_log_t1();

create or replace function sync_log_type_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_log_type integer;
	    name text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_log_type = NEW.id;
            name = NEW.name;

            -- Create query
            value_query = 'INSERT INTO log_type (id, name) VALUES ' ||
                '(' || id_log_type || ', ' || quote_literal(name) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_log_type = OLD.id;
            name = NEW.name;

            -- Create query
            value_query = 'UPDATE log_type SET id=' || NEW.id || '';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || ', name=' || quote_literal(name);
            END IF;

             -- Create query
            value_query = value_query || ' WHERE id=' || id_log_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_log_type = OLD.id;

            -- Create query
            value_query = 'DELETE FROM log_type WHERE id=' || id_log_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_log_type_t1 on log_type;
CREATE TRIGGER sync_log_type_t1 AFTER INSERT OR UPDATE OR DELETE ON log_type
    FOR EACH ROW EXECUTE PROCEDURE sync_log_type_t1();

create or replace function sync_monument_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_monument integer;
	    description text;
	    inscription text;
	    height text;
	    foundation text;
	    foundation_depth text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_monument = NEW.id;
            description = NEW.description;
            inscription = NEW.inscription;
            height = NEW.height;
            foundation = NEW.foundation;
            foundation_depth = NEW.foundation_depth;

            -- Create query
            value_query = 'INSERT INTO monument (id, description';

            IF inscription IS NOT NULL THEN
                value_query = value_query || ', inscription';
            END IF;

            IF height IS NOT NULL THEN
                value_query = value_query || ', height';
            END IF;

            IF foundation IS NOT NULL THEN
                value_query = value_query || ', foundation';
            END IF;

            IF foundation_depth IS NOT NULL THEN
                value_query = value_query || ', foundation_depth';
            END IF;

            value_query = value_query || ') VALUES (' || id_monument || ', ' || quote_literal(description);

            IF inscription IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(inscription);
            END IF;

            IF height IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(height);
            END IF;

            IF foundation IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(foundation);
            END IF;

            IF foundation_depth IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(foundation_depth);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_monument = OLD.id;
            description = NEW.description;
            inscription = NEW.inscription;
            height = NEW.height;
            foundation = NEW.foundation;
            foundation_depth = NEW.foundation_depth;

            -- Create query
            value_query = 'UPDATE monument SET ';

            -- Handle NULL values
            IF NEW.description IS NOT NULL THEN
                value_query = value_query || 'description=' || quote_literal(description);
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Added possibility to edit ID
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

            -- Handle NULL values
            IF NEW.inscription IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'inscription =' || quote_literal(inscription);
            END IF;

            -- Handle NULL values
            IF NEW.height IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'height =' || quote_literal(height);
            END IF;

            -- Handle NULL values
            IF NEW.foundation IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'foundation =' || quote_literal(foundation);
            END IF;

            -- Handle NULL values
            IF NEW.foundation_depth IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'foundation_depth =' || quote_literal(foundation_depth);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_monument || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_monument = OLD.id;

            -- Create query
            value_query = 'DELETE FROM monument WHERE id=' || id_monument || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_monument_t1 on monument;
CREATE TRIGGER sync_monument_t1 AFTER INSERT OR UPDATE OR DELETE ON monument
    FOR EACH ROW EXECUTE PROCEDURE sync_monument_t1();

create or replace function sync_network_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_network integer;
	    name text;
	    id_contact integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_network = NEW.id;
            name = NEW.name;
            id_contact = NEW.id_contact;

             -- Create query
            value_query = 'INSERT INTO network (id, name';

            IF id_contact IS NOT NULL THEN
                value_query = value_query || ', id_contact';
            END IF;

            value_query = value_query || ') VALUES (' || id_network || ', ' || quote_literal(name);

            IF id_contact IS NOT NULL THEN
                value_query = value_query || ', ' || id_contact;
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_network = OLD.id;
            name = NEW.name;
            id_contact = NEW.id_contact;
            value_first = true;

            -- Create query
            value_query = 'UPDATE network SET ';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
            END IF;

             -- Handle NULL values
             -- Possibility to edit ID
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

             -- Handle NULL values
            IF NEW.id_contact IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_contact =' || id_contact;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_network || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_network = OLD.id;

            -- Create query
            value_query = 'DELETE FROM network WHERE id=' || id_network || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
			
            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_network_t1 on network;
CREATE TRIGGER sync_network_t1 AFTER INSERT OR UPDATE OR DELETE ON network
    FOR EACH ROW EXECUTE PROCEDURE sync_network_t1();

create or replace function sync_state_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_state integer;
	    id_country integer;
	    name text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_state = NEW.id;
            id_country = NEW.id_country;
            name = NEW.name;

            -- Create query
            value_query = 'INSERT INTO state (id, id_country, name) VALUES ' ||
                '(' || id_state || ', ' || id_country || ', ' || quote_literal(name) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_state = OLD.id;
            id_country = NEW.id_country;
            name = NEW.name;

            -- Create query
            value_query = 'UPDATE state SET ';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Added possibility to update id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id =' || NEW.id;
            END IF;

            -- Handle NULL values
            IF NEW.id_country IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_country =' || id_country;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_state || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_state = OLD.id;

            -- Create query
            value_query = 'DELETE FROM state WHERE id=' || id_state || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_state_t1 on state;
CREATE TRIGGER sync_state_t1 AFTER INSERT OR UPDATE OR DELETE ON state
    FOR EACH ROW EXECUTE PROCEDURE sync_state_t1();

    
create or replace function sync_station_colocation_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_station_colocation integer;
	    id_station integer;
	    id_station_colocated integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_station_colocation = NEW.id;
            id_station = NEW.id_station;
            id_station_colocated = NEW.id_station_colocated;

            -- Create query
            value_query = 'INSERT INTO station_colocation (id, id_station, id_station_colocated) VALUES ' ||
                '(' || id_station_colocation || ', ' || id_station || ', ' || id_station_colocated || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_station_colocation = OLD.id;
            id_station = NEW.id_station;
            id_station_colocated = NEW.id_station_colocated;

            -- Create query
            value_query = 'UPDATE station_colocation SET ';

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || id_station;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

            -- Handle NULL values
            IF NEW.id_station_colocated IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_station_colocated =' || id_station_colocated;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_station_colocation || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_station_colocation = OLD.id;
            id_station = OLD.id_station;

            -- Create query
            value_query = 'DELETE FROM station_colocation WHERE id=' || id_station_colocation || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_station_colocation_t1 on station_colocation;
CREATE TRIGGER sync_station_colocation_t1 AFTER INSERT OR UPDATE OR DELETE ON station_colocation
    FOR EACH ROW EXECUTE PROCEDURE sync_station_colocation_t1();

    
create or replace function sync_station_contact_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_station_contacts integer;
	    id_station integer;
	    id_contact integer;
	    role text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_station_contacts = NEW.id;
            id_station = NEW.id_station;
            id_contact = NEW.id_contact;
            role = NEW.role;

            -- Create query
            value_query = 'INSERT INTO station_contact (id, id_station, id_contact';

            IF role IS NOT NULL THEN
                value_query = value_query || ', role';
            END IF;

            value_query = value_query || ') VALUES (' || id_station_contacts || ', ' || id_station || ', ' || id_contact;

            IF role IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(role);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_station_contacts = OLD.id;
            id_station = NEW.id_station;
            id_contact = NEW.id_contact;
            role = NEW.role;
            value_first = true;

            -- Create query
            value_query = 'UPDATE station_contact SET ';

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || id_station;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.id_contact IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_contact =' || id_contact;
            END IF;

            -- Handle NULL values
            IF NEW.role IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'role =' || quote_literal(role);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_station_contacts || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_station_contacts = OLD.id;

            -- Create query
            -- By setting value_stationID to NULL we need to ensure that the correct entry is removed
            -- in the remote node (not use only ID)
            -- Handle NULL values

            value_query = 'DELETE FROM station_contact WHERE id=' || id_station_contacts || ' AND ';

            -- Handle NULL values
            IF OLD.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || OLD.id_station;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF OLD.id_contact IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_contact=' || OLD.id_contact;
            END IF;

            -- Handle NULL values
            IF OLD.role IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'role=' || quote_literal(OLD.role);
            END IF;

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            -- We can't use station_id anymore in the new version of the database
            -- because now station_id is a foreign key, in table queries, that belongs to table station
            -- We can't use a key that was previously removed from the station table

            -- value_stationID = old.id_station;
            value_stationID = NULL;
            -- raise notice '%', value_query; debug only

            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_station_contact_t1 on station_contact;
CREATE TRIGGER sync_station_contact_t1 AFTER INSERT OR UPDATE OR DELETE ON station_contact
    FOR EACH ROW EXECUTE PROCEDURE sync_station_contact_t1();

create or replace function sync_station_item_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_station_item integer;
	    id_station integer;
	    id_item integer;
	    date_from timestamp;
	    date_to timestamp;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_station_item = NEW.id;
            id_station = NEW.id_station;
            id_item = NEW.id_item;
            date_from = NEW.date_from;
            date_to = NEW.date_to;

            -- Create query
            value_query = 'INSERT INTO station_item (id, id_station, id_item, date_from';

            IF date_to IS NOT NULL THEN
                value_query = value_query || ', date_to';
            END IF;

            value_query = value_query || ') VALUES (' || id_station_item || ', ' || id_station || ', ' || id_item || ', ' || quote_literal(date_from);


            IF date_to IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_to);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_station_item = OLD.id;
            id_station = NEW.id_station;
            id_item = NEW.id_item;
            date_from = NEW.date_from;
            date_to = NEW.date_to;
            value_first = true;

            -- Create query
            value_query = 'UPDATE station_item SET ';

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || id_station;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.id_item IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_item =' || id_item;
            END IF;

            -- Handle NULL values
            IF NEW.date_from IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_from =' || quote_literal(date_from);
            END IF;

            -- Handle NULL values
            IF NEW.date_to IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_to =' || quote_literal(date_to);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_station_item || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_station_item = OLD.id;

            -- Create query
            -- By setting value_stationID to NULL we need to ensure that the correct entry is removed
            -- in the remote node (not use only ID)
            value_query = 'DELETE FROM station_item WHERE id=' || id_station_item || ' AND ';

            -- Handle NULL values
            IF OLD.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || OLD.id_station;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF OLD.id_item IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_item =' || OLD.id_item;
            END IF;

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
               -- We can't use station_id anymore in the new version of the database
               -- because now station_id is a foreign key, in table queries, that belongs to table station
               -- We can't use a key that was previously removed from the station table
            -- value_stationID = old.id_station;
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_station_item_t1 on station_item;
CREATE TRIGGER sync_station_item_t1 AFTER INSERT OR UPDATE OR DELETE ON station_item
    FOR EACH ROW EXECUTE PROCEDURE sync_station_item_t1();

create or replace function sync_station_network_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_station_network integer;
	    id_station integer;
	    id_network integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_station_network = NEW.id;
            id_station = NEW.id_station;
            id_network = NEW.id_network;

            -- Create query
            value_query = 'INSERT INTO station_network (id, id_station, id_network) VALUES ' ||
                '(' || id_station_network || ', ' || id_station || ', ' || id_network || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_station_network = OLD.id;
            id_station = NEW.id_station;
            id_network = NEW.id_network;
            value_first = true;

            -- Create query
            value_query = 'UPDATE station_network SET ';

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || id_station;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.id_network IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_network =' || id_network;
            END IF;

             -- Create query
            value_query = value_query || ' WHERE id=' || id_station_network || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            -- Create query
            value_query = 'DELETE FROM station_network WHERE ';

            -- Handle NULL values
            IF OLD.id IS NOT NULL THEN
                value_query = value_query || 'id=' || OLD.id;
                value_first = false;
            END IF;

            IF OLD.id_station IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_station=' || OLD.id_station;
            END IF;

           -- Handle NULL values
            IF OLD.id_network IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_network=' || OLD.id_network;
            END IF;

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            -- We can't use station_id anymore in the new version of the database
            -- because now station_id is a foreign key, in table queries, that belongs to table station
            -- We can't use a key that was previously removed from the station table

            -- value_stationID = old.id_station;
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_station_network_t1 on station_network;
CREATE TRIGGER sync_station_network_t1 AFTER INSERT OR UPDATE OR DELETE ON station_network
    FOR EACH ROW EXECUTE PROCEDURE sync_station_network_t1();

create or replace function sync_station_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;
      value_query2 text;

	    id_station integer;
	    name text;
	    marker text;
	    description text;
	    date_from timestamp;
	    date_to timestamp;
	    id_station_type integer;
	    comment text;
	    id_location integer;
	    id_monument integer;
	    id_geological integer;
	    iers_domes text;
	    cpd_num text;
	    monument_num integer;
	    receiver_num integer;
	    country_code text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_station = NEW.id;
            name = NEW.name;
            marker = NEW.marker;
            description = NEW.description;
            date_from = NEW.date_from;
            date_to = NEW.date_to;
            id_station_type = NEW.id_station_type;
            comment = NEW.comment;
            id_location = NEW.id_location;
            id_monument = NEW.id_monument;
            id_geological = NEW.id_geological;
            iers_domes = NEW.iers_domes;
            cpd_num = NEW.cpd_num;
            monument_num = NEW.monument_num;
            receiver_num = NEW.receiver_num;
            country_code = NEW.country_code;

            -- Create query
            value_query = 'INSERT INTO station (id, name, marker';

            IF description IS NOT NULL THEN
                value_query = value_query || ', description';
            END IF;

            IF date_from IS NOT NULL THEN
                 value_query = value_query || ', date_from';
            END IF;

            IF date_to IS NOT NULL THEN
                 value_query = value_query || ', date_to';
            END IF;

            IF id_station_type IS NOT NULL THEN
                 value_query = value_query || ', id_station_type';
            END IF;

            IF comment IS NOT NULL THEN
                 value_query = value_query || ', comment';
            END IF;

            IF id_location IS NOT NULL THEN
                 value_query = value_query || ', id_location';
            END IF;

            IF id_monument IS NOT NULL THEN
                 value_query = value_query || ', id_monument';
            END IF;

            IF id_geological IS NOT NULL THEN
                 value_query = value_query || ', id_geological';
            END IF;

            IF iers_domes IS NOT NULL THEN
                 value_query = value_query || ', iers_domes';
            END IF;

            IF cpd_num IS NOT NULL THEN
                 value_query = value_query || ', cpd_num';
            END IF;

            IF monument_num IS NOT NULL THEN
                 value_query = value_query || ', monument_num';
            END IF;

            IF receiver_num IS NOT NULL THEN
                 value_query = value_query || ', receiver_num';
            END IF;

            IF country_code IS NOT NULL THEN
                 value_query = value_query || ', country_code';
            END IF;

            value_query = value_query || ') VALUES (' || id_station || ', ' || quote_literal(name) || ', ' || quote_literal(marker);

            IF description IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(description);
            END IF;

            IF date_from IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(date_from);
            END IF;

            IF date_to IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(date_to);
            END IF;

            IF id_station_type IS NOT NULL THEN
                 value_query = value_query || ', ' || id_station_type;
            END IF;

            IF comment IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(comment);
            END IF;

            IF id_location IS NOT NULL THEN
                 value_query = value_query || ', ' || id_location;
            END IF;

            IF id_monument IS NOT NULL THEN
                 value_query = value_query || ', ' || id_monument;
            END IF;

            IF id_geological IS NOT NULL THEN
                 value_query = value_query || ', ' || id_geological;
            END IF;

            IF iers_domes IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(iers_domes);
            END IF;

            IF cpd_num IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(cpd_num);
            END IF;

            IF monument_num IS NOT NULL THEN
                 value_query = value_query || ', ' || monument_num;
            END IF;

            IF receiver_num IS NOT NULL THEN
                 value_query = value_query || ', '  || receiver_num;
            END IF;

            IF country_code IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(country_code);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_station = OLD.id;
            name = NEW.name;
            marker = NEW.marker;
            description = NEW.description;
            date_from = NEW.date_from;
            date_to = NEW.date_to;
            id_station_type = NEW.id_station_type;
            comment = NEW.comment;
            id_location = NEW.id_location;
            id_monument = NEW.id_monument;
            id_geological = NEW.id_geological;
            iers_domes = NEW.iers_domes;
            cpd_num = NEW.cpd_num;
            monument_num = NEW.monument_num;
            receiver_num = NEW.receiver_num;
            country_code = NEW.country_code;
            value_first = true;

            -- Create query
            value_query = 'UPDATE station SET ';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.marker IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'marker =' || quote_literal(marker);
            END IF;

            -- Handle NULL values
            IF NEW.description IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'description =' || quote_literal(description);
            END IF;

            -- Handle NULL values
            IF NEW.date_from IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_from =' || quote_literal(date_from);
            END IF;

            -- Handle NULL values
            IF NEW.date_to IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_to =' || quote_literal(date_to);
            END IF;

            -- Handle NULL values
            IF NEW.id_station_type IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_station_type =' || id_station_type;
            END IF;

            -- Handle NULL values
            IF NEW.comment IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'comment =' || quote_literal(comment);
            END IF;

            -- Handle NULL values
            IF NEW.id_location IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_location =' || id_location;
            END IF;

            -- Handle NULL values
            IF NEW.id_monument IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_monument =' || id_monument;
            END IF;

            -- Handle NULL values
            IF NEW.id_geological IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                -- Bug fix, it was id_monument
                value_query = value_query || 'id_geological =' || id_geological;
            END IF;

            -- Handle NULL values
            IF NEW.iers_domes IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'iers_domes =' || quote_literal(iers_domes);
            END IF;

            -- Handle NULL values
            IF NEW.cpd_num IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'cpd_num =' || quote_literal(cpd_num);
            END IF;

            -- Handle NULL values
            IF NEW.monument_num IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'monument_num =' || monument_num;
            END IF;

            -- Handle NULL values
            IF NEW.receiver_num IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'receiver_num =' || receiver_num;
            END IF;

            -- Handle NULL values
            IF NEW.country_code IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'country_code =' || quote_literal(country_code);
            END IF;

            -- Handle NULL values
            -- Added possibility to edit id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id =' || NEW.id;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_station || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            IF (NEW.id != OLD.id) THEN
                value_stationID = NEW.id;
            ELSE
                value_stationID = OLD.id;
            END IF;


            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_station = OLD.id;

            -- Create query
            value_query = 'DELETE FROM station WHERE id=' || id_station || ';';
            -- every time that a station is deleted, it´s also need to delete all the entries from queries which have the same station id
            value_query2 = 'DELETE FROM queries WHERE station_id=' || id_station || ';';

            -- Set metadata type
            value_metadata = 'T1';
            --value_metadata2 = 'T1';

            -- Set station code
            value_stationID = NULL;
            --value_stationID2 = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query2, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_station_t1 on station;
CREATE TRIGGER sync_station_t1 AFTER INSERT OR UPDATE OR DELETE ON station
    FOR EACH ROW EXECUTE PROCEDURE sync_station_t1();


create or replace function sync_station_type_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_station_type integer;
	    name text;
	    type text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_station_type = NEW.id;
            name = NEW.name;
            type = NEW.type;

            -- Create query
            value_query = 'INSERT INTO station_type (id, name';

            IF type IS NOT NULL THEN
                value_query = value_query || ', type';
            END IF;

            value_query = value_query || ') VALUES (' || id_station_type || ', ' || quote_literal(name);

            IF type IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(type);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_station_type = OLD.id;
            name = NEW.name;
            type = NEW.type;
            value_first = true;

            -- Create query
            value_query = 'UPDATE station_type SET ';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Added possibility to update id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

            -- Handle NULL values
            IF NEW.type IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'type =' || quote_literal(type);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_station_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_station_type = OLD.id;

            -- Create query
            value_query = 'DELETE FROM station_type WHERE id=' || id_station_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_station_type_t1 on station_type;
CREATE TRIGGER sync_station_type_t1 AFTER INSERT OR UPDATE OR DELETE ON station_type
    FOR EACH ROW EXECUTE PROCEDURE sync_station_type_t1();


create or replace function sync_tectonic_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_tectonic integer;
        plate_name text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_tectonic = NEW.id;
            plate_name = NEW.plate_name;

            -- Create query
            value_query = 'INSERT INTO tectonic (id, plate_name) VALUES ' ||
                '(' || id_tectonic || ', ' || quote_literal(plate_name) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_tectonic = OLD.id;
            plate_name = NEW.plate_name;
            value_first = true;

            -- Create query
            value_query = 'UPDATE tectonic SET ';

            -- Handle NULL values
            IF NEW.plate_name IS NOT NULL THEN
                value_query = value_query || 'plate_name=' || quote_literal(plate_name);
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Added possibility to update id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_tectonic || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_tectonic = OLD.id;

            -- Create query
            value_query = 'DELETE FROM tectonic WHERE id=' || id_tectonic || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_tectonic_t1 on tectonic;
CREATE TRIGGER sync_tectonic_t1 AFTER INSERT OR UPDATE OR DELETE ON tectonic
    FOR EACH ROW EXECUTE PROCEDURE sync_tectonic_t1();


create or replace function sync_user_group_station_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_user_group_station integer;
	    id_user_group integer;
	    id_station integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_user_group_station = NEW.id;
            id_user_group = NEW.id_user_group;
            id_station = NEW.id_station;

            -- Create query
            value_query = 'INSERT INTO user_group_station (id';

            IF id_user_group IS NOT NULL THEN
                value_query = value_query || ', id_user_group';
            END IF;

            IF id_station IS NOT NULL THEN
                 value_query = value_query || ', id_station';
            END IF;

            value_query = value_query || ') VALUES (' || id_user_group_station;

            IF id_user_group IS NOT NULL THEN
                value_query = value_query || ', ' || id_user_group;
            END IF;

            IF id_station IS NOT NULL THEN
                 value_query = value_query || ', ' || id_station;
            END IF;

             value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_user_group_station = OLD.id;
            id_user_group = NEW.id_user_group;
            id_station = NEW.id_station;
            value_first = true;

            -- If this update introduces a different station than the previous one for the current record, then we must remove
            -- the entry associated with the old station from the remote node;
            IF (OLD.id_station != NEW.id_station) THEN
                value_query = 'DELETE FROM user_group_station WHERE id=' || OLD.id || ';';
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, 'T1', OLD.id_station);
            END IF;

             -- Create query

            -- Create query
            -- We must INSERT if the the current record is not available on the remote node
            -- this happens when the id of the station is not present in the remote node
            value_query = 'DO ' || chr(36) || '' || chr(36) || ' BEGIN IF EXISTS (SELECT * FROM user_group_station WHERE id=' || OLD.id ||') THEN ';

            -- UPDATE OP
            value_query = value_query || ' UPDATE user_group_station SET id=' || NEW.id || ',';

            -- Handle NULL values
            IF NEW.id_user_group IS NOT NULL THEN
                value_query = value_query || 'id_user_group=' || id_user_group;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_station =' || id_station;
            END IF;

            value_query = value_query || ' WHERE id=' || id_user_group_station || ';';

            value_query = value_query || ' ELSE ';

            -- INSERT OP
            value_query = value_query || 'INSERT INTO user_group_station (id';

            IF id_user_group IS NOT NULL THEN
                value_query = value_query || ', id_user_group';
            END IF;

            IF id_station IS NOT NULL THEN
                 value_query = value_query || ', id_station';
            END IF;

            value_query = value_query || ') VALUES (' || id_user_group_station;

            IF id_user_group IS NOT NULL THEN
                value_query = value_query || ', ' || id_user_group;
            END IF;

            IF id_station IS NOT NULL THEN
                 value_query = value_query || ', ' || id_station;
            END IF;

            value_query = value_query || ');';

            value_query = value_query || ' END IF; END; ' || chr(36) || '' || chr(36) || ';';


            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_user_group_station = OLD.id;

            -- Create query
            value_query = 'DELETE FROM user_group_station WHERE id=' || id_user_group_station || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = old.id_station;

            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_user_group_station_t1 on user_group_station;
CREATE TRIGGER sync_user_group_station_t1 AFTER INSERT OR UPDATE OR DELETE ON user_group_station
    FOR EACH ROW EXECUTE PROCEDURE sync_user_group_station_t1();

    create or replace function sync_user_group_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_user_group integer;
	    name text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_user_group = NEW.id;
            name = NEW.name;

            -- Create query
            value_query = 'INSERT INTO user_group (id, name) VALUES ' ||
                '(' || id_user_group || ', ' || quote_literal(name) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_user_group = OLD.id;
            name = NEW.name;
            value_first = true;

            -- Create query
            -- Added possibility to edit id
            value_query = 'UPDATE user_group SET id=' || NEW.id || ', ';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_user_group || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_user_group = OLD.id;

            -- Create query
            value_query = 'DELETE FROM user_group WHERE id=' || id_user_group || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_user_group_t1 on user_group;
CREATE TRIGGER sync_user_group_t1 AFTER INSERT OR UPDATE OR DELETE ON user_group
    FOR EACH ROW EXECUTE PROCEDURE sync_user_group_t1();