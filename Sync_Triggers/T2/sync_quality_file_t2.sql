CREATE OR REPLACE FUNCTION sync_quality_file_t2() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
	value_query text;
    value_metadata text;
    value_stationID integer;
    value_first boolean;
    one_row RECORD;

	id_quality_file integer;
    name text;
    id_rinexfile integer;
    id_data_center_structure integer;
    file_size integer;
    id_file_type integer;
    relative_path text;
    creation_date text;
    revision_time text;
    mdfivechecksum text;
    status integer;

    aux_format text;
    aux_sampling_window text;
    aux_sampling_frequency text;
    aux_id_station text;
    aux_md5checksum text;  

    old_id_rinexfile integer;
    old_md5checksum text;
    old_id_station integer;     
    old_rf_md5checksum text;
    old_id_data_center_structure integer;
    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_quality_file = NEW.id;
            name = NEW.name;
            id_rinexfile = NEW.id_rinexfile;
            id_data_center_structure = NEW.id_data_center_structure;
            file_size = NEW.file_size;
            id_file_type = NEW.id_file_type;
            relative_path = NEW.relative_path;
            creation_date = NEW.creation_date;
            revision_time = NEW.revision_time;
            mdfivechecksum = NEW.md5checksum;
            status = NEW.status;

            -- Gets data from file_type
            FOR one_row IN 
                SELECT format, sampling_window, sampling_frequency FROM file_type WHERE id = id_file_type
            LOOP
                aux_format = one_row.format;
                aux_sampling_window = one_row.sampling_window;
                aux_sampling_frequency = one_row.sampling_frequency;
            END LOOP;

            -- Gets data from rinex_file
            FOR one_row IN 
                SELECT id_station, md5checksum FROM rinex_file WHERE id = id_rinexfile
            LOOP
                aux_id_station = one_row.id_station;
                aux_md5checksum = one_row.md5checksum;
            END LOOP;

			-- Create query
            value_query = 'INSERT INTO quality_file (name, id_rinexfile, id_data_center_structure, id_file_type, relative_path, status';

			IF file_size IS NOT NULL THEN
                value_query = value_query || ', file_size';
            END IF;

			IF creation_date IS NOT NULL THEN
                value_query = value_query || ', creation_date';
            END IF;

			IF revision_time IS NOT NULL THEN
                value_query = value_query || ', revision_time';
            END IF;

            IF mdfivechecksum IS NOT NULL THEN
                value_query = value_query || ', md5checksum';
            END IF;

			value_query = value_query || ') VALUES (' || quote_literal(name) || ', (SELECT id FROM rinex_file WHERE id_station= '|| aux_id_station ||' AND md5checksum='|| quote_literal(aux_md5checksum) ||'), ' || id_data_center_structure || ', (SELECT id from file_type WHERE format = ' || quote_literal(aux_format) || ' AND sampling_window=' || quote_literal(aux_sampling_window) || ' AND sampling_frequency=' || quote_literal(aux_sampling_frequency) || '), ' || quote_literal(relative_path) || ', ' || status;

			IF file_size IS NOT NULL THEN
                value_query = value_query || ', ' || file_size;
            END IF;

			IF creation_date IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(creation_date);
            END IF;

			IF revision_time IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(revision_time);
            END IF;

            IF mdfivechecksum IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(mdfivechecksum);
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T2';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_quality_file = OLD.id;
            old_id_rinexfile = OLD.id_rinexfile;
            old_md5checksum = OLD.md5checksum;
            old_id_data_center_structure = OLD.id_data_center_structure;

            name = NEW.name;
            id_rinexfile = NEW.id_rinexfile;
            id_data_center_structure = NEW.id_data_center_structure;
            file_size = NEW.file_size;
            id_file_type = NEW.id_file_type;
            relative_path = NEW.relative_path;
            creation_date = NEW.creation_date;
            revision_time = NEW.revision_time;
            mdfivechecksum = NEW.md5checksum;
            status = NEW.status;

            -- Gets data from file_type
            FOR one_row IN 
                SELECT format, sampling_window, sampling_frequency FROM file_type WHERE id = id_file_type
            LOOP
                aux_format = one_row.format;
                aux_sampling_window = one_row.sampling_window;
                aux_sampling_frequency = one_row.sampling_frequency;
            END LOOP;

            -- Gets data from rinex_file
            FOR one_row IN 
                SELECT id_station, md5checksum FROM rinex_file WHERE id = id_rinexfile
            LOOP
                aux_id_station = one_row.id_station;
                aux_md5checksum = one_row.md5checksum;
            END LOOP;

            -- Gets data from rinex_file
            FOR one_row IN 
                SELECT id_station, md5checksum FROM rinex_file WHERE id = old_id_rinexfile
            LOOP
                old_id_station = one_row.id_station;
                old_rf_md5checksum = one_row.md5checksum;
            END LOOP;

			-- Create query
            value_query = 'UPDATE quality_file SET name =' || quote_literal(name) || ', id_rinexfile=(SELECT id FROM rinex_file WHERE id_station= '|| aux_id_station ||' AND md5checksum='|| quote_literal(aux_md5checksum) ||'), id_data_center_structure=' || id_data_center_structure || ', id_file_type=(SELECT id from file_type WHERE format = ' || quote_literal(aux_format) || ' AND sampling_window=' || quote_literal(aux_sampling_window) || ' AND sampling_frequency=' || quote_literal(aux_sampling_frequency) || '), relative_path=' || quote_literal(relative_path) || ', status=' || status;

			 -- Handle NULL values
            IF NEW.file_size IS NOT NULL THEN
                value_query = value_query || ', file_size=' || file_size;
            END IF;

			-- Handle NULL values
            IF NEW.creation_date IS NOT NULL THEN
                value_query = value_query || ', creation_date=' || quote_literal(creation_date);
            END IF;

			-- Handle NULL values
            IF NEW.revision_time IS NOT NULL THEN
                value_query = value_query || ', revision_time=' || quote_literal(revision_time);
            END IF;

			-- Handle NULL values
			-- bug fix
            IF NEW.md5checksum IS NOT NULL THEN
                value_query = value_query || ', md5checksum=' || quote_literal(mdfivechecksum);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id_rinexfile=(SELECT id FROM rinex_file WHERE id_station= '|| old_id_station ||' AND md5checksum='|| quote_literal(old_rf_md5checksum) ||') AND id_data_center_structure='|| old_id_data_center_structure;

            IF OLD.md5checksum IS NOT NULL THEN
                value_query = value_query || ' AND md5checksum=' || quote_literal(old_md5checksum);
            END IF;

            value_query = value_query || ';';

            -- Set metadata type
            value_metadata = 'T2';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_rinexfile = OLD.id_rinexfile;
            id_data_center_structure = OLD.id_data_center_structure;
            mdfivechecksum = OLD.md5checksum;

            -- Gets data from rinex_file
            FOR one_row IN 
                SELECT id_station, md5checksum FROM rinex_file WHERE id = id_rinexfile
            LOOP
                aux_id_station = one_row.id_station;
                aux_md5checksum = one_row.md5checksum;
            END LOOP;

            -- Create query
            value_query = 'DELETE FROM quality_file WHERE id_rinexfile=(SELECT id FROM rinex_file WHERE id_station= '|| aux_id_station ||' AND md5checksum='|| quote_literal(aux_md5checksum) ||') AND id_data_center_structure =' || id_data_center_structure;

            IF OLD.md5checksum IS NOT NULL THEN
                value_query = value_query || ' AND md5checksum=' || quote_literal(mdfivechecksum);
            END IF;

            value_query = value_query || ';';

            -- Set metadata type
            value_metadata = 'T2';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, if the id_rinexfile exists in table rinex_file
            IF (EXISTS(SELECT id FROM rinex_file WHERE id=id_rinexfile)) THEN
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;

            RETURN NULL;
        END IF;
    END;
$$;

DROP TRIGGER IF EXISTS sync_quality_file_t2 on quality_file;
CREATE TRIGGER sync_quality_file_t2 AFTER INSERT OR UPDATE OR DELETE ON quality_file
    FOR EACH ROW EXECUTE PROCEDURE sync_quality_file_t2();


