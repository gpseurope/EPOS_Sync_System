CREATE OR REPLACE FUNCTION sync_rinex_error_types_t2() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
	value_query text;
    value_metadata text;
    value_stationID integer;
    value_first boolean;

	id_rinex_error_types integer;
    error_type text;
    aux_error_type text;

    BEGIN
    RETURN NULL;
    /*
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_rinex_error_types = NEW.id;
            error_type = NEW.error_type;      

			-- Create query
            value_query = 'INSERT INTO rinex_error_types (error_type) VALUES ('|| quote_literal(error_type) ||')';

            -- Set metadata type
            value_metadata = 'T2';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN

            error_type = NEW.error_type;
            aux_error_type = OLD.error_type;

			-- Create query
            value_query = 'UPDATE rinex_error_types SET error_type = ' || quote_literal(error_type) || ' WHERE error_type = ' || quote_literal(aux_error_type) || ';';

            -- Set metadata type
            value_metadata = 'T2';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            
            error_type = OLD.error_type;

            -- Create query
            value_query = 'DELETE FROM rinex_error_types WHERE error_type='|| quote_literal(error_type) ||';';

            -- Set metadata type
            value_metadata = 'T2';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
    */
$$;

DROP TRIGGER IF EXISTS sync_rinex_error_types_t2 on rinex_error_types;
--CREATE TRIGGER sync_rinex_error_types_t2 AFTER INSERT OR UPDATE OR DELETE ON rinex_error_types
--    FOR EACH ROW EXECUTE PROCEDURE sync_rinex_error_types_t2();


