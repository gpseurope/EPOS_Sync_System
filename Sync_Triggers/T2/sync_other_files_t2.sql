CREATE OR REPLACE FUNCTION sync_other_files_t2() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
	value_query text;
    value_metadata text;
    value_stationID integer;
    value_first boolean;
    one_row RECORD;

	id_other_files integer;
    name text;
    id_data_center_structure integer;
    file_size integer;
    id_file_type integer;
    relative_path text;
    creation_date text;
    revision_time text;
    md5checksum text;
    status integer;
    id_station integer;

    aux_format text;
    aux_sampling_window text;
    aux_sampling_frequency text;
    old_format text;
    old_sampling_window text;
    old_sampling_frequency text;
    old_name text;
    old_id_data_center_structure integer;
    old_id_file_type integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_other_files = NEW.id;
            name = NEW.name;
            id_data_center_structure = NEW.id_data_center_structure;
            file_size = NEW.file_size;
            id_file_type = NEW.id_file_type;
            relative_path = NEW.relative_path;
            creation_date = NEW.creation_date;
            revision_time = NEW.revision_time;
            md5checksum = NEW.md5checksum;
            status = NEW.status;
            id_station = NEW.id_station;

            -- Gets data from file_type
            FOR one_row IN 
                SELECT format, sampling_window, sampling_frequency FROM file_type WHERE id = id_file_type
            LOOP
                aux_format = one_row.format;
                aux_sampling_window = one_row.sampling_window;
                aux_sampling_frequency = one_row.sampling_frequency;
            END LOOP;

			-- Create query
            value_query = 'INSERT INTO other_files (name, id_data_center_structure, id_file_type, relative_path, status';

			IF file_size IS NOT NULL THEN
                value_query = value_query || ', file_size';
            END IF;

			IF creation_date IS NOT NULL THEN
                value_query = value_query || ', creation_date';
            END IF;

			IF revision_time IS NOT NULL THEN
                value_query = value_query || ', revision_time';
            END IF;

            IF md5checksum IS NOT NULL THEN
                value_query = value_query || ', md5checksum';
            END IF;

            IF id_station IS NOT NULL THEN
                value_query = value_query || ', id_station';
            END IF;

			value_query = value_query || ') VALUES (' || quote_literal(name) || ', ' || id_data_center_structure || ', (SELECT id from file_type WHERE format = ' || quote_literal(aux_format) || ' AND sampling_window=' || quote_literal(aux_sampling_window) || ' AND sampling_frequency=' || quote_literal(aux_sampling_frequency) || '), ' || quote_literal(relative_path) || ', ' || status;

			IF file_size IS NOT NULL THEN
                value_query = value_query || ', ' || file_size;
            END IF;

			IF creation_date IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(creation_date);
            END IF;

			IF revision_time IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(revision_time);
            END IF;

            IF md5checksum IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(md5checksum);
            END IF;

            IF id_station IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(id_station);
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T2';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_other_files = OLD.id;
            name = NEW.name;
            id_data_center_structure = NEW.id_data_center_structure;
            file_size = NEW.file_size;
            id_file_type = NEW.id_file_type;
            relative_path = NEW.relative_path;
            creation_date = NEW.creation_date;
            revision_time = NEW.revision_time;
            md5checksum = NEW.md5checksum;
            status = NEW.status;
            id_station = NEW.id_station;

            old_name = OLD.name;
            -- bug fix
            old_id_data_center_structure = OLD.id_data_center_structure;
            old_id_file_type = OLD.id_file_type;

            -- Gets data from file_type
            FOR one_row IN 
                SELECT format, sampling_window, sampling_frequency FROM file_type WHERE id = id_file_type
            LOOP
                aux_format = one_row.format;
                aux_sampling_window = one_row.sampling_window;
                aux_sampling_frequency = one_row.sampling_frequency;
            END LOOP;

			-- Create query
            value_query = 'UPDATE other_files SET name =' || quote_literal(name) || ', id_data_center_structure=' || id_data_center_structure || ', id_file_type=(SELECT id from file_type WHERE format = ' || quote_literal(aux_format) || ' AND sampling_window=' || quote_literal(aux_sampling_window) || ' AND sampling_frequency=' || quote_literal(aux_sampling_frequency) || '), relative_path=' || quote_literal(relative_path) || ', status=' || status;

			 -- Handle NULL values
            IF NEW.file_size IS NOT NULL THEN
                value_query = value_query || ', file_size=' || file_size;
            END IF;

			-- Handle NULL values
            IF NEW.creation_date IS NOT NULL THEN
                value_query = value_query || ', creation_date=' || quote_literal(creation_date);
            END IF;

			-- Handle NULL values
            IF NEW.revision_time IS NOT NULL THEN
                value_query = value_query || ', revision_time=' || quote_literal(revision_time);
            END IF;

			-- Handle NULL values
            IF NEW.md5checksum IS NOT NULL THEN
                value_query = value_query || ', md5checksum=' || quote_literal(md5checksum);
            END IF;

            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || ', id_station=' || id_station;
            END IF;

            -- Gets data from file_type
            FOR one_row IN 
                SELECT format, sampling_window, sampling_frequency FROM file_type WHERE id = old_id_file_type
            LOOP
                old_format = one_row.format;
                old_sampling_window = one_row.sampling_window;
                old_sampling_frequency = one_row.sampling_frequency;
            END LOOP;

			-- Create query
            value_query = value_query || ' WHERE name=' || quote_literal(old_name) || ' AND id_data_center_structure=' || old_id_data_center_structure || ' AND id_file_type=(SELECT id from file_type WHERE format = ' || quote_literal(old_format) || ' AND sampling_window=' || quote_literal(old_sampling_window) || ' AND sampling_frequency=' || quote_literal(old_sampling_frequency) || ');';

            -- Set metadata type
            value_metadata = 'T2';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            name = OLD.name;
            -- bug fix
            id_data_center_structure = OLD.id_data_center_structure;
            --
            id_file_type = OLD.id_file_type;

            -- Gets data from file_type
            FOR one_row IN 
                SELECT format, sampling_window, sampling_frequency FROM file_type WHERE id = id_file_type
            LOOP
                old_format = one_row.format;
                old_sampling_window = one_row.sampling_window;
                old_sampling_frequency = one_row.sampling_frequency;
            END LOOP;

            -- Create query
            value_query = 'DELETE FROM other_files WHERE name=' || quote_literal(name) || ' AND id_data_center_structure=' || id_data_center_structure || ' AND id_file_type=(SELECT id from file_type WHERE format = ' || quote_literal(old_format) || ' AND sampling_window=' || quote_literal(old_sampling_window) || ' AND sampling_frequency=' || quote_literal(old_sampling_frequency) || ');';

            -- Set metadata type
            value_metadata = 'T2';

            -- Set station code
            value_stationID = NULL;

            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- In this case, if the id_file_type exists
            IF EXISTS(SELECT id FROM file_type where id=id_file_type) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;


            RETURN NULL;
        END IF;
    END;
$$;

DROP TRIGGER IF EXISTS sync_other_files_t2 on other_files;
CREATE TRIGGER sync_other_files_t2 AFTER INSERT OR UPDATE OR DELETE ON other_files
    FOR EACH ROW EXECUTE PROCEDURE sync_other_files_t2();


