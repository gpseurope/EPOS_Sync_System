-- Remove previous version of the T2 auto connections feature (fail safe) - This feature was moved to sync_connections_t1
DROP TRIGGER IF EXISTS sync_station_t2 on station;