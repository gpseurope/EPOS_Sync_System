CREATE OR REPLACE FUNCTION sync_rinex_errors_t2() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
	value_query text;
    value_metadata text;
    value_stationID integer;
    value_first boolean;
    one_row RECORD;

    id_station_ integer;
    md5checksum_ text;
    status_ integer;
	id_rinex_errors integer;
    id_rinex_file integer;
    id_error_type integer;

    aux_id_station integer;
    aux_md5checksum text;
    aux_status integer;
    aux_error_type text;
    old_id_station integer;
    old_md5checksum text;
    old_error_type text;
    old_status integer;

    BEGIN
        RETURN NULL;
    /*
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_rinex_errors = NEW.id;
            id_rinex_file = NEW.id_rinex_file;
            id_error_type = NEW.id_error_type;

            IF id_rinex_file IS NULL OR id_error_type IS NULL THEN
                RAISE EXCEPTION '[INSERT] - ''id_rinex_file'' or ''id_error_type'' fields of table ''rinex_errors'' should not be null! Please, revise your data.';
                RETURN NULL;
            END IF;

            -- Gets data from rinex_file
            FOR one_row IN
                    SELECT id_station, md5checksum, status FROM rinex_file WHERE id = id_rinex_file
            LOOP
                aux_id_station = one_row.id_station;
                aux_md5checksum = one_row.md5checksum;
                aux_status = one_row.status;
            END LOOP;

            -- Handle rinex QC status ON INSERT rinex errors
            -- IF status <= 0 THEN NOT synchronize INSERT rinex errors
            -- IF status > 0  THEN synchronize INSERT rinex errors
            IF (aux_status <= 0) THEN
                RAISE NOTICE '[rinex_errors] QC Status = % - Not creating synchronizing operations for insert', aux_status;
                RETURN NULL;
            END IF;

            -- Gets data from rinex_error_types
            FOR one_row IN
                SELECT error_type FROM rinex_error_types WHERE id = id_error_type
            LOOP
                aux_error_type = one_row.error_type;
            END LOOP;

            -- Create query
            value_query = 'INSERT INTO rinex_errors (id_rinex_file, id_error_type';

    	    value_query = value_query || ') VALUES ((SELECT id FROM rinex_file WHERE id_station= '|| aux_id_station ||' AND md5checksum = '|| quote_literal(aux_md5checksum) ||'), (SELECT id from rinex_error_types WHERE error_type = '|| quote_literal(aux_error_type) ||'))';

            -- Set metadata type
            value_metadata = 'T2';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Added possibility to update and sync rinex_errors entries
        IF (TG_OP = 'UPDATE') THEN
            id_rinex_file = NEW.id_rinex_file;
            id_error_type = NEW.id_error_type;
            id_rinex_file = NEW.id_rinex_file;

            IF id_rinex_file IS NULL OR id_error_type IS NULL THEN
                RAISE EXCEPTION '[UPDATE] - ''id_rinex_file'' or ''id_error_type'' fields of table ''rinex_errors'' should not be null! Please, revise your data.';
                RETURN NULL;
            END IF;

            -- GET OLD VALUES
            FOR one_row IN
                SELECT id_station, md5checksum, status FROM rinex_file WHERE id = OLD.id_rinex_file
            LOOP
                old_id_station = one_row.id_station;
                old_md5checksum = one_row.md5checksum;
                old_status = one_row.status;
            END LOOP;
            -- Get NEW VALUES
            FOR one_row IN
                SELECT id_station, md5checksum, status FROM rinex_file WHERE id = NEW.id_rinex_file
            LOOP
                id_station_ = one_row.id_station;
                md5checksum_ = one_row.md5checksum;
                status_ = one_row.status;
            END LOOP;

            -- Handle rinex QC status ON UPDATE rinex errors
            -- IF status <= 0 THEN NOT synchronize UPDATE rinex errors
            -- IF status > 0  THEN synchronize UPDATE rinex errors
            IF (status_ <= 0) THEN
                RAISE NOTICE '[rinex_errors] QC Status = % - Not creating synchronizing operations for update', status_;
                RETURN NULL;
            END IF;
            -- Get OLD values
            FOR one_row IN
                SELECT error_type FROM rinex_error_types WHERE id = OLD.id_error_type
            LOOP
                old_error_type = one_row.error_type;
            END LOOP;
            -- Get NEW values
            FOR one_row IN
                SELECT error_type FROM rinex_error_types WHERE id = NEW.id_error_type
            LOOP
                aux_error_type = one_row.error_type;
            END LOOP;

            -- Create query
            value_query = 'UPDATE rinex_errors SET ';

            IF NEW.id_error_type IS NOT NULL THEN
                -- Bug fix: We can't simply set id_error_type = to the new id the error type available on the local node!
                -- In the remote node (i.e., DGW) the id may be different
                value_query = value_query || 'id_error_type=(SELECT id from rinex_error_types WHERE error_type = '|| quote_literal(aux_error_type) ||')';
                value_first = false;
            END IF;

            IF NEW.id_rinex_file IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                -- Same fixed fixed, please, see previous bug fix comment
                value_query = value_query || 'id_rinex_file=(SELECT id FROM rinex_file WHERE id_station= '|| id_station_ ||' AND md5checksum = '|| quote_literal(md5checksum_) ||')';
            END IF;

            value_query = value_query || ' WHERE id_rinex_file=(SELECT id FROM rinex_file WHERE id_station= '|| old_id_station ||' AND md5checksum = '|| quote_literal(old_md5checksum) ||') AND id_error_type=(SELECT id from rinex_error_types WHERE error_type = '|| quote_literal(old_error_type) ||');';

            -- Set metadata type
            value_metadata = 'T2';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;


        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_rinex_file = OLD.id_rinex_file;
            id_error_type = OLD.id_error_type;
            old_status    = 0;

            FOR one_row IN
                SELECT id_station, md5checksum, status FROM rinex_file WHERE id = id_rinex_file
            LOOP
                old_id_station = one_row.id_station;
                old_md5checksum = one_row.md5checksum;
                old_status = one_row.status;
            END LOOP;

            -- Handle rinex QC status ON DELETE rinex errors
            -- IF status <= 0 THEN NOT synchronize DELETE rinex errors
            -- IF status > 0  THEN synchronize DELETE rinex errors
            IF (old_status <= 0) THEN
                RAISE NOTICE '[rinex_errors] QC Status = % - Not creating synchronizing operations for delete', old_status;
                RETURN NULL;
            END IF;

            FOR one_row IN
                SELECT error_type FROM rinex_error_types WHERE id = id_error_type
            LOOP
                old_error_type = one_row.error_type;
            END LOOP;

            -- Create query
            value_query = 'DELETE FROM rinex_errors WHERE id_rinex_file=(SELECT id FROM rinex_file WHERE id_station= '|| old_id_station ||' AND md5checksum = '|| quote_literal(old_md5checksum) ||') AND id_error_type=(SELECT id from rinex_error_types WHERE error_type = '|| quote_literal(old_error_type) ||');';
            -- first fail-safe
            IF value_query IS NULL THEN
                RETURN NULL;
            END IF;

            -- Set metadata type
            value_metadata = 'T2';

            -- Set station code
            value_stationID = NULL;

            -- second fail-safe
            -- Insert data to queries
            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the id_error_type exists in table rinex_error_types
            -- Bug fixes:
            -- And if the id_rinex_file exists in table rinex_file
            IF EXISTS(SELECT id FROM rinex_error_types where id=id_error_type) THEN
                IF (EXISTS(SELECT id FROM rinex_file WHERE id=id_rinex_file)) THEN
                    INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
                END IF;
            END IF;
            --
            RETURN NULL;
        END IF;
    END;*/
$$;

DROP TRIGGER IF EXISTS sync_rinex_errors_t2 on rinex_errors;
--CREATE TRIGGER sync_rinex_errors_t2 AFTER INSERT OR UPDATE OR DELETE ON rinex_errors
    --FOR EACH ROW EXECUTE PROCEDURE sync_rinex_errors_t2();


