create or replace function sync_country_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_country integer;
	    name text;
	    iso_code text;

	    old_name text;
        old_iso_code text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_country = NEW.id;
            name = NEW.name;
            iso_code = NEW.iso_code;

            -- Create query
            value_query = 'INSERT INTO country (id, name, iso_code) VALUES ' ||
                '(' || id_country || ', ' || quote_literal(name) || ', ' || quote_literal(iso_code) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_country = OLD.id;
			name = NEW.name;
            iso_code = NEW.iso_code;
			value_first = true;

			 -- Create query
            value_query = 'UPDATE country SET ';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
            END IF;

			-- Handle NULL values
			-- Added possibility to edit id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

			-- Handle NULL values
            IF NEW.iso_code IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'iso_code =' || quote_literal(iso_code);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_country || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_country = OLD.id;

            -- Create query
            value_query = 'DELETE FROM country WHERE id=' || id_country || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_country_t1 on country;
CREATE TRIGGER sync_country_t1 AFTER INSERT OR UPDATE OR DELETE ON country
    FOR EACH ROW EXECUTE PROCEDURE sync_country_t1();


