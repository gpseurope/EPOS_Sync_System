create or replace function sync_condition_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_condition integer;
	    id_station integer;
	    id_effect integer;
	    date_from timestamp;
	    date_to timestamp;
	    degradation text;
	    comments text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_condition = NEW.id;
            id_station = NEW.id_station;
            id_effect = NEW.id_effect;
			date_from = NEW.date_from;
			date_to = NEW.date_to;
			degradation = NEW.degradation;
			comments = NEW.comments;

			-- Create query
            value_query = 'INSERT INTO condition (id, id_station, id_effect';

			IF date_from IS NOT NULL THEN
                value_query = value_query || ', date_from';
            END IF;

			IF date_to IS NOT NULL THEN
                value_query = value_query || ', date_to';
            END IF;

			IF degradation IS NOT NULL THEN
                value_query = value_query || ', degradation';
            END IF;

			IF comments IS NOT NULL THEN
                value_query = value_query || ', comments';
            END IF;

			value_query = value_query || ') VALUES (' || id_condition || ', ' || id_station || ', ' || id_effect;

			IF date_from IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_from);
            END IF;

			IF date_to IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_to);
            END IF;

			IF degradation IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(degradation);
            END IF;

			IF comments IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(comments);
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_condition = OLD.id;
			id_station = NEW.id_station;
            id_effect = NEW.id_effect;
			date_from = NEW.date_from;
			date_to = NEW.date_to;
			degradation = NEW.degradation;
			comments = NEW.comments;
			value_first = true;

			-- Create query
            value_query = 'UPDATE condition SET ';

			-- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station =' || id_station;
                value_first = false;
            END IF;

			-- Handle NULL values
            IF NEW.id_effect IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_effect =' || id_effect;
            END IF;

			-- Handle NULL values
            IF NEW.date_from IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_from =' || quote_literal(date_from);
            END IF;

			-- Handle NULL values
            IF NEW.date_to IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_to =' || quote_literal(date_to);
            END IF;

			-- Handle NULL values
            IF NEW.degradation IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'degradation =' || quote_literal(degradation);
            END IF;

			-- Handle NULL values
            IF NEW.comments IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'comments =' || quote_literal(comments);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_condition || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_condition = OLD.id;

            -- Create query
            value_query = 'DELETE FROM condition WHERE id=' || id_condition || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            -- this is wrong, the id_condition is not the same as the id of the station
            -- value_stationID = id_condition;
            -- set this to correct value
            value_stationID = OLD.id_station;

            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;


            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_condition_t1 on condition;
CREATE TRIGGER sync_condition_t1 AFTER INSERT OR UPDATE OR DELETE ON condition
    FOR EACH ROW EXECUTE PROCEDURE sync_condition_t1();

