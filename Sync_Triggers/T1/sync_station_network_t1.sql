create or replace function sync_station_network_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_station_network integer;
	    id_station integer;
	    id_network integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_station_network = NEW.id;
            id_station = NEW.id_station;
            id_network = NEW.id_network;

            -- Create query
            value_query = 'INSERT INTO station_network (id, id_station, id_network) VALUES ' ||
                '(' || id_station_network || ', ' || id_station || ', ' || id_network || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_station_network = OLD.id;
            id_station = NEW.id_station;
            id_network = NEW.id_network;
            value_first = true;

            -- Create query
            value_query = 'UPDATE station_network SET ';

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || id_station;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.id_network IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_network =' || id_network;
            END IF;

             -- Create query
            value_query = value_query || ' WHERE id=' || id_station_network || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            -- Create query
            value_query = 'DELETE FROM station_network WHERE ';

            -- Handle NULL values
            IF OLD.id IS NOT NULL THEN
                value_query = value_query || 'id=' || OLD.id;
                value_first = false;
            END IF;

            IF OLD.id_station IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_station=' || OLD.id_station;
            END IF;

           -- Handle NULL values
            IF OLD.id_network IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_network=' || OLD.id_network;
            END IF;

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            -- We can't use station_id anymore in the new version of the database
            -- because now station_id is a foreign key, in table queries, that belongs to table station
            -- We can't use a key that was previously removed from the station table

            -- value_stationID = old.id_station;
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_station_network_t1 on station_network;
CREATE TRIGGER sync_station_network_t1 AFTER INSERT OR UPDATE OR DELETE ON station_network
    FOR EACH ROW EXECUTE PROCEDURE sync_station_network_t1();

