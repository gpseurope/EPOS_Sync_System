create or replace function sync_station_item_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_station_item integer;
	    id_station integer;
	    id_item integer;
	    date_from timestamp;
	    date_to timestamp;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_station_item = NEW.id;
            id_station = NEW.id_station;
            id_item = NEW.id_item;
            date_from = NEW.date_from;
            date_to = NEW.date_to;

            -- Create query
            value_query = 'INSERT INTO station_item (id, id_station, id_item, date_from';

            IF date_to IS NOT NULL THEN
                value_query = value_query || ', date_to';
            END IF;

            value_query = value_query || ') VALUES (' || id_station_item || ', ' || id_station || ', ' || id_item || ', ' || quote_literal(date_from);


            IF date_to IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_to);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_station_item = OLD.id;
            id_station = NEW.id_station;
            id_item = NEW.id_item;
            date_from = NEW.date_from;
            date_to = NEW.date_to;
            value_first = true;

            -- Create query
            value_query = 'UPDATE station_item SET ';

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || id_station;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.id_item IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_item =' || id_item;
            END IF;

            -- Handle NULL values
            IF NEW.date_from IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_from =' || quote_literal(date_from);
            END IF;

            -- Handle NULL values
            IF NEW.date_to IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_to =' || quote_literal(date_to);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_station_item || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_station_item = OLD.id;

            -- Create query
            -- By setting value_stationID to NULL we need to ensure that the correct entry is removed
            -- in the remote node (not use only ID)
            value_query = 'DELETE FROM station_item WHERE id=' || id_station_item || ' AND ';

            -- Handle NULL values
            IF OLD.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || OLD.id_station;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF OLD.id_item IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_item =' || OLD.id_item;
            END IF;

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
               -- We can't use station_id anymore in the new version of the database
               -- because now station_id is a foreign key, in table queries, that belongs to table station
               -- We can't use a key that was previously removed from the station table
            -- value_stationID = old.id_station;
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_station_item_t1 on station_item;
CREATE TRIGGER sync_station_item_t1 AFTER INSERT OR UPDATE OR DELETE ON station_item
    FOR EACH ROW EXECUTE PROCEDURE sync_station_item_t1();

