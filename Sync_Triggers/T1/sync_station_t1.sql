create or replace function sync_station_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;
      value_query2 text;

	    id_station integer;
	    name text;
	    marker text;
	    description text;
	    date_from timestamp;
	    date_to timestamp;
	    id_station_type integer;
	    comment text;
	    id_location integer;
	    id_monument integer;
	    id_geological integer;
	    iers_domes text;
	    cpd_num text;
	    monument_num integer;
	    receiver_num integer;
	    country_code text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_station = NEW.id;
            name = NEW.name;
            marker = NEW.marker;
            description = NEW.description;
            date_from = NEW.date_from;
            date_to = NEW.date_to;
            id_station_type = NEW.id_station_type;
            comment = NEW.comment;
            id_location = NEW.id_location;
            id_monument = NEW.id_monument;
            id_geological = NEW.id_geological;
            iers_domes = NEW.iers_domes;
            cpd_num = NEW.cpd_num;
            monument_num = NEW.monument_num;
            receiver_num = NEW.receiver_num;
            country_code = NEW.country_code;

            -- Create query
            value_query = 'INSERT INTO station (id, name, marker';

            IF description IS NOT NULL THEN
                value_query = value_query || ', description';
            END IF;

            IF date_from IS NOT NULL THEN
                 value_query = value_query || ', date_from';
            END IF;

            IF date_to IS NOT NULL THEN
                 value_query = value_query || ', date_to';
            END IF;

            IF id_station_type IS NOT NULL THEN
                 value_query = value_query || ', id_station_type';
            END IF;

            IF comment IS NOT NULL THEN
                 value_query = value_query || ', comment';
            END IF;

            IF id_location IS NOT NULL THEN
                 value_query = value_query || ', id_location';
            END IF;

            IF id_monument IS NOT NULL THEN
                 value_query = value_query || ', id_monument';
            END IF;

            IF id_geological IS NOT NULL THEN
                 value_query = value_query || ', id_geological';
            END IF;

            IF iers_domes IS NOT NULL THEN
                 value_query = value_query || ', iers_domes';
            END IF;

            IF cpd_num IS NOT NULL THEN
                 value_query = value_query || ', cpd_num';
            END IF;

            IF monument_num IS NOT NULL THEN
                 value_query = value_query || ', monument_num';
            END IF;

            IF receiver_num IS NOT NULL THEN
                 value_query = value_query || ', receiver_num';
            END IF;

            IF country_code IS NOT NULL THEN
                 value_query = value_query || ', country_code';
            END IF;

            value_query = value_query || ') VALUES (' || id_station || ', ' || quote_literal(name) || ', ' || quote_literal(marker);

            IF description IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(description);
            END IF;

            IF date_from IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(date_from);
            END IF;

            IF date_to IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(date_to);
            END IF;

            IF id_station_type IS NOT NULL THEN
                 value_query = value_query || ', ' || id_station_type;
            END IF;

            IF comment IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(comment);
            END IF;

            IF id_location IS NOT NULL THEN
                 value_query = value_query || ', ' || id_location;
            END IF;

            IF id_monument IS NOT NULL THEN
                 value_query = value_query || ', ' || id_monument;
            END IF;

            IF id_geological IS NOT NULL THEN
                 value_query = value_query || ', ' || id_geological;
            END IF;

            IF iers_domes IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(iers_domes);
            END IF;

            IF cpd_num IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(cpd_num);
            END IF;

            IF monument_num IS NOT NULL THEN
                 value_query = value_query || ', ' || monument_num;
            END IF;

            IF receiver_num IS NOT NULL THEN
                 value_query = value_query || ', '  || receiver_num;
            END IF;

            IF country_code IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(country_code);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_station = OLD.id;
            name = NEW.name;
            marker = NEW.marker;
            description = NEW.description;
            date_from = NEW.date_from;
            date_to = NEW.date_to;
            id_station_type = NEW.id_station_type;
            comment = NEW.comment;
            id_location = NEW.id_location;
            id_monument = NEW.id_monument;
            id_geological = NEW.id_geological;
            iers_domes = NEW.iers_domes;
            cpd_num = NEW.cpd_num;
            monument_num = NEW.monument_num;
            receiver_num = NEW.receiver_num;
            country_code = NEW.country_code;
            value_first = true;

            -- Create query
            value_query = 'UPDATE station SET ';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.marker IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'marker =' || quote_literal(marker);
            END IF;

            -- Handle NULL values
            IF NEW.description IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'description =' || quote_literal(description);
            END IF;

            -- Handle NULL values
            IF NEW.date_from IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_from =' || quote_literal(date_from);
            END IF;

            -- Handle NULL values
            IF NEW.date_to IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_to =' || quote_literal(date_to);
            END IF;

            -- Handle NULL values
            IF NEW.id_station_type IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_station_type =' || id_station_type;
            END IF;

            -- Handle NULL values
            IF NEW.comment IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'comment =' || quote_literal(comment);
            END IF;

            -- Handle NULL values
            IF NEW.id_location IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_location =' || id_location;
            END IF;

            -- Handle NULL values
            IF NEW.id_monument IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_monument =' || id_monument;
            END IF;

            -- Handle NULL values
            IF NEW.id_geological IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                -- Bug fix, it was id_monument
                value_query = value_query || 'id_geological =' || id_geological;
            END IF;

            -- Handle NULL values
            IF NEW.iers_domes IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'iers_domes =' || quote_literal(iers_domes);
            END IF;

            -- Handle NULL values
            IF NEW.cpd_num IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'cpd_num =' || quote_literal(cpd_num);
            END IF;

            -- Handle NULL values
            IF NEW.monument_num IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'monument_num =' || monument_num;
            END IF;

            -- Handle NULL values
            IF NEW.receiver_num IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'receiver_num =' || receiver_num;
            END IF;

            -- Handle NULL values
            IF NEW.country_code IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'country_code =' || quote_literal(country_code);
            END IF;

            -- Handle NULL values
            -- Added possibility to edit id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id =' || NEW.id;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_station || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            IF (NEW.id != OLD.id) THEN
                value_stationID = NEW.id;
            ELSE
                value_stationID = OLD.id;
            END IF;


            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_station = OLD.id;

            -- Create query
            value_query = 'DELETE FROM station WHERE id=' || id_station || ';';
            -- every time that a station is deleted, it´s also need to delete all the entries from queries which have the same station id
            value_query2 = 'DELETE FROM queries WHERE station_id=' || id_station || ';';

            -- Set metadata type
            value_metadata = 'T1';
            --value_metadata2 = 'T1';

            -- Set station code
            value_stationID = NULL;
            --value_stationID2 = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query2, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_station_t1 on station;
CREATE TRIGGER sync_station_t1 AFTER INSERT OR UPDATE OR DELETE ON station
    FOR EACH ROW EXECUTE PROCEDURE sync_station_t1();


