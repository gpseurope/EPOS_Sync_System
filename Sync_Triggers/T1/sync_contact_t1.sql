create or replace function sync_contact_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_contact integer;
	    name text;
	    title text;
	    email text;
	    phone text;
	    gsm text;
	    comment text;
	    id_agency integer;
	    role text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_contact = NEW.id;
            name = NEW.name;
            title = NEW.title;
            email = NEW.email;
            phone = NEW.phone;
            gsm = NEW.gsm;
            comment = NEW.comment;
            id_agency = NEW.id_agency;
            role = NEW.role;

			-- Create query
            value_query = 'INSERT INTO contact (id, name, id_agency';

			IF title IS NOT NULL THEN
                value_query = value_query || ', title';
            END IF;

			IF email IS NOT NULL THEN
                value_query = value_query || ', email';
            ELSE
                 raise notice 'Warning On Inserting new Contact - Email Can Not Be NUll';
            END IF;

			IF phone IS NOT NULL THEN
                value_query = value_query || ', phone';
            END IF;

			IF gsm IS NOT NULL THEN
                value_query = value_query || ', gsm';
            END IF;

			IF comment IS NOT NULL THEN
                value_query = value_query || ', comment';
            END IF;

			IF role IS NOT NULL THEN
                value_query = value_query || ', role';
            END IF;

            --start to add the values
			value_query = value_query || ') VALUES (' || id_contact || ', ' || quote_literal(name) || ', ' || id_agency;

			IF title IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(title);
            END IF;

			IF email IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(email);
            END IF;

			IF phone IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(phone);
            END IF;

			IF gsm IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(gsm);
            END IF;

			IF comment IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(comment);
            END IF;

			IF role IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(role);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        -- 7/2/24 Permite NULLS for non mandatory fields
        -- 7/2/42 raise error message when trying to change mandatory or unique fields to null
        IF (TG_OP = 'UPDATE') THEN
            id_contact = OLD.id;
			name = NEW.name;
            title = NEW.title;
            email = NEW.email;
            phone = NEW.phone;
            gsm = NEW.gsm;
            comment = NEW.comment;
            id_agency = NEW.id_agency;
            role = NEW.role;
			value_first = true;

			-- Create query
            value_query = 'UPDATE contact SET ';

			-- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name= ' || quote_literal(name);
                value_first = false;
            ELSE
                raise notice 'Warning On Update Contact - name Can Not Be NUll';
            END IF;

			-- Handle NULL values
			-- Added possibility to edit id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

			-- Permit NULL values and alter only on change of value
            IF NEW.title != OLD.title then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'title = ' || quote_literal(title);
            END IF;

			-- Handle NULL values
            IF NEW.email IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'email = ' || quote_literal(email);
            ELSE
                raise notice 'Warning On Update Contact - Email Can Not Be NUll';
            END IF;

			-- Permit NULL values and alter only on change of value
            IF NEW.phone != OLD.phone then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'phone = ' || quote_literal(phone);
            END IF;

			-- Permit NULL values and alter only on change of value
            IF NEW.gsm != OLD.gsm then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'gsm = ' || quote_literal(gsm);
            END IF;

			-- Permit NULL values and alter only on change of value
            IF NEW.comment != OLD.comment then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'comment = ' || quote_literal(comment);
            END IF;

			-- Handle NULL values
            IF NEW.id_agency IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_agency =' || id_agency;
            ELSE
                raise notice 'Warning On Update Contact - Email if agency can not be null';
            END IF;

			-- Permit NULL values and alter only on change of value
            IF NEW.role != OLD.role then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'role = ' || quote_literal(role);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_contact || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_contact = OLD.id;

            -- Create query
            value_query = 'DELETE FROM contact WHERE id=' || id_contact || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_contact_t1 on contact;
CREATE TRIGGER sync_contact_t1 AFTER INSERT OR UPDATE OR DELETE ON contact
    FOR EACH ROW EXECUTE PROCEDURE sync_contact_t1();

