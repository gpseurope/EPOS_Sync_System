create or replace function sync_coordinates_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_coordinates integer;
	    x numeric;
	    y numeric;
	    z numeric;
	    lat numeric;
	    lon numeric;
	    altitude numeric;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_coordinates = NEW.id;
            x = NEW.x;
            y = NEW.y;
            z = NEW.z;
            lat = NEW.lat;
            lon = NEW.lon;
            altitude = NEW.altitude;

			-- Create query
            value_query = 'INSERT INTO coordinates (id, x, y, z';

			IF lat IS NOT NULL THEN
                value_query = value_query || ', lat';
            END IF;

			IF lon IS NOT NULL THEN
                value_query = value_query || ', lon';
            END IF;

			IF altitude IS NOT NULL THEN
                value_query = value_query || ', altitude';
            END IF;

            value_query = value_query || ') VALUES (' || id_coordinates || ', ' || x || ', ' || y || ', ' || z;

			IF lat IS NOT NULL THEN
                value_query = value_query || ', ' || lat;
            END IF;

			IF lon IS NOT NULL THEN
                value_query = value_query || ', ' || lon;
            END IF;

			IF altitude IS NOT NULL THEN
                value_query = value_query || ', ' || altitude;
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_coordinates = OLD.id;
			x = NEW.x;
            y = NEW.y;
            z = NEW.z;
            lat = NEW.lat;
            lon = NEW.lon;
            altitude = NEW.altitude;
			value_first = true;

			-- Create query
            value_query = 'UPDATE coordinates SET ';

			-- Handle NULL values
            IF NEW.x IS NOT NULL THEN
                value_query = value_query || 'x=' || x;
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Add possibility to change ID
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_query = false;
                END IF;
                value_query = value_query || 'id =' || NEW.id;
            END IF;

			-- Handle NULL values
            IF NEW.y IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_query = false;
                END IF;
                value_query = value_query || 'y =' || y;
            END IF;

			-- Handle NULL values
            IF NEW.z IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'z =' || z;
            END IF;

			-- Handle NULL values
            IF NEW.lat IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_query = false;
                END IF;
                value_query = value_query || 'lat =' || lat;
            END IF;

			-- Handle NULL values
            IF NEW.lon IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_query = false;
                END IF;
                value_query = value_query || 'lon =' || lon;
            END IF;

			-- Handle NULL values
            IF NEW.altitude IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_query = false;
                END IF;
                value_query = value_query || 'altitude =' || altitude;
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_coordinates || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_coordinates = OLD.id;

            -- Create query
            value_query = 'DELETE FROM coordinates WHERE id=' || id_coordinates || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_coordinates_t1 on coordinates;
CREATE TRIGGER sync_coordinates_t1 AFTER INSERT OR UPDATE OR DELETE ON coordinates
    FOR EACH ROW EXECUTE PROCEDURE sync_coordinates_t1();

