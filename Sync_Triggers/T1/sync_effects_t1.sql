create or replace function sync_effects_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_effects integer;
	    type text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_effects = NEW.id;
            type = NEW.type;

            -- Create query
            value_query = 'INSERT INTO effects (id, type) VALUES ' ||
                '(' || id_effects || ', ' || quote_literal(type) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_effects = OLD.id;
            type = NEW.type;
			value_first = true;

            -- Create query
            -- Added possibility to update ID
            value_query = 'UPDATE effects SET id=' || NEW.id ||', type=' || quote_literal(type) || ' WHERE id=' || id_effects || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_effects = OLD.id;

            -- Create query
            value_query = 'DELETE FROM effects WHERE id=' || id_effects || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
			
            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_effects_t1 on effects;
CREATE TRIGGER sync_effects_t1 AFTER INSERT OR UPDATE OR DELETE ON effects
    FOR EACH ROW EXECUTE PROCEDURE sync_effects_t1();

