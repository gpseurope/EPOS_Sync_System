create or replace function sync_log_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_log integer;
	    title text;
	    date_ date;
	    id_log_type integer;
	    id_station integer;
	    modified text;
      previous text;
	    id_contact integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_log = NEW.id;
            title = NEW.title;
            date_ = NEW.date;
            id_log_type = NEW.id_log_type;
            id_station = NEW.id_station;
            modified = NEW.modified;
            previous = NEW.previous;
            id_contact = NEW.id_contact;

            -- Create query
            value_query = 'INSERT INTO log (id, title, id_log_type, id_station';

            IF date_ IS NOT NULL THEN
                value_query = value_query || ', date';
            END IF;

            IF modified IS NOT NULL THEN
                value_query = value_query || ', modified';
            END IF;

            IF previous IS NOT NULL THEN
                value_query = value_query || ', previous';
            END IF;

            IF id_contact IS NOT NULL THEN
                value_query = value_query || ', id_contact';
            END IF;

            value_query = value_query || ') VALUES (' || id_log || ', ' || quote_literal(title) || ', ' || id_log_type || ', ' || id_station;

            IF date_ IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_);
            END IF;

            IF modified IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(modified);
            END IF;

            IF previous IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(previous);
            END IF;

            IF id_contact IS NOT NULL THEN
                value_query = value_query || ', ' || id_contact;
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_log = OLD.id;
            title = NEW.title;
            date_ = NEW.date;
            id_log_type = NEW.id_log_type;
            id_station = NEW.id_station;
            modified = NEW.modified;
            previous = NEW.previous;
            id_contact = NEW.id_contact;
            value_first = true;

            -- Create query
            value_query = 'UPDATE log SET ';

            -- Handle NULL values
            IF NEW.title IS NOT NULL THEN
                value_query = value_query || 'title=' || quote_literal(title);
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.date IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date =' || quote_literal(date_);
            END IF;

            -- Handle NULL values
            IF NEW.id_log_type IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_log_type =' || id_log_type;
            END IF;

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_station =' || id_station;
            END IF;

            -- Handle NULL values
            IF NEW.modified IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'modified =' || quote_literal(modified);
            END IF;

            IF NEW.previous IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'previous =' || quote_literal(previous);
            END IF;

            -- Handle NULL values
            IF NEW.id_contact IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_contact =' || id_contact;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_log || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_log = OLD.id;
            id_station = OLD.id_station;

            -- Create query
            value_query = 'DELETE FROM log WHERE ';

            -- By setting value_station ID to NULL we need to ensure that the correct entry is removed
            -- in the remote node (not use only ID)
            -- Handle NULL values
            IF OLD.id IS NOT NULL THEN
                value_query = value_query || 'id=' || quote_literal(OLD.id);
                value_first = false;
            END IF;

            -- Handle NULL values
            IF OLD.title IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'title =' || quote_literal(OLD.title);
            END IF;

            -- Handle NULL values
            IF OLD.date IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date =' || quote_literal(OLD.date);
            END IF;

            -- Handle NULL values
            IF OLD.id_log_type IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_log_type =' || OLD.id_log_type;
            END IF;

            -- Handle NULL values
            IF OLD.id_station IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_station =' || OLD.id_station;
            END IF;

            -- Handle NULL values
            IF OLD.modified IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'modified =' || quote_literal(OLD.modified);
            END IF;

            IF OLD.previous IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'previous =' || quote_literal(OLD.previous);
            END IF;

            -- Handle NULL values
            IF OLD.id_contact IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_contact =' || OLD.id_contact;
            END IF;


            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
                -- We can't use station_id anymore in the new version of the database
                -- because now station_id is a foreign key, in table queries, that belongs to table station
                -- We can't use a key that was previously removed from the station table
            -- value_stationID = old.id_station;

            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_log_t1 on log;
CREATE TRIGGER sync_log_t1 AFTER INSERT OR UPDATE OR DELETE ON log
    FOR EACH ROW EXECUTE PROCEDURE sync_log_t1();

