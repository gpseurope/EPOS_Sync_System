create or replace function sync_colocation_offset_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_colocation_offset integer;
	    id_station_colocation integer;
	    offset_x numeric;
	    offset_y numeric;
	    offset_z numeric;
	    date_measured text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_colocation_offset = NEW.id;
            id_station_colocation = NEW.id_station_colocation;
			offset_x = NEW.offset_x;
			offset_y = NEW.offset_y;
			offset_z = NEW.offset_z;
			date_measured = NEW.date_measured;

			-- Create query
            value_query = 'INSERT INTO colocation_offset (id, id_station_colocation, date_measured';

			IF offset_x IS NOT NULL THEN
                value_query = value_query || ', offset_x';
            END IF;

			IF offset_y IS NOT NULL THEN
                value_query = value_query || ', offset_y';
            END IF;

			IF offset_z IS NOT NULL THEN
                value_query = value_query || ', offset_z';
            END IF;

			value_query = value_query || ') VALUES (' || id_colocation_offset || ', ' || id_station_colocation || ', ' || quote_literal(date_measured);

			IF offset_x IS NOT NULL THEN
                value_query = value_query || ', ' || offset_x;
            END IF;

			IF offset_y IS NOT NULL THEN
                value_query = value_query || ', ' || offset_y;
            END IF;

			IF offset_z IS NOT NULL THEN
                value_query = value_query || ', ' || offset_z;
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            -- value_stationID = NULL; It cannot be NULL because the station may not exist on the destiny node
            -- so, if we try to insert with a station that is not available on the destiny node an error will
            -- occur.
            -- Lets assign to value_stationID the right one
            SELECT id_station INTO value_stationID FROM station_colocation WHERE id = id_station_colocation;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_colocation_offset = OLD.id;
			id_station_colocation = NEW.id_station_colocation;
			offset_x = NEW.offset_x;
			offset_y = NEW.offset_y;
			offset_z = NEW.offset_z;
			date_measured = NEW.date_measured;
			value_first = true;

			-- Create query
            value_query = 'UPDATE colocation_offset SET ';

			-- Handle NULL values
            IF NEW.id_station_colocation IS NOT NULL THEN
                value_query = value_query || 'id_station_colocation=' || id_station_colocation;
                value_first = false;
            END IF;

			-- Handle NULL values
            IF NEW.offset_x IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'offset_x =' || offset_x;
            END IF;

			-- Handle NULL values
            IF NEW.offset_y IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'offset_y =' || offset_y;
            END IF;

			-- Handle NULL values
            IF NEW.offset_z IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'offset_z =' || offset_z;
            END IF;

			-- Handle NULL values
            IF NEW.date_measured IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_measured =' || quote_literal(date_measured);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_colocation_offset || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            -- value_stationID = NULL;
            -- It cannot be NULL because the station may not exist on the destiny node
            -- so, if we try to insert with a station that is not available on the destiny node an error will
            -- occur.
            -- Lets assign to value_stationID the right one
            SELECT id_station INTO value_stationID FROM station_colocation WHERE id = id_station_colocation;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_colocation_offset = OLD.id;
            id_station_colocation = OLD.id_station_colocation;

            -- Create query
            value_query = 'DELETE FROM colocation_offset WHERE id=' || id_colocation_offset || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            -- value_stationID = NULL;
            -- It cannot be NULL because the station may not exist on the destiny node
            SELECT id_station INTO value_stationID FROM station_colocation WHERE id = id_station_colocation;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_colocation_offset_t1 on colocation_offset;
CREATE TRIGGER sync_colocation_offset_t1 AFTER INSERT OR UPDATE OR DELETE ON colocation_offset
    FOR EACH ROW EXECUTE PROCEDURE sync_colocation_offset_t1();
    

    