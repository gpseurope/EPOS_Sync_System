create or replace function sync_location_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_location integer;
	    id_city integer;
	    id_coordinates integer;
	    id_tectonic integer;
	    description text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_location = NEW.id;
            id_city = NEW.id_city;
            id_coordinates = NEW.id_coordinates;
            id_tectonic = NEW.id_tectonic;
            description = NEW.description;

            -- Create query
            value_query = 'INSERT INTO location (id, id_city, id_coordinates';

            IF id_tectonic IS NOT NULL THEN
                value_query = value_query || ', id_tectonic';
            END IF;

            IF description IS NOT NULL THEN
                value_query = value_query || ', description';
            END IF;

            value_query = value_query || ') VALUES (' || id_location || ', ' || id_city || ', ' || id_coordinates;

            IF id_tectonic IS NOT NULL THEN
                value_query = value_query || ', ' || id_tectonic;
            END IF;

            IF description IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(description);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_location = OLD.id;
            id_city = NEW.id_city;
            id_coordinates = NEW.id_coordinates;
            id_tectonic = NEW.id_tectonic;
            description = NEW.description;
            value_first = true;

            -- Create query
            value_query = 'UPDATE location SET ';

            -- Handle NULL values
            IF NEW.id_city IS NOT NULL THEN
                value_query = value_query || 'id_city=' || id_city;
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Added possibility to edit ID
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

            -- Handle NULL values
            IF NEW.id_coordinates IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_coordinates =' || id_coordinates;
            END IF;

            -- Handle NULL values
            IF NEW.id_tectonic IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_tectonic =' || id_tectonic;
            END IF;

            -- Handle NULL values
            IF NEW.description IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'description =' || quote_literal(description);
            END IF;

             -- Create query
            value_query = value_query || ' WHERE id=' || id_location || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_location = OLD.id;

            -- Create query
            value_query = 'DELETE FROM location WHERE id=' || id_location || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_location_t1 on location;
CREATE TRIGGER sync_location_t1 AFTER INSERT OR UPDATE OR DELETE ON location
    FOR EACH ROW EXECUTE PROCEDURE sync_location_t1();

