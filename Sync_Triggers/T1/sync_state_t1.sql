create or replace function sync_state_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_state integer;
	    id_country integer;
	    name text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_state = NEW.id;
            id_country = NEW.id_country;
            name = NEW.name;

            -- Create query
            value_query = 'INSERT INTO state (id, id_country, name) VALUES ' ||
                '(' || id_state || ', ' || id_country || ', ' || quote_literal(name) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_state = OLD.id;
            id_country = NEW.id_country;
            name = NEW.name;

            -- Create query
            value_query = 'UPDATE state SET ';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Added possibility to update id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id =' || NEW.id;
            END IF;

            -- Handle NULL values
            IF NEW.id_country IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_country =' || id_country;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_state || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_state = OLD.id;

            -- Create query
            value_query = 'DELETE FROM state WHERE id=' || id_state || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_state_t1 on state;
CREATE TRIGGER sync_state_t1 AFTER INSERT OR UPDATE OR DELETE ON state
    FOR EACH ROW EXECUTE PROCEDURE sync_state_t1();

    
