CREATE OR REPLACE FUNCTION sync_data_center_t1() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
    value_query text;
    value_metadata text;
    value_stationID integer;
    value_first boolean;

    id_data_center integer;
    id_agency integer;
    name text;
    acronym text;
    protocol text;
    hostname text;
    root_path text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_data_center = NEW.id;
            id_agency = NEW.id_agency;
            name = NEW.name;
            acronym = NEW.acronym;
            protocol = NEW.protocol;
            hostname = NEW.hostname;
            root_path = NEW.root_path;

            -- Create query
            value_query = 'INSERT INTO data_center (id, name, acronym, protocol, hostname, root_path';

            IF id_agency IS NOT NULL THEN
                value_query = value_query || ', id_agency';
            END IF;

            value_query = value_query || ') VALUES (' || id_data_center || ', ' || quote_literal(name) || ', ' || quote_literal(acronym) || ', ' || quote_literal(protocol) || ', ' || quote_literal(hostname) || ', ' || quote_literal(root_path);

            IF id_agency IS NOT NULL THEN
                value_query = value_query || ', ' || id_agency;
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_data_center = OLD.id;
            id_agency = NEW.id_agency;
            name = NEW.name;
            acronym = NEW.acronym;
            protocol = NEW.protocol;
            hostname = NEW.hostname;
            root_path = NEW.root_path;
            value_first = true;

            -- Create query
            -- Added possibility to update id
            value_query = 'UPDATE data_center SET id=' || NEW.id || ', name=' || quote_literal(name) || ', acronym=' || quote_literal(acronym) || ', protocol=' || quote_literal(protocol) || ', hostname=' || quote_literal(hostname) || ', root_path=' || quote_literal(root_path);

            -- Handle NULL values
            IF NEW.id_agency IS NOT NULL THEN
                value_query = value_query || ', id_agency=' || id_agency;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_data_center || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_data_center = OLD.id;

            -- Create query
            value_query = 'DELETE FROM data_center WHERE id=' || id_data_center || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$;

DROP TRIGGER IF EXISTS sync_data_center_t1 on data_center;
CREATE TRIGGER sync_data_center_t1 AFTER INSERT OR UPDATE OR DELETE ON data_center
    FOR EACH ROW EXECUTE PROCEDURE sync_data_center_t1();


