create or replace function sync_bedrock_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_bedrock integer;
	    condition text;
	    type text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_bedrock = NEW.id;
            condition = NEW.condition;
            type = NEW.type;

			-- Create query
            value_query = 'INSERT INTO bedrock (id, condition';

            IF type IS NOT NULL THEN
                value_query = value_query || ', type';
            END IF;

			value_query = value_query || ') VALUES (' || id_bedrock || ', ' || quote_literal(condition);

			IF type IS NOT NULL THEN
                 value_query = value_query || ', ' || quote_literal(type);
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_bedrock = OLD.id;
			condition = NEW.condition;
            type = NEW.type;
			value_first = true;

			-- Create query
            value_query = 'UPDATE bedrock SET ';

			-- Handle NULL values
            IF NEW.condition IS NOT NULL THEN
                value_query = value_query || 'condition=' || quote_literal(condition);
                value_first = false;
            END IF;

			-- Handle NULL values
			-- Added possibility to edit id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

			-- Handle NULL values
            IF NEW.type IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'type =' || quote_literal(type);
            END IF;

			 -- Create query
            value_query = value_query || ' WHERE id=' || id_bedrock || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_bedrock = OLD.id;

            -- Create query
            value_query = 'DELETE FROM bedrock WHERE id=' || id_bedrock || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_bedrock_t1 on bedrock;
CREATE TRIGGER sync_bedrock_t1 AFTER INSERT OR UPDATE OR DELETE ON bedrock
    FOR EACH ROW EXECUTE PROCEDURE sync_bedrock_t1();

    