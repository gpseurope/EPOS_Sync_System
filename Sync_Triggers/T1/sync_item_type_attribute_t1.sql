create or replace function sync_item_type_attribute_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_item_type_attribute integer;
	    id_item_type integer;
	    id_attribute integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_item_type_attribute = NEW.id;
            id_item_type = NEW.id_item_type;
            id_attribute = NEW.id_attribute;

            -- Create query
            value_query = 'INSERT INTO item_type_attribute (id, id_item_type, id_attribute) VALUES ' ||
                '(' || id_item_type_attribute || ', ' || id_item_type || ', ' || id_attribute || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_item_type_attribute = OLD.id;
			      id_item_type = NEW.id_item_type;
            id_attribute = NEW.id_attribute;
			      value_first = true;

			-- Create query
            value_query = 'UPDATE item_type_attribute SET ';

            -- Handle NULL values
            IF NEW.id_item_type IS NOT NULL THEN
                value_query = value_query || 'id_item_type=' || id_item_type;
                value_first = false;
            END IF;

			-- Handle NULL values
            IF NEW.id_attribute IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_attribute =' || id_attribute;
            END IF;

             -- Create query
            value_query = value_query || ' WHERE id=' || id_item_type_attribute || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_item_type_attribute = OLD.id;

            -- Create query
            value_query = 'DELETE FROM item_type_attribute WHERE id=' || id_item_type_attribute || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_item_type_attribute_t1 on item_type_attribute;
CREATE TRIGGER sync_item_type_attribute_t1 AFTER INSERT OR UPDATE OR DELETE ON item_type_attribute
    FOR EACH ROW EXECUTE PROCEDURE sync_item_type_attribute_t1();


