create or replace function sync_instrument_collocation_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_instrument_collocation integer;
	    id_station integer;
	    type text;
        status text;
	    date_from timestamp;
	    date_to timestamp;
	    comment text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_instrument_collocation = NEW.id;
            id_station = NEW.id_station;
            type = NEW.type;
            status = NEW.status;
			date_from = NEW.date_from;
			date_to = NEW.date_to;
			comment = NEW.comment;

			-- Create query
            value_query = 'INSERT INTO instrument_collocation (id, id_station, type';

            IF status IS NOT NULL THEN
                value_query = value_query || ', status';
            END IF;

			IF date_from IS NOT NULL THEN
                value_query = value_query || ', date_from';
            END IF;

			IF date_to IS NOT NULL THEN
                value_query = value_query || ', date_to';
            END IF;

			IF comment IS NOT NULL THEN
                value_query = value_query || ', comment';
            END IF;

			value_query = value_query || ') VALUES (' || id_instrument_collocation || ', ' || id_station || ', ' || quote_literal(type);

            IF status IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(status);
            END IF;

			IF date_from IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_from);
            END IF;

			IF date_to IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_to);
            END IF;

			IF comment IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(comment);
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_instrument_collocation = OLD.id;
			id_station = NEW.id_station;
            type = NEW.type;
            status = NEW.status;
			date_from = NEW.date_from;
			date_to = NEW.date_to;
			comment = NEW.comment;
			value_first = true;

			-- Create query
            value_query = 'UPDATE instrument_collocation SET ';

			-- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || id_station;
                value_first = false;
            END IF;

			-- Handle NULL values
            IF NEW.type IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'type =' || quote_literal(type);
            END IF;

            -- Handle NULL values
            IF NEW.status IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'status =' || quote_literal(status);
            END IF;

			-- Handle NULL values
            IF NEW.date_from IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_from =' || quote_literal(date_from);
            END IF;

			-- Handle NULL values
            IF NEW.date_to IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_to =' || quote_literal(date_to);
            END IF;

			-- Handle NULL values
            IF NEW.comment IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'comment =' || quote_literal(comment);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_instrument_collocation || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_instrument_collocation = OLD.id;
            id_station = OLD.id_station;
            -- Create query
            value_query = 'DELETE FROM instrument_collocation WHERE id=' || id_instrument_collocation || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            -- Bug fix it was id_instrument_collocation it must be id_station!
            value_stationID = id_station;


            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_instrument_collocation_t1 on instrument_collocation;
CREATE TRIGGER sync_instrument_collocation_t1 AFTER INSERT OR UPDATE OR DELETE ON instrument_collocation
    FOR EACH ROW EXECUTE PROCEDURE sync_instrument_collocation_t1();

    