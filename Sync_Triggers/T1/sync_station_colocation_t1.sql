create or replace function sync_station_colocation_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_station_colocation integer;
	    id_station integer;
	    id_station_colocated integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_station_colocation = NEW.id;
            id_station = NEW.id_station;
            id_station_colocated = NEW.id_station_colocated;

            -- Create query
            value_query = 'INSERT INTO station_colocation (id, id_station, id_station_colocated) VALUES ' ||
                '(' || id_station_colocation || ', ' || id_station || ', ' || id_station_colocated || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_station_colocation = OLD.id;
            id_station = NEW.id_station;
            id_station_colocated = NEW.id_station_colocated;

            -- Create query
            value_query = 'UPDATE station_colocation SET ';

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || id_station;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

            -- Handle NULL values
            IF NEW.id_station_colocated IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_station_colocated =' || id_station_colocated;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_station_colocation || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_station_colocation = OLD.id;
            id_station = OLD.id_station;

            -- Create query
            value_query = 'DELETE FROM station_colocation WHERE id=' || id_station_colocation || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_station_colocation_t1 on station_colocation;
CREATE TRIGGER sync_station_colocation_t1 AFTER INSERT OR UPDATE OR DELETE ON station_colocation
    FOR EACH ROW EXECUTE PROCEDURE sync_station_colocation_t1();

    
