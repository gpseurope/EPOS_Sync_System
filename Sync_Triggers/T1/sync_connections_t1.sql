CREATE OR REPLACE FUNCTION sync_connections_t1() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
	value_query 	text;
	value_values	text;
	one_row    		RECORD;
    BEGIN

        -- Handle insert
        IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') then

		    -- Handle station not NULL and metadata = T1
			IF (NEW.station IS NOT NULL AND NEW.metadata = 'T1') THEN
				-- IF NOT EXISTS (SELECT * FROM queries WHERE query ILIKE '%insert into station % values (' || NEW.station || ',%') THEN
                -- BUG FIXES
                IF NOT EXISTS(SELECT * FROM queries WHERE query ILIKE '%insert into station % values ('|| NEW.station ||', %)') THEN

                    raise notice 'Query to insert a new station is MIA for station %', NEW.station;
                    -- STATION TABLE BEGIN --
				    	FOR one_row IN 
					        SELECT id, name, marker, description, date_from, date_to, id_station_type, comment, id_location, id_monument, id_geological, iers_domes, cpd_num, monument_num, receiver_num, country_code FROM station WHERE id = NEW.station
					    LOOP 
					    	value_query = 'INSERT INTO station (id, name, marker';
					    	value_values = 'VALUES (' || one_row.id || ', ' || quote_literal(one_row.name) || ', ' || quote_literal(one_row.marker);

					    	IF one_row.description IS NOT NULL THEN
								value_query =  value_query || ', description';
								value_values = value_values || ', ' || quote_literal(one_row.description);
				            END IF;

					    	IF one_row.date_from IS NOT NULL THEN
								value_query =  value_query || ', date_from';
								value_values = value_values || ', ' || quote_literal(one_row.date_from);
				            END IF;

				            IF one_row.date_to IS NOT NULL THEN
								value_query =  value_query || ', date_to';
								value_values = value_values || ', ' || quote_literal(one_row.date_to);
				            END IF;

				            IF one_row.id_station_type IS NOT NULL THEN
								value_query =  value_query || ', id_station_type';
								value_values = value_values || ', ' || one_row.id_station_type;
				            END IF;

				            IF one_row.comment IS NOT NULL THEN
								value_query =  value_query || ', comment';
								value_values = value_values || ', ' || quote_literal(one_row.comment);
				            END IF;

				            IF one_row.id_location IS NOT NULL THEN
								value_query =  value_query || ', id_location';
								value_values = value_values || ', ' || one_row.id_location;
				            END IF;

				            IF one_row.id_monument IS NOT NULL THEN
								value_query =  value_query || ', id_monument';
								value_values = value_values || ', ' || quote_literal(one_row.id_monument);
				            END IF;

				            IF one_row.id_geological IS NOT NULL THEN
								value_query =  value_query || ', id_geological';
								value_values = value_values || ', ' || quote_literal(one_row.id_geological);
				            END IF;

				            IF one_row.iers_domes IS NOT NULL THEN
								value_query =  value_query || ', iers_domes';
								value_values = value_values || ', ' || quote_literal(one_row.iers_domes);
				            END IF;

				            IF one_row.cpd_num IS NOT NULL THEN
								value_query =  value_query || ', cpd_num';
								value_values = value_values || ', ' || quote_literal(one_row.cpd_num);
				            END IF;

				            IF one_row.monument_num IS NOT NULL THEN
								value_query =  value_query || ', monument_num';
								value_values = value_values || ', ' || one_row.monument_num;
				            END IF;

				            IF one_row.receiver_num IS NOT NULL THEN
								value_query =  value_query || ', receiver_num';
								value_values = value_values || ', ' || one_row.receiver_num;
				            END IF;

				            IF one_row.country_code IS NOT NULL THEN
								value_query =  value_query || ', country_code';
								value_values = value_values || ', ' || quote_literal(one_row.country_code);
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);
                            raise notice 'Station mapped to destiny node id : %', NEW.destiny;
					    END LOOP;
			    	-- STATION TABLE END --

			    	-- STATIONS BEGIN --
		    			-- station_colocation
		    			FOR one_row IN
				        	SELECT id, id_station, id_station_colocated FROM station_colocation WHERE id_station = NEW.station
					    LOOP

					    	value_query = 'INSERT INTO station_colocation (id, id_station, id_station_colocated) VALUES (' || one_row.id || ', ' || one_row.id_station || ', ' || one_row.id_station_colocated || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;

					    -- local_ties
					    FOR one_row IN 
					        SELECT id, id_station, name, usage, cpd_num, iers_domes, dx, dy, dz, accuracy, survey_method, date_at, comment FROM local_ties WHERE id_station = NEW.station
					    LOOP 
					    	value_query = 'INSERT INTO local_ties (id, id_station, name';
					    	value_values = 'VALUES (' || one_row.id || ', ' || one_row.id_station || ', ' || quote_literal(one_row.name);

					    	IF one_row.usage IS NOT NULL THEN
								value_query =  value_query || ', usage';
								value_values = value_values || ', ' || quote_literal(one_row.usage);
				            END IF;

					    	IF one_row.cpd_num IS NOT NULL THEN
								value_query =  value_query || ', cpd_num';
								value_values = value_values || ', ' || quote_literal(one_row.cpd_num);
				            END IF;

				            IF one_row.iers_domes IS NOT NULL THEN
								value_query =  value_query || ', iers_domes';
								value_values = value_values || ', ' || quote_literal(one_row.iers_domes);
				            END IF;

				            IF one_row.dx IS NOT NULL THEN
								value_query =  value_query || ', dx';
								value_values = value_values || ', ' || one_row.dx;
				            END IF;

				            IF one_row.dy IS NOT NULL THEN
								value_query =  value_query || ', dy';
								value_values = value_values || ', ' || one_row.dy;
				            END IF;

				            IF one_row.dz IS NOT NULL THEN
								value_query =  value_query || ', dz';
								value_values = value_values || ', ' || one_row.dz;
				            END IF;

				            IF one_row.accuracy IS NOT NULL THEN
								value_query =  value_query || ', accuracy';
								value_values = value_values || ', ' || one_row.accuracy;
				            END IF;

				            IF one_row.survey_method IS NOT NULL THEN
								value_query =  value_query || ', survey_method';
								value_values = value_values || ', ' || quote_literal(one_row.survey_method);
				            END IF;

				            IF one_row.date_at IS NOT NULL THEN
								value_query =  value_query || ', date_at';
								value_values = value_values || ', ' || quote_literal(one_row.date_at);
				            END IF;

				            IF one_row.comment IS NOT NULL THEN
								value_query =  value_query || ', comment';
								value_values = value_values || ', ' || quote_literal(one_row.comment);
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;

					    -- instrument_collocation
					    FOR one_row IN 
					        SELECT id, id_station, type, status, date_from, date_to, comment FROM instrument_collocation WHERE id_station = NEW.station
					    LOOP 
					    	value_query = 'INSERT INTO instrument_collocation (id, id_station, type';
					    	value_values = 'VALUES (' || one_row.id || ', ' || one_row.id_station || ', ' || quote_literal(one_row.type);

					    	IF one_row.status IS NOT NULL THEN
								value_query =  value_query || ', status';
								value_values = value_values || ', ' || quote_literal(one_row.status);
				            END IF;

				            IF one_row.date_from IS NOT NULL THEN
								value_query =  value_query || ', date_from';
								value_values = value_values || ', ' || quote_literal(one_row.date_from);
				            END IF;

				            IF one_row.date_to IS NOT NULL THEN
								value_query =  value_query || ', date_to';
								value_values = value_values || ', ' || quote_literal(one_row.date_to);
				            END IF;

				            IF one_row.comment IS NOT NULL THEN
								value_query =  value_query || ', comment';
								value_values = value_values || ', ' || quote_literal(one_row.comment);
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;
			    	-- STATIONS END --

			    	-- LOCAL CONDITIONS AND EFFECTS BEGIN --
			    		-- condition
						FOR one_row IN 
					        SELECT id, id_station, id_effect, date_from, date_to, degradation, comments FROM condition WHERE id_station = NEW.station
					    LOOP 
					    	value_query = 'INSERT INTO condition (id, id_station, id_effect';
					    	value_values = 'VALUES (' || one_row.id || ', ' || one_row.id_station || ', ' || one_row.id_effect;

				            IF one_row.date_from IS NOT NULL THEN
								value_query =  value_query || ', date_from';
								value_values = value_values || ', ' || quote_literal(one_row.date_from);
				            END IF;

				            IF one_row.date_to IS NOT NULL THEN
								value_query =  value_query || ', date_to';
								value_values = value_values || ', ' || quote_literal(one_row.date_to);
				            END IF;

							IF one_row.degradation IS NOT NULL THEN
								value_query =  value_query || ', degradation';
								value_values = value_values || ', ' || quote_literal(one_row.degradation);
				            END IF;

				            IF one_row.comments IS NOT NULL THEN
								value_query =  value_query || ', comments';
								value_values = value_values || ', ' || quote_literal(one_row.comments);
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;
			    	-- LOCAL CONDITIONS AND EFFECTS END --

			    	-- ACCESS CONTROL BEGIN --
			    		-- user_group_station
		    			FOR one_row IN 
				            -- SELECT id, id_user_group, id_station FROM user_group_station WHERE id_stations = NEW.station
					        -- BUG fixes
					        SELECT id, id_user_group, id_station FROM user_group_station WHERE id_station = NEW.station
					    LOOP

					    	value_query = 'INSERT INTO user_group_station (id, id_user_group, id_station) VALUES (' || one_row.id || ', ' || one_row.id_user_group || ', ' || one_row.id_station || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;
			    	-- ACCESS CONTROL END --

			    	-- SITE LOGS BEGIN --
			    		-- log
		    			FOR one_row IN 
				        	SELECT id, title, date, id_log_type, id_station, modified, previous, id_contact FROM log WHERE id_station = NEW.station
					    LOOP 

					    	value_query = 'INSERT INTO log (id, title, id_log_type, id_station';
					    	value_values = 'VALUES (' || one_row.id || ', ' || quote_literal(one_row.title) || ', ' || one_row.id_log_type || ', ' || one_row.id_station;

				            IF one_row.date IS NOT NULL THEN
								value_query =  value_query || ', date';
								value_values = value_values || ', ' || quote_literal(one_row.date);
				            END IF;

				            IF one_row.modified IS NOT NULL THEN
								value_query =  value_query || ', modified';
								value_values = value_values || ', ' || quote_literal(one_row.modified);
				            END IF;

							IF one_row.previous IS NOT NULL THEN
								value_query =  value_query || ', previous';
								value_values = value_values || ', ' || quote_literal(one_row.previous);
				            END IF;

				            IF one_row.id_contact IS NOT NULL THEN
								value_query =  value_query || ', id_contact';
								value_values = value_values || ', ' || one_row.id_contact;
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;
			    	-- SITE LOGS END --

			    	-- ORGANIZATIONAL BEGIN --
			    		-- station_contact
			    		FOR one_row IN 
				        	SELECT id, id_station, id_contact, role FROM station_contact WHERE id_station = NEW.station
					    LOOP 

					    	value_query = 'INSERT INTO station_contact (id, id_station, id_contact';
					    	value_values = 'VALUES (' || one_row.id || ', ' || one_row.id_station || ', ' || one_row.id_contact;

				            IF one_row.role IS NOT NULL THEN
								value_query =  value_query || ', role';
								value_values = value_values || ', ' || quote_literal(one_row.role);
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;

					    -- station_network
					    FOR one_row IN 
				        	SELECT id, id_station, id_network FROM station_network WHERE id_station = NEW.station
					    LOOP 

					    	value_query = 'INSERT INTO station_network (id, id_station, id_network) VALUES (' || one_row.id || ', ' || one_row.id_station || ', ' || one_row.id_network || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;
			    	-- ORGANIZATIONAL END --

			    	-- DOCUMENTS BEGIN --
			    		-- document
			    		FOR one_row IN 
				        	SELECT id, date, title, description, link, id_station, id_item, id_document_type FROM document WHERE id_station = NEW.station
					    LOOP 

					    	value_query = 'INSERT INTO document (id, date, title, link, id_document_type, id_station';
					    	value_values = 'VALUES (' || one_row.id || ', ' || quote_literal(one_row.date) || ', ' || quote_literal(one_row.title) || ', ' || quote_literal(one_row.link) || ', ' || one_row.id_document_type || ', ' || one_row.id_station;

				            IF one_row.description IS NOT NULL THEN
								value_query =  value_query || ', description';
								value_values = value_values || ', ' || quote_literal(one_row.description);
				            END IF;

				            IF one_row.id_item IS NOT NULL THEN
								value_query =  value_query || ', id_item';
								value_values = value_values || ', ' || one_row.id_item;
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;
			    	-- DOCUMENTS END --

			    	-- ITEMS BEGIN --
			    		-- station_item
			    		FOR one_row IN 
				        	SELECT id, id_station, id_item, date_from, date_to FROM station_item WHERE id_station = NEW.station
					    LOOP 

					    	value_query = 'INSERT INTO station_item (id, id_station, id_item, date_from';
					    	value_values = 'VALUES (' || one_row.id || ', ' || one_row.id_station || ', ' || one_row.id_item || ', ' || quote_literal(one_row.date_from);

				            IF one_row.date_to IS NOT NULL THEN
								value_query =  value_query || ', date_to';
								value_values = value_values || ', ' || quote_literal(one_row.date_to);
				            END IF;

				            value_query = value_query || ') ' || value_values || ');';

				            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

					    END LOOP;
			    	-- ITEMS END --



				END IF; -- END IF NOT EXISTS STATION IN QUERIES --

                -----------------------------------------------------
				-- AUTO CREATE T2 CONNECTIONS ON THE NODE SIDE BASED ON THE T1 CONNECTIONS OF THE DGW
				-----------------------------------------------------
                -- After adding a new T1 station-connection a T2 station-connection should be established in the remote node

                -- Handle CREATE CONNECTIONS
                IF (TG_OP = 'INSERT') THEN
                    -- Add a new T2 connection between the local node and the remote one
                    value_query = 'DO ' || chr(36) || '' || chr(36) || ' BEGIN';
				    -- Only proceed if the newly sync station is available on the remote node (i.e., successfully synchronized)
                    value_query = value_query || ' IF EXISTS (SELECT * FROM station WHERE id='|| NEW.station ||') THEN';
				    value_query = value_query || ' IF NOT EXISTS(SELECT * FROM connections WHERE station=' || NEW.station ||') THEN';
				    value_query = value_query || ' INSERT INTO connections (source, destiny, station, metadata) VALUES (1, (SELECT id FROM node where id!=1 LIMIT 1), ' || NEW.station || ', ''T2'');';
                    value_query = value_query || ' END IF; END IF; END; ' || chr(36) || '' || chr(36) || ';';
                    -- raise notice 'INSERT: %', value_query;
                    INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);

                    -- A generic connection between the current node and the DGW should also be established
                    value_query = 'DO ' || chr(36) || '' || chr(36) || ' BEGIN';
				    -- Only proceed if there is at least one station successfully sync to the remote node
                    value_query = value_query || ' IF EXISTS (SELECT * FROM station) THEN';
				    value_query = value_query || ' IF NOT EXISTS(SELECT * FROM connections WHERE metadata=''T2'' AND station IS NULL AND destiny IS NOT NULL) THEN';
				    value_query = value_query || ' INSERT INTO connections (source, destiny, station, metadata) VALUES (1, (SELECT id FROM node where id!=1 LIMIT 1), NULL, ''T2'');';
                    value_query = value_query || ' END IF; END IF; END; ' || chr(36) || '' || chr(36) || ';';
                    -- raise notice 'INSERT: %', value_query;
                    INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);
                END IF;

                -- Handle UPDATE CONNECTIONS
                IF (TG_OP = 'UPDATE') THEN
                    value_query = 'DO ' || chr(36) || '' || chr(36) || ' BEGIN';
                    value_query = value_query || ' IF EXISTS (SELECT * FROM station WHERE id='|| NEW.station ||') THEN';
				    value_query = value_query || ' IF EXISTS (SELECT * FROM station WHERE id='|| OLD.station ||') THEN';
				    value_query = value_query || ' UPDATE connections SET station =' || NEW.station ||' WHERE station =' || OLD.station || ';';
                    value_query = value_query || ' END IF; END IF; END; ' || chr(36) || '' || chr(36) || ';';
                    -- raise notice 'UPDATE: %', value_query;
                    INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NEW.station, NEW.destiny);
                END IF;

                RETURN NULL;
		    END IF; -- END IF STATION IS NOT NULL AND METADATA = T1

	        -- Handle not null values and metadata != T1
		    IF (NEW.station IS NOT NULL OR NEW.metadata != 'T1') THEN
		        RETURN NULL;
		    END IF;
        
        	-- ORGANIZATIONAL BEGIN --
        		-- agency
		        FOR one_row IN 
			        SELECT id, name, abbreviation, address, www, infos FROM agency
			    LOOP  
				   	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE agency IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO agency (id, name';

					value_values = ' SELECT ' || one_row.id || ', ' || quote_literal(one_row.name);

					IF one_row.abbreviation IS NOT NULL THEN
						value_query =  value_query || ', abbreviation';
						value_values = value_values || ', ' || quote_literal(one_row.abbreviation);
		            END IF;

		            IF one_row.address IS NOT NULL THEN
						value_query =  value_query || ', address';
						value_values = value_values || ', ' || quote_literal(one_row.address);
		            END IF;

		            IF one_row.www IS NOT NULL THEN
						value_query =  value_query || ', www';
						value_values = value_values || ', ' || quote_literal(one_row.www);
		            END IF;

		            IF one_row.infos IS NOT NULL THEN
						value_query =  value_query || ', infos';
						value_values = value_values || ', ' || quote_literal(one_row.infos);
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '	SELECT * FROM agency WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';
		
		            -- Insert data to queries
		            INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
	
				-- contact
		        FOR one_row IN 
			        SELECT id, name, title, email, phone, gsm, comment, id_agency, role FROM contact
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE contact IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO contact (id, name, id_agency';

					value_values = ' SELECT ' || one_row.id || ', ' || quote_literal(one_row.name) || ', ' || one_row.id_agency;

					IF one_row.title IS NOT NULL THEN
						value_query =  value_query || ', title';
						value_values = value_values || ', ' || quote_literal(one_row.title);
		            END IF;

					IF one_row.email IS NOT NULL THEN
						value_query =  value_query || ', email';
						value_values = value_values || ', ' || quote_literal(one_row.email);
		            END IF;

		            IF one_row.phone IS NOT NULL THEN
						value_query =  value_query || ', phone';
						value_values = value_values || ', ' || quote_literal(one_row.phone);
		            END IF;

					IF one_row.gsm IS NOT NULL THEN
						value_query =  value_query || ', gsm';
						value_values = value_values || ', ' || quote_literal(one_row.gsm);
		            END IF;

					IF one_row.comment IS NOT NULL THEN
						value_query =  value_query || ', comment';
						value_values = value_values || ', ' || quote_literal(one_row.comment);
		            END IF;

		            IF one_row.role IS NOT NULL THEN
						value_query =  value_query || ', role';
						value_values = value_values || ', ' || quote_literal(one_row.role);
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '	SELECT * FROM contact WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
	        		
				-- network
		        FOR one_row IN 
			        SELECT id, name, id_contact FROM network
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE network IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO network (id, name';

					value_values = ' SELECT ' || one_row.id || ', ' || quote_literal(one_row.name);

					IF one_row.id_contact IS NOT NULL THEN
						value_query =  value_query || ', id_contact';
						value_values = value_values || ', ' || one_row.id_contact;
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '	SELECT * FROM network WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
			-- ORGANIZATIONAL END --

			-- ITEMS BEGIN --
	
				-- attribute
		        FOR one_row IN 
			        SELECT id, name FROM attribute
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE attribute IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO attribute (id, name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM attribute WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';
			        
			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- item_type
		        FOR one_row IN 
			        SELECT id, name FROM item_type
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE item_type IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO item_type (id, name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM item_type WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- item_type_attribute
		        FOR one_row IN 
			        SELECT id, id_item_type, id_attribute FROM item_type_attribute
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE item_type_attribute IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO item_type_attribute (id, id_item_type, id_attribute) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.id_item_type) || ', ' || quote_literal(one_row.id_attribute) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM item_type_attribute WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- item
		        FOR one_row IN 
			        SELECT id, id_item_type, id_contact_as_producer, id_contact_as_owner, comment FROM item
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE item IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO item (id, id_item_type';

					value_values = ' SELECT ' || one_row.id || ', ' || one_row.id_item_type;

					IF one_row.id_contact_as_producer IS NOT NULL THEN
						value_query =  value_query || ', id_contact_as_producer';
						value_values = value_values || ', ' || one_row.id_contact_as_producer;
		            END IF;

		            IF one_row.id_contact_as_owner IS NOT NULL THEN
						value_query =  value_query || ', id_contact_as_owner';
						value_values = value_values || ', ' || one_row.id_contact_as_owner;
		            END IF;

		            IF one_row.comment IS NOT NULL THEN
						value_query =  value_query || ', comment';
						value_values = value_values || ', ' || quote_literal(one_row.comment);
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM item WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- item_attribute
		        FOR one_row IN 
			        SELECT id, id_item, id_attribute, date_from, date_to, value_varchar, value_date, value_numeric FROM item_attribute
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE item_attribute IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO item_attribute (id, id_item, id_attribute';

					value_values = ' SELECT ' || one_row.id || ', ' || one_row.id_item || ', ' || one_row.id_attribute;

					IF one_row.date_from IS NOT NULL THEN
						value_query =  value_query || ', date_from';
						value_values = value_values || ', ' || quote_literal(one_row.date_from);
		            END IF;

		            IF one_row.date_to IS NOT NULL THEN
						value_query =  value_query || ', date_to';
						value_values = value_values || ', ' || quote_literal(one_row.date_to);
		            END IF;

		            IF one_row.value_varchar IS NOT NULL THEN
						value_query =  value_query || ', value_varchar';
						value_values = value_values || ', ' || quote_literal(one_row.value_varchar);
		            END IF;

		            IF one_row.value_date IS NOT NULL THEN
						value_query =  value_query || ', value_date';
						value_values = value_values || ', ' || quote_literal(one_row.value_date);
		            END IF;

		            IF one_row.value_numeric IS NOT NULL THEN
						value_query =  value_query || ', value_numeric';
						value_values = value_values || ', ' || one_row.value_numeric;
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM item_attribute WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
			
			-- SITE LOGS BEGIN --
				-- log_type
		        FOR one_row IN 
			        SELECT id, name FROM log_type
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE log_type IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO log_type (id, name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM log_type WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
			-- SITE LOGS END --
			
			-- DOCUMENTS BEGIN --
				-- document_type
		        FOR one_row IN 
			        SELECT id, name FROM document_type
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE document_type IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO document_type (id, name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM document_type WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
			-- DOCUMENTS END --
			
			-- ACCESS CONTROL BEGIN --
				-- user_group
		        FOR one_row IN 
			        SELECT id, name FROM user_group
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE user_group IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO user_group (id, name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM user_group WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
			-- ACCESS CONTROL END --
			
			-- STATIONS BEGIN --			
				-- bedrock
		        FOR one_row IN 
			        SELECT id, condition, type FROM bedrock
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE bedrock IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO bedrock (id, condition';

					value_values = ' SELECT ' || one_row.id || ', ' || quote_literal(one_row.condition);

					IF one_row.type IS NOT NULL THEN
						value_query =  value_query || ', type';
						value_values = value_values || ', ' || quote_literal(one_row.type);
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM bedrock WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- geological
		        FOR one_row IN 
			        SELECT id, id_bedrock, characteristic, fracture_spacing, fault_zone, distance_to_fault FROM geological
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE geological IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO geological (id, characteristic ';

					value_values = ' SELECT ' || one_row.id || ', ' || quote_literal(one_row.characteristic);

					IF one_row.id_bedrock IS NOT NULL THEN
						value_query =  value_query || ', id_bedrock';
						value_values = value_values || ', ' || one_row.id_bedrock;
		            END IF;

		            IF one_row.fracture_spacing IS NOT NULL THEN
						value_query =  value_query || ', fracture_spacing';
						value_values = value_values || ', ' || quote_literal(one_row.fracture_spacing);
		            END IF;

		            IF one_row.fault_zone IS NOT NULL THEN
						value_query =  value_query || ', fault_zone';
						value_values = value_values || ', ' || quote_literal(one_row.fault_zone);
		            END IF;

		            IF one_row.distance_to_fault IS NOT NULL THEN
						value_query =  value_query || ', distance_to_fault';
						value_values = value_values || ', ' || quote_literal(one_row.distance_to_fault);
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM geological WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- station_type
		        FOR one_row IN 
			        SELECT id, name, type FROM station_type
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE station_type IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO station_type (id, name';

					value_values = ' SELECT ' || one_row.id || ', ' || quote_literal(one_row.name);

					IF one_row.type IS NOT NULL THEN
						value_query =  value_query || ', type';
						value_values = value_values || ', ' || quote_literal(one_row.type);
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM station_type WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- monument
		        FOR one_row IN 
			        SELECT id, description, inscription, height, foundation, foundation_depth FROM monument
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE monument IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO monument (id, description';

					value_values = ' SELECT ' || one_row.id || ', ' || quote_literal(one_row.description);

					IF one_row.inscription IS NOT NULL THEN
						value_query =  value_query || ', inscription';
						value_values = value_values || ', ' || quote_literal(one_row.inscription);
		            END IF;

		            IF one_row.height IS NOT NULL THEN
						value_query =  value_query || ', height';
						value_values = value_values || ', ' || one_row.height;
		            END IF;

					IF one_row.foundation IS NOT NULL THEN
						value_query =  value_query || ', foundation';
						value_values = value_values || ', ' || quote_literal(one_row.foundation);
		            END IF;

		            IF one_row.foundation_depth IS NOT NULL THEN
						value_query =  value_query || ', foundation_depth';
						value_values = value_values || ', ' || one_row.foundation_depth;
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM monument WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- coordinates
		        FOR one_row IN 
			        SELECT id, x, y, z, lat, lon, altitude FROM coordinates
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE coordinates IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO coordinates (id, x, y, z';
					
					value_values = ' SELECT ' || one_row.id || ', ' || one_row.x || ', ' || one_row.y || ', ' || one_row.z;

					IF one_row.lat IS NOT NULL THEN
						value_query =  value_query || ', lat';
						value_values = value_values || ', ' || one_row.lat;
		            END IF;

					IF one_row.lon IS NOT NULL THEN
						value_query =  value_query || ', lon';
						value_values = value_values || ', ' || one_row.lon;
		            END IF;

					IF one_row.altitude IS NOT NULL THEN
						value_query =  value_query || ', altitude';
						value_values = value_values || ', ' || one_row.altitude;
		            END IF;		            

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM coordinates WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- tectonic
		        FOR one_row IN 
			        SELECT id, plate_name FROM tectonic
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE tectonic IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO tectonic (id, plate_name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.plate_name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM tectonic WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- country
		        FOR one_row IN 
			        SELECT id, name, iso_code FROM country
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE country IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO country (id, name, iso_code) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.name) || ', ' || quote_literal(one_row.iso_code) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM country WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- state
		        FOR one_row IN 
			        SELECT id, id_country, name FROM state
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE state IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO state (id, id_country, name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || one_row.id_country || ', ' || quote_literal(one_row.name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM state WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- city
		        FOR one_row IN 
			        SELECT id, id_state, name FROM city
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE city IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO city (id, id_state, name) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || one_row.id_state || ', ' || quote_literal(one_row.name) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM city WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;

				-- location
		        FOR one_row IN 
			        SELECT id, id_city, id_coordinates, id_tectonic, description FROM location
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE location IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO location ( id, id_city, id_coordinates';

					value_values = ' SELECT ' || one_row.id || ', ' || one_row.id_city || ', ' || one_row.id_coordinates;

					IF one_row.id_tectonic IS NOT NULL THEN
						value_query =  value_query || ', id_tectonic';
						value_values = value_values || ', ' || one_row.id_tectonic;
		            END IF;

					IF one_row.description IS NOT NULL THEN
						value_query =  value_query || ', description';
						value_values = value_values || ', ' || quote_literal(one_row.description);
		            END IF;

					value_query = value_query || ') ' || value_values || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM location WHERE id = ' || one_row.id || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
			-- STATIONS END --
			
			-- LOCAL CONDITIONS AND EFFECTS BEGIN --
				-- effects
		        FOR one_row IN 
			        SELECT id, type FROM effects
			    LOOP
			    	value_query = 'BEGIN;';
					value_query = value_query || 'LOCK TABLE effects IN SHARE ROW EXCLUSIVE MODE;';
					value_query = value_query || 'INSERT INTO effects (id, type) ';
					value_query = value_query || '	SELECT ' || one_row.id || ', ' || quote_literal(one_row.type) || ' ';
					value_query = value_query || '	WHERE NOT EXISTS (';
					value_query = value_query || '		SELECT * FROM effects WHERE id = ' || quote_literal(one_row.id) || ' '; 
					value_query = value_query || '	);';
					value_query = value_query || 'COMMIT;';

			        INSERT INTO queries (query, metadata) VALUES (value_query, 'T1');
			    END LOOP;
			-- LOCAL CONDITIONS AND EFFECTS END --

           	RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            -- If the connection is removed, also remove the T2 connection on the remote node;
            IF OLD.station IS NOT NULL THEN
                -- After removing the connection, you should also delete the station from the DGW
                -- The removal of the station, in the remote node, should not be done automatically
                value_query = 'DELETE FROM connections WHERE station =' || OLD.station;
                INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, 'T1', NULL, OLD.destiny);
            END IF;
            RETURN NULL;
        END IF;

    END;
$$;

DROP TRIGGER IF EXISTS sync_connections_t1 on connections;
CREATE TRIGGER sync_connections_t1 AFTER INSERT OR UPDATE OR DELETE ON connections
    FOR EACH ROW EXECUTE PROCEDURE sync_connections_t1();