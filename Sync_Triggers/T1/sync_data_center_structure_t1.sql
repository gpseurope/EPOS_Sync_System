CREATE OR REPLACE FUNCTION sync_data_center_structure_t1() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
    value_query text;
    value_metadata text;
    value_stationID integer;
    value_first boolean;

    id_data_center_structure integer;
    id_data_center_ integer;
    id_file_type integer;
    directory_naming text;
    comments text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_data_center_structure = NEW.id;
            id_data_center_ = NEW.id_data_center;
            id_file_type = NEW.id_file_type;
            directory_naming = NEW.directory_naming;
            comments = NEW.comments;

            -- Create query
            value_query = 'INSERT INTO data_center_structure (id, id_data_center, id_file_type, directory_naming';

            IF comments IS NOT NULL THEN
                value_query = value_query || ', comments';
            END IF;

            value_query = value_query || ') VALUES (' || id_data_center_structure || ', ' || id_data_center_ || ', ' || id_file_type|| ', ' || quote_literal(directory_naming);

            IF comments IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(comments);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, value_metadata, value_stationID,  (SELECT id_node FROM node_data_center WHERE id_data_center=id_data_center_));

            -- Update the previous queries related to the insertion/update/delete of a new data center
            UPDATE queries SET destiny = (SELECT id_node FROM node_data_center WHERE id_data_center=id_data_center_) WHERE query ILIKE '% data_center % VALUES (' || id_data_center_ || '%';

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_data_center_structure = OLD.id;
            id_data_center_ = NEW.id_data_center;
            id_file_type = NEW.id_file_type;
            directory_naming = NEW.directory_naming;
            comments = NEW.comments;

            -- Create query
            -- Added possibility to update id
            value_query = 'UPDATE data_center_structure SET id=' || NEW.id || ', id_data_center=' || id_data_center_ || ', id_file_type=' || id_file_type || ', directory_naming=' || quote_literal(directory_naming);

            -- Handle NULL values
            IF NEW.comments IS NOT NULL THEN
                value_query = value_query || ', comments=' || quote_literal(comments);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_data_center_structure || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id, destiny) VALUES (value_query, value_metadata, value_stationID,  (SELECT id_node FROM node_data_center WHERE id_data_center=id_data_center_));

            -- Update the previous queries related to the insertion/update/delete of a new data center
            UPDATE queries SET destiny = (SELECT id_node FROM node_data_center WHERE id_data_center=id_data_center_) WHERE query ILIKE '% data_center % VALUES (' || id_data_center_ || '%';


            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_data_center_structure = OLD.id;

            -- Create query
            value_query = 'DELETE FROM data_center_structure WHERE id=' || id_data_center_structure || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$;

DROP TRIGGER IF EXISTS sync_data_center_structure_t1 on data_center_structure;
CREATE TRIGGER sync_data_center_structure_t1 AFTER INSERT OR UPDATE OR DELETE ON data_center_structure
    FOR EACH ROW EXECUTE PROCEDURE sync_data_center_structure_t1();


