create or replace function sync_attribute_t1() returns trigger
language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_attribute integer;
	    name text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_attribute = NEW.id;
            name = NEW.name;

            -- Create query
            value_query = 'INSERT INTO attribute (id, name) VALUES ' ||
                '(' || id_attribute || ', ' || quote_literal(name) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_attribute = OLD.id;
			name = NEW.name;
			value_first = true;

			-- Create query
			-- Added possibility to update id
            value_query = 'UPDATE attribute SET id=' || NEW.id || ',';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
            END IF;

             -- Create query
            value_query = value_query || ' WHERE id=' || id_attribute || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_attribute = OLD.id;

            -- Create query
            value_query = 'DELETE FROM attribute WHERE id=' || id_attribute || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_attribute_t1 on attribute;
CREATE TRIGGER sync_attribute_t1 AFTER INSERT OR UPDATE OR DELETE ON attribute
    FOR EACH ROW EXECUTE PROCEDURE sync_attribute_t1();

    