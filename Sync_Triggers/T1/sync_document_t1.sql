create or replace function sync_document_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_document integer;
	    date_ date;
	    title text;
	    description text;
	    link text;
	    id_station integer;
	    id_item integer;
	    id_document_type integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_document = NEW.id;
            date_ = NEW.date;
            title = NEW.title;
            description = NEW.description;
            link = NEW.link;
            id_station = NEW.id_station;
            id_item = NEW.id_item;
            id_document_type = NEW.id_document_type;

            -- Create query
            value_query = 'INSERT INTO document (id, date, title, link, id_station, id_item, id_document_type';

			IF description IS NOT NULL THEN
                value_query = value_query || ', description';
            END IF;

			 value_query = value_query || ') VALUES (' || id_document || ', ' || quote_literal(date_) || ', ' || quote_literal(title) || ', ' ||
				quote_literal(link) || ', ' || id_station || ', ' || id_item || ', ' || id_document_type;

			IF description IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(description);
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_document = OLD.id;
			      date_ = NEW.date;
            title = NEW.title;
            description = NEW.description;
            link = NEW.link;
            id_station = NEW.id_station;
            id_item = NEW.id_item;
            id_document_type = NEW.id_document_type;
			      value_first = true;

      --Create query
           value_query = 'UPDATE document SET ';

			-- Handle NULL values
            IF NEW.date IS NOT NULL THEN
                value_query = value_query || 'date=' || quote_literal(date_);
                value_first = false;
            END IF;

			-- Handle NULL values
            IF NEW.title IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'title =' || quote_literal(title);
            END IF;

			-- Handle NULL values
            IF NEW.description IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'description =' || quote_literal(description);
            END IF;

			-- Handle NULL values
            IF NEW.link IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'link =' || quote_literal(link);
            END IF;

			-- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_station =' || id_station;
            END IF;

			-- Handle NULL values
            IF NEW.id_item IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_item =' || id_item;
            END IF;

			-- Handle NULL values
            IF NEW.id_document_type IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_document_type =' || id_document_type;
            END IF;

			 -- Create query
            value_query = value_query || ' WHERE id=' || id_document || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_document = OLD.id;

            -- Create query
            value_query = 'DELETE FROM document WHERE id=' || id_document || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = OLD.id_station;

            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_document_t1 on document;
CREATE TRIGGER sync_document_t1 AFTER INSERT OR UPDATE OR DELETE ON document
    FOR EACH ROW EXECUTE PROCEDURE sync_document_t1();


