create or replace function sync_file_type_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_file_type integer;
	    format text;
      sampling_window text;
	    sampling_frequency text;

    BEGIN
      -- Handle insert
      IF (TG_OP = 'INSERT') THEN
          id_file_type = NEW.id;
          format = NEW.format;
          sampling_window = new.sampling_window;
          sampling_frequency = new.sampling_frequency;

			-- Create query
      value_query = 'INSERT INTO file_type (id, format';

			IF sampling_window IS NOT NULL THEN
        value_query = value_query || ', sampling_window';
      END IF;

      IF sampling_frequency IS NOT NULL THEN
        value_query = value_query || ', sampling_frequency';
      END IF;

			value_query = value_query || ') VALUES (' || id_file_type || ', ' || quote_literal(format);

			IF sampling_window  IS NOT NULL THEN
        value_query = value_query || ', ' || quote_literal(sampling_window);
      END IF;

      IF sampling_frequency IS NOT NULL THEN
        value_query = value_query || ', ' || quote_literal(sampling_frequency);
      END IF;

			 value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

      -- Handle update
      IF (TG_OP = 'UPDATE') THEN
            id_file_type = OLD.id;
            format = NEW.format;
            sampling_window = NEW.sampling_window;
            sampling_frequency = NEW.sampling_frequency;
			      value_first = true;

			-- Create query
            value_query = 'UPDATE file_type SET ';

            -- bug fixes, all set columns were format ?!

            -- Handle NULL values
            IF NEW.id IS NOT NULL THEN
                value_query = value_query || 'id =' || NEW.id;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.format IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'format =' || quote_literal(format);
            END IF;

            IF sampling_window IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'sampling_window =' || quote_literal(sampling_window);
            END IF;

            IF sampling_frequency IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'sampling_frequency =' || quote_literal(sampling_frequency);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_file_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

      -- Handle delete
      IF (TG_OP = 'DELETE') THEN
            id_file_type = OLD.id;

            -- Create query
            value_query = 'DELETE FROM file_type WHERE id=' || id_file_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_file_type_t1 on file_type;
CREATE TRIGGER sync_file_type_t1 AFTER INSERT OR UPDATE OR DELETE ON file_type
    FOR EACH ROW EXECUTE PROCEDURE sync_file_type_t1();

