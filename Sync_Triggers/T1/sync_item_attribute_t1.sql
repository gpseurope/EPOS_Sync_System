create or replace function sync_item_attribute_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
	    value_metadata text;
        value_stationID integer;
		value_first boolean;

        id_item_attribute integer;
	    id_item integer;
	    id_attribute integer;
	    date_from timestamp;
	    date_to timestamp;
	    value_varchar text;
	    value_date date;
	    value_numeric numeric;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_item_attribute = NEW.id;
            id_item = NEW.id_item;
            id_attribute = NEW.id_attribute;
            date_from = NEW.date_from;
            date_to = NEW.date_to;
            value_varchar = NEW.value_varchar;
            value_date = NEW.value_date;
            value_numeric = NEW.value_numeric;

			-- Create query
            value_query = 'INSERT INTO item_attribute (id, id_item, id_attribute';

            IF date_from IS NOT NULL THEN
                value_query = value_query || ', date_from';
            END IF;

			IF date_to IS NOT NULL THEN
                value_query = value_query || ', date_to';
            END IF;

			IF value_varchar IS NOT NULL THEN
                value_query = value_query || ', value_varchar';
            END IF;

			IF value_date IS NOT NULL THEN
                value_query = value_query || ', value_date';
            END IF;

			IF value_numeric IS NOT NULL THEN
                value_query = value_query || ', value_numeric';
            END IF;

			value_query = value_query || ') VALUES (' || id_item_attribute || ', ' || id_item || ', ' || id_attribute;

			IF date_from IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_from);
            END IF;

			IF date_to IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_to);
            END IF;

			IF value_varchar IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(value_varchar);
            END IF;

			IF value_date IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(value_date);
            END IF;

			IF value_numeric IS NOT NULL THEN
                value_query = value_query || ', ' || value_numeric;
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_item_attribute = OLD.id;
            id_item = OLD.id_item;
            id_attribute = OLD.id_attribute;
            date_from = NEW.date_from;
            date_to = NEW.date_to;
            value_varchar = NEW.value_varchar;
            value_date = NEW.value_date;
            value_numeric = NEW.value_numeric;
			      value_first = true;

			-- Create query
            value_query = 'UPDATE item_attribute SET ';

			-- Handle NULL values
            IF NEW.date_from IS NOT NULL THEN
                value_query = value_query || 'date_from=' || quote_literal(date_from);
                value_first = false;
            END IF;

			-- Handle NULL values
			-- Added possibility to edit id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

			-- Handle NULL values
			-- Added id_item that was missing
            IF NEW.id_item IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_item=' || NEW.id_item;
            END IF;

			-- Handle NULL values
			-- Added id_attribute that was missing
            IF NEW.id_attribute IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_attribute=' || NEW.id_attribute;
            END IF;

			-- Handle NULL values
            IF NEW.date_to IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_to =' || quote_literal(date_to);
            END IF;

			-- Handle NULL values
            IF NEW.value_varchar IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'value_varchar =' || quote_literal(value_varchar);
            END IF;

			-- Handle NULL values
            IF NEW.value_date IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'value_date =' || quote_literal(value_date);
            END IF;

			-- Handle NULL values
            IF NEW.value_numeric IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'value_numeric =' || value_numeric;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_item_attribute || ' AND id_item=' || id_item || ' AND id_attribute=' || id_attribute || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            -- id_item = OLD.id_item;
            -- id_attribute = OLD.id_attribute;

            -- Create query
            --- value_query = 'DELETE FROM item_attribute WHERE id=' || id_item_attribute || ' AND id_item=' || id_item || ' AND id_attribute=' || id_attribute || ';';

            value_query = 'DELETE FROM item_attribute WHERE id=' || OLD.id ||  ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_item_attribute_t1 on item_attribute;
CREATE TRIGGER sync_item_attribute_t1 AFTER INSERT OR UPDATE OR DELETE ON item_attribute
    FOR EACH ROW EXECUTE PROCEDURE sync_item_attribute_t1();


