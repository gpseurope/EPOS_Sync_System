create or replace function sync_local_ties_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_local_ties integer;
	    id_station integer;
	    name text;
	    usage_ text;
	    cpd_num text;
      iers_domes text;
	    dx numeric;
	    dy numeric;
	    dz numeric;
	    accuracy numeric;
	    survey_method text;
	    date_at date;
	    comment text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_local_ties = NEW.id;
            id_station = NEW.id_station;
            name = NEW.name;
            usage_ = NEW.usage;
            cpd_num = NEW.cpd_num;
            iers_domes = NEW.iers_domes;
            dx = NEW.dx;
            dy = NEW.dy;
            dz = NEW.dz;
            accuracy = NEW.accuracy;
            survey_method = NEW.survey_method;
            date_at = NEW.date_at;
            comment = NEW.comment;

            -- Create query
            value_query = 'INSERT INTO local_ties (id, id_station, name';

            IF usage_ IS NOT NULL THEN
                value_query = value_query || ', usage';
            END IF;

            IF cpd_num IS NOT NULL THEN
                value_query = value_query || ', cpd_num';
            END IF;

            IF iers_domes  IS NOT NULL THEN
                value_query = value_query || ', iers_domes';
            END IF;

            IF dx IS NOT NULL THEN
                value_query = value_query || ', dx';
            END IF;

            IF dy IS NOT NULL THEN
                value_query = value_query || ', dy';
            END IF;

            IF dz IS NOT NULL THEN
                value_query = value_query || ', dz';
            END IF;

            IF accuracy IS NOT NULL THEN
                value_query = value_query || ', accuracy';
            END IF;

            IF survey_method IS NOT NULL THEN
                value_query = value_query || ', survey_method';
            END IF;

            IF date_at IS NOT NULL THEN
                value_query = value_query || ', date_at';
            END IF;

            IF comment IS NOT NULL THEN
                value_query = value_query || ', comment';
            END IF;

            value_query = value_query || ') VALUES (' || id_local_ties || ', ' || id_station || ', ' || quote_literal(name);

            IF usage_ IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(usage_);
            END IF;

            IF cpd_num IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(cpd_num);
            END IF;

            IF iers_domes IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(iers_domes);
            END IF;

            IF dx IS NOT NULL THEN
                value_query = value_query || ', ' || dx;
            END IF;

            IF dy IS NOT NULL THEN
                value_query = value_query || ', ' || dy;
            END IF;

            IF dz IS NOT NULL THEN
                value_query = value_query || ', ' || dz;
            END IF;

            IF accuracy IS NOT NULL THEN
                value_query = value_query || ', ' || accuracy;
            END IF;

            IF survey_method IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(survey_method);
            END IF;

            IF date_at IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(date_at);
            END IF;

            IF comment IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(comment);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_local_ties = OLD.id;
            id_station = NEW.id_station;
            name = NEW.name;
            usage_ = NEW.usage;
            cpd_num = NEW.cpd_num;
            iers_domes = NEW.iers_domes;
            dx = NEW.dx;
            dy = NEW.dy;
            dz = NEW.dz;
            accuracy = NEW.accuracy;
            survey_method = NEW.survey_method;
            date_at = NEW.date_at;
            comment = NEW.comment;
            value_first = true;

            -- Create query
            value_query = 'UPDATE local_ties SET ';

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || id_station;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'name =' || quote_literal(name);
            END IF;

            -- Handle NULL values
            IF NEW.usage IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'usage =' || quote_literal(usage_);
            END IF;

            -- Handle NULL values
            IF NEW.cpd_num IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'cpd_num =' || quote_literal(cpd_num);
            END IF;

            IF NEW.iers_domes IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'iers_domes =' || quote_literal(iers_domes);
            END IF;

            -- Handle NULL values
            IF NEW.dx IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'dx =' || dx;
            END IF;

            -- Handle NULL values
            IF NEW.dy IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'dy =' || dy;
            END IF;

            -- Handle NULL values
            IF NEW.dz IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'dz =' || dz;
            END IF;

            -- Handle NULL values
            IF NEW.accuracy IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'accuracy =' || accuracy;
            END IF;

            -- Handle NULL values
            IF NEW.survey_method IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'survey_method =' || quote_literal(survey_method);
            END IF;

            -- Handle NULL values
            IF NEW.date_at IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'date_at =' || quote_literal(date_at);
            END IF;

            -- Handle NULL values
            IF NEW.comment IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'comment =' || quote_literal(comment);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_local_ties || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         -- Handle delete, we don't need to handle delete because the databases is cascade on delete
         IF (TG_OP = 'DELETE') THEN
           id_local_ties = OLD.id;
            -- Create query
            value_query = 'DELETE FROM local_ties WHERE id=' || id_local_ties || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = OLD.id_station;

            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;

            RETURN NULL;
         END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_local_ties_t1 on local_ties;
CREATE TRIGGER sync_local_ties_t1 AFTER INSERT OR UPDATE OR DELETE ON local_ties
    FOR EACH ROW EXECUTE PROCEDURE sync_local_ties_t1();

