create or replace function sync_city_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_city integer;
	    id_state integer;
	    name text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_city = NEW.id;
            id_state = NEW.id_state;
            name = NEW.name;

            -- Create query
            value_query = 'INSERT INTO city (id, id_state, name) VALUES ' ||
                '(' || id_city || ', ' || id_state || ', ' || quote_literal(name) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_city = OLD.id;
            id_state = NEW.id_state;
            name = NEW.name;
			value_first = true;

            -- Create query
            value_query = 'UPDATE city SET ';

			-- Handle NULL values
            IF NEW.id_state IS NOT NULL THEN
                value_query = value_query || 'id_state=' || id_state;
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Added possibility to update id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id =' || NEW.id;
            END IF;

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'name =' || quote_literal(name);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_city || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_city = OLD.id;

            -- Create query
            value_query = 'DELETE FROM city WHERE id=' || id_city || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_city_t1 on city;
CREATE TRIGGER sync_city_t1 AFTER INSERT OR UPDATE OR DELETE ON city
    FOR EACH ROW EXECUTE PROCEDURE sync_city_t1();
    

    