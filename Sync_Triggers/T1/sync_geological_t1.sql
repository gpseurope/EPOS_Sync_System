create or replace function sync_geological_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_geological integer;
	    id_bedrock integer;
	    characteristic text;
	    fracture_spacing text;
	    fault_zone text;
	    distance_to_fault text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_geological = NEW.id;
            id_bedrock = NEW.id_bedrock;
            characteristic = NEW.characteristic;
            fracture_spacing = NEW.fracture_spacing;
            fault_zone = NEW.fault_zone;
            distance_to_fault = NEW.distance_to_fault;

			-- Create query
            value_query = 'INSERT INTO geological (id, characteristic';

			IF id_bedrock IS NOT NULL THEN
                value_query = value_query || ', id_bedrock';
            END IF;

			IF fracture_spacing IS NOT NULL THEN
                value_query = value_query || ', fracture_spacing';
            END IF;

			IF fault_zone IS NOT NULL THEN
                value_query = value_query || ', fault_zone';
            END IF;

			IF distance_to_fault IS NOT NULL THEN
                value_query = value_query || ', distance_to_fault';
            END IF;

			value_query = value_query || ') VALUES (' || id_geological || ', ' || quote_literal(characteristic);

			IF id_bedrock IS NOT NULL THEN
                value_query = value_query || ', ' || id_bedrock;
            END IF;

			IF fracture_spacing IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(fracture_spacing);
            END IF;

			IF fault_zone IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(fault_zone);
            END IF;

			IF distance_to_fault IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(distance_to_fault);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_geological = OLD.id;
			      id_bedrock = NEW.id_bedrock;
            characteristic = NEW.characteristic;
            fracture_spacing = NEW.fracture_spacing;
            fault_zone = NEW.fault_zone;
            distance_to_fault = NEW.distance_to_fault;
			      value_first = true;

            -- Create query
            value_query = 'UPDATE geological SET ';

            -- Handle NULL values
            IF NEW.id_bedrock IS NOT NULL THEN
                value_query = value_query || 'id_bedrock=' || id_bedrock;
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Added possibility to update ID
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

            -- Handle NULL values
            IF NEW.characteristic IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'characteristic =' || quote_literal(characteristic);
            END IF;

			-- Handle NULL values
            IF NEW.fracture_spacing IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'fracture_spacing =' || quote_literal(fracture_spacing);
            END IF;

			-- Handle NULL values
            IF NEW.fault_zone IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'fault_zone =' || quote_literal(fault_zone);
            END IF;

			-- Handle NULL values
            IF NEW.distance_to_fault IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'distance_to_fault =' || quote_literal(distance_to_fault);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_geological || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_geological = OLD.id;

            -- Create query
            value_query = 'DELETE FROM geological WHERE id=' || id_geological || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_geological_t1 on geological;
CREATE TRIGGER sync_geological_t1 AFTER INSERT OR UPDATE OR DELETE ON geological
    FOR EACH ROW EXECUTE PROCEDURE sync_geological_t1();

