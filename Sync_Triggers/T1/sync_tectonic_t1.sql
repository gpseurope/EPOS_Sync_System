create or replace function sync_tectonic_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_tectonic integer;
        plate_name text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_tectonic = NEW.id;
            plate_name = NEW.plate_name;

            -- Create query
            value_query = 'INSERT INTO tectonic (id, plate_name) VALUES ' ||
                '(' || id_tectonic || ', ' || quote_literal(plate_name) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_tectonic = OLD.id;
            plate_name = NEW.plate_name;
            value_first = true;

            -- Create query
            value_query = 'UPDATE tectonic SET ';

            -- Handle NULL values
            IF NEW.plate_name IS NOT NULL THEN
                value_query = value_query || 'plate_name=' || quote_literal(plate_name);
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Added possibility to update id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_tectonic || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_tectonic = OLD.id;

            -- Create query
            value_query = 'DELETE FROM tectonic WHERE id=' || id_tectonic || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_tectonic_t1 on tectonic;
CREATE TRIGGER sync_tectonic_t1 AFTER INSERT OR UPDATE OR DELETE ON tectonic
    FOR EACH ROW EXECUTE PROCEDURE sync_tectonic_t1();


