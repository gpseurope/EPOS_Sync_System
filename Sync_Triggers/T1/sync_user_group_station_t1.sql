create or replace function sync_user_group_station_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_user_group_station integer;
	    id_user_group integer;
	    id_station integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_user_group_station = NEW.id;
            id_user_group = NEW.id_user_group;
            id_station = NEW.id_station;

            -- Create query
            value_query = 'INSERT INTO user_group_station (id';

            IF id_user_group IS NOT NULL THEN
                value_query = value_query || ', id_user_group';
            END IF;

            IF id_station IS NOT NULL THEN
                 value_query = value_query || ', id_station';
            END IF;

            value_query = value_query || ') VALUES (' || id_user_group_station;

            IF id_user_group IS NOT NULL THEN
                value_query = value_query || ', ' || id_user_group;
            END IF;

            IF id_station IS NOT NULL THEN
                 value_query = value_query || ', ' || id_station;
            END IF;

             value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_user_group_station = OLD.id;
            id_user_group = NEW.id_user_group;
            id_station = NEW.id_station;
            value_first = true;

            -- If this update introduces a different station than the previous one for the current record, then we must remove
            -- the entry associated with the old station from the remote node;
            IF (OLD.id_station != NEW.id_station) THEN
                value_query = 'DELETE FROM user_group_station WHERE id=' || OLD.id || ';';
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, 'T1', OLD.id_station);
            END IF;

             -- Create query

            -- Create query
            -- We must INSERT if the the current record is not available on the remote node
            -- this happens when the id of the station is not present in the remote node
            value_query = 'DO ' || chr(36) || '' || chr(36) || ' BEGIN IF EXISTS (SELECT * FROM user_group_station WHERE id=' || OLD.id ||') THEN ';

            -- UPDATE OP
            value_query = value_query || ' UPDATE user_group_station SET id=' || NEW.id || ',';

            -- Handle NULL values
            IF NEW.id_user_group IS NOT NULL THEN
                value_query = value_query || 'id_user_group=' || id_user_group;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_station =' || id_station;
            END IF;

            value_query = value_query || ' WHERE id=' || id_user_group_station || ';';

            value_query = value_query || ' ELSE ';

            -- INSERT OP
            value_query = value_query || 'INSERT INTO user_group_station (id';

            IF id_user_group IS NOT NULL THEN
                value_query = value_query || ', id_user_group';
            END IF;

            IF id_station IS NOT NULL THEN
                 value_query = value_query || ', id_station';
            END IF;

            value_query = value_query || ') VALUES (' || id_user_group_station;

            IF id_user_group IS NOT NULL THEN
                value_query = value_query || ', ' || id_user_group;
            END IF;

            IF id_station IS NOT NULL THEN
                 value_query = value_query || ', ' || id_station;
            END IF;

            value_query = value_query || ');';

            value_query = value_query || ' END IF; END; ' || chr(36) || '' || chr(36) || ';';


            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_user_group_station = OLD.id;

            -- Create query
            value_query = 'DELETE FROM user_group_station WHERE id=' || id_user_group_station || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = old.id_station;

            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_user_group_station_t1 on user_group_station;
CREATE TRIGGER sync_user_group_station_t1 AFTER INSERT OR UPDATE OR DELETE ON user_group_station
    FOR EACH ROW EXECUTE PROCEDURE sync_user_group_station_t1();

    