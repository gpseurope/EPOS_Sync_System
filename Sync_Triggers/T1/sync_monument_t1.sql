create or replace function sync_monument_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_monument integer;
	    description text;
	    inscription text;
	    height text;
	    foundation text;
	    foundation_depth text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_monument = NEW.id;
            description = NEW.description;
            inscription = NEW.inscription;
            height = NEW.height;
            foundation = NEW.foundation;
            foundation_depth = NEW.foundation_depth;

            -- Create query
            value_query = 'INSERT INTO monument (id, description';

            IF inscription IS NOT NULL THEN
                value_query = value_query || ', inscription';
            END IF;

            IF height IS NOT NULL THEN
                value_query = value_query || ', height';
            END IF;

            IF foundation IS NOT NULL THEN
                value_query = value_query || ', foundation';
            END IF;

            IF foundation_depth IS NOT NULL THEN
                value_query = value_query || ', foundation_depth';
            END IF;

            value_query = value_query || ') VALUES (' || id_monument || ', ' || quote_literal(description);

            IF inscription IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(inscription);
            END IF;

            IF height IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(height);
            END IF;

            IF foundation IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(foundation);
            END IF;

            IF foundation_depth IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(foundation_depth);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_monument = OLD.id;
            description = NEW.description;
            inscription = NEW.inscription;
            height = NEW.height;
            foundation = NEW.foundation;
            foundation_depth = NEW.foundation_depth;

            -- Create query
            value_query = 'UPDATE monument SET ';

            -- Handle NULL values
            IF NEW.description IS NOT NULL THEN
                value_query = value_query || 'description=' || quote_literal(description);
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Added possibility to edit ID
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

            -- Handle NULL values
            IF NEW.inscription IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'inscription =' || quote_literal(inscription);
            END IF;

            -- Handle NULL values
            IF NEW.height IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'height =' || quote_literal(height);
            END IF;

            -- Handle NULL values
            IF NEW.foundation IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'foundation =' || quote_literal(foundation);
            END IF;

            -- Handle NULL values
            IF NEW.foundation_depth IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'foundation_depth =' || quote_literal(foundation_depth);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_monument || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_monument = OLD.id;

            -- Create query
            value_query = 'DELETE FROM monument WHERE id=' || id_monument || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_monument_t1 on monument;
CREATE TRIGGER sync_monument_t1 AFTER INSERT OR UPDATE OR DELETE ON monument
    FOR EACH ROW EXECUTE PROCEDURE sync_monument_t1();

