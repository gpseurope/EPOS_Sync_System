create or replace function sync_station_type_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_station_type integer;
	    name text;
	    type text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_station_type = NEW.id;
            name = NEW.name;
            type = NEW.type;

            -- Create query
            value_query = 'INSERT INTO station_type (id, name';

            IF type IS NOT NULL THEN
                value_query = value_query || ', type';
            END IF;

            value_query = value_query || ') VALUES (' || id_station_type || ', ' || quote_literal(name);

            IF type IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(type);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_station_type = OLD.id;
            name = NEW.name;
            type = NEW.type;
            value_first = true;

            -- Create query
            value_query = 'UPDATE station_type SET ';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
            END IF;

            -- Handle NULL values
            -- Added possibility to update id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

            -- Handle NULL values
            IF NEW.type IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'type =' || quote_literal(type);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_station_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_station_type = OLD.id;

            -- Create query
            value_query = 'DELETE FROM station_type WHERE id=' || id_station_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_station_type_t1 on station_type;
CREATE TRIGGER sync_station_type_t1 AFTER INSERT OR UPDATE OR DELETE ON station_type
    FOR EACH ROW EXECUTE PROCEDURE sync_station_type_t1();


