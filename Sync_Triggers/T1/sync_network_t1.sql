create or replace function sync_network_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_network integer;
	    name text;
	    id_contact integer;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_network = NEW.id;
            name = NEW.name;
            id_contact = NEW.id_contact;

             -- Create query
            value_query = 'INSERT INTO network (id, name';

            IF id_contact IS NOT NULL THEN
                value_query = value_query || ', id_contact';
            END IF;

            value_query = value_query || ') VALUES (' || id_network || ', ' || quote_literal(name);

            IF id_contact IS NOT NULL THEN
                value_query = value_query || ', ' || id_contact;
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_network = OLD.id;
            name = NEW.name;
            id_contact = NEW.id_contact;
            value_first = true;

            -- Create query
            value_query = 'UPDATE network SET ';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
            END IF;

             -- Handle NULL values
             -- Possibility to edit ID
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

             -- Handle NULL values
            IF NEW.id_contact IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_contact =' || id_contact;
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_network || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_network = OLD.id;

            -- Create query
            value_query = 'DELETE FROM network WHERE id=' || id_network || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
			
            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_network_t1 on network;
CREATE TRIGGER sync_network_t1 AFTER INSERT OR UPDATE OR DELETE ON network
    FOR EACH ROW EXECUTE PROCEDURE sync_network_t1();

