CREATE OR REPLACE FUNCTION sync_agency_t1() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
	  value_query text;
    value_metadata text;
    value_stationID integer;
    value_first boolean;

	  id_agency integer;
	  name text;
	  abbreviation text;
	  address text;
	  www text;
	  infos text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_agency = NEW.id;
            name = NEW.name;
            abbreviation = NEW.abbreviation;
            address = NEW.address;
            www = NEW.www;
            infos = NEW.infos;

			-- Create query
            value_query = 'INSERT INTO agency (id, name';

			IF abbreviation IS NOT NULL THEN
                value_query = value_query || ', abbreviation';
                value_first = false;
            ELSE
                 raise notice 'Warning On Inserting new Agency - abbreviation Can Not Be NUll';
            END IF;

			IF address IS NOT NULL THEN
                value_query = value_query || ', address';
                value_first = false;
            END IF;

			IF www IS NOT NULL THEN
                value_query = value_query || ', www';
                value_first = false;
            END IF;

			IF infos IS NOT NULL THEN
                value_query = value_query || ', infos';
                value_first = false;
            END IF;

			value_query = value_query || ') VALUES (' || id_agency || ', ' || quote_literal(name);

			IF abbreviation IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(abbreviation);
            END IF;

			IF address IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(address);
            END IF;

			IF www IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(www);
            END IF;

			IF infos IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(infos);
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        -- 7/2/24 Permite NULLS for non mandatory fields
        -- 7/2/42 raise error message when trying to change mandatory or unique fields to null
        IF (TG_OP = 'UPDATE') THEN
            id_agency = OLD.id;
			name = NEW.name;
            abbreviation = NEW.abbreviation;
            address = NEW.address;
            www = NEW.www;
            infos = NEW.infos;
			value_first = true;

			-- Create query
            value_query = 'UPDATE agency SET ';

			 -- Handle NULL values
             -- Warn on NULL name
            IF NEW.name != OLD.name then
                value_query = value_query || 'name=' || quote_literal(name);
                value_first = false;
                IF NEW.name IS NULL THEN
                    raise notice 'Warning On Updating Agency - null for then name of an agency is not advised';
                END IF;
            END IF;

			-- Handle NULL values
            -- Warn on NULL abreviation
            IF NEW.abbreviation IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'abbreviation=' || quote_literal(abbreviation);
            ELSE
                 raise notice 'Error On Updating Agency - null value for abbreviation not allowed';
            END IF;

			-- Permit NULL values and alter only on change of value
            IF NEW.address != OLD.address then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'address=' || quote_literal(address);
            END IF;

			-- Handle NULL values
			-- Possibility to update id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

			-- Handle NULL values
            -- Permit NULL values and alter only on change of value
            IF NEW.www != OLD.www then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'www=' || quote_literal(www);
            END IF;

			-- Handle NULL values
              -- Permit NULL values and alter only on change of value
            IF NEW.infos != OLD.infos then
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'infos=' || quote_literal(infos);
            END IF;

			-- Create query
            value_query = value_query || ' WHERE id=' || id_agency || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_agency = OLD.id;

            -- Create query
            value_query = 'DELETE FROM agency WHERE id=' || id_agency || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$;

DROP TRIGGER IF EXISTS sync_agency_t1 on agency;
CREATE TRIGGER sync_agency_t1 AFTER INSERT OR UPDATE OR DELETE ON agency
    FOR EACH ROW EXECUTE PROCEDURE sync_agency_t1();


