CREATE OR REPLACE FUNCTION sync_datacenter_station_t1() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
    value_query text;
    value_metadata text;
    value_stationID integer;
    value_first boolean;

    id_datacenter_station integer;
    id_station integer;
    id_datacenter integer;
    datacenter_type text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_datacenter_station = NEW.id;
            id_station = NEW.id_station;
            id_datacenter = NEW.id_datacenter;
            datacenter_type = NEW.datacenter_type;

            IF id_station IS NULL THEN
                RETURN NULL;
            END IF;
        
            -- Create query
            value_query = 'INSERT INTO datacenter_station (id';

            IF id_station IS NOT NULL THEN
                value_query = value_query || ', id_station';
            END IF;

            IF id_datacenter IS NOT NULL THEN
                value_query = value_query || ', id_datacenter';
            END IF;

            IF datacenter_type IS NOT NULL THEN
                value_query = value_query || ', datacenter_type';
            END IF;
            -- Bug fix the id was previously id_station
            value_query = value_query || ') VALUES (' || id_datacenter_station;

            IF id_station IS NOT NULL THEN
                value_query = value_query || ', ' || id_station;
            END IF;

            IF id_datacenter IS NOT NULL THEN
                value_query = value_query || ', ' || id_datacenter;
            END IF;

            IF datacenter_type IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(datacenter_type);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            IF id_station IS NOT NULL THEN
                -- Set station code
                value_stationID = id_station;
            END IF;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_datacenter_station = OLD.id;
            id_station = NEW.id_station;
            id_datacenter = NEW.id_datacenter;
            datacenter_type = NEW.datacenter_type;

            IF id_station IS NULL THEN
                RETURN NULL;
            END IF;

            -- If this update introduces a different station than the previous one for the current record, then we must remove
            -- the entry associated with the old station from the remote node;
            IF (OLD.id_station != NEW.id_station) THEN
                value_query = 'DELETE FROM datacenter_station WHERE id=' || id_datacenter_station || ';';
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, 'T1', OLD.id_station);
            END IF;

            -- Create query
            value_query = 'DO ' || chr(36) || '' || chr(36) || ' BEGIN IF EXISTS (SELECT * FROM datacenter_station WHERE id=' || OLD.id ||') THEN ';

            -- Added possibility to update id
            value_query = value_query || 'UPDATE datacenter_station SET id=' || NEW.id;

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || ', id_station=' || id_station;
            END IF;

            IF NEW.id_datacenter IS NOT NULL THEN
                value_query = value_query || ', id_datacenter=' || id_datacenter;
            END IF;

            IF NEW.datacenter_type IS NOT NULL THEN
                value_query = value_query || ', datacenter_type=' || quote_literal(datacenter_type);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_datacenter_station || ';';

            value_query = value_query || ' ELSE ';

            -- Create query
            value_query = value_query || 'INSERT INTO datacenter_station (id';

            IF id_station IS NOT NULL THEN
                value_query = value_query || ', id_station';
            END IF;

            IF id_datacenter IS NOT NULL THEN
                value_query = value_query || ', id_datacenter';
            END IF;

            IF datacenter_type IS NOT NULL THEN
                value_query = value_query || ', datacenter_type';
            END IF;

            value_query = value_query || ') VALUES (' || NEW.id;

            IF id_station IS NOT NULL THEN
                value_query = value_query || ', ' || id_station;
            END IF;

            IF id_datacenter IS NOT NULL THEN
                value_query = value_query || ', ' || id_datacenter;
            END IF;

            IF datacenter_type IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(datacenter_type);
            END IF;

            value_query = value_query || '); END IF; END; ' || chr(36) || '' || chr(36) || ';';

            -- Set metadata type
            value_metadata = 'T1';

            IF id_station IS NOT NULL THEN
                -- Set station code
                value_stationID = id_station;
            END IF;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);


            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_datacenter_station = OLD.id;
            id_station = OLD.id_station;

            -- Create query
            value_query = 'DELETE FROM datacenter_station WHERE id=' || id_datacenter_station || ';';

            -- Set metadata type
            value_metadata = 'T1';

            IF id_station IS NOT NULL THEN
                -- Set station code
                value_stationID = id_station;
            END IF;

            -- Only store remote query to delete if this is not a "cascade on delete" operation
            -- That is, the station exists
            IF EXISTS(SELECT id FROM station where id=value_stationID) THEN
                -- Insert data to queries
                INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);
            END IF;

            RETURN NULL;
        END IF;
    END;
$$;

DROP TRIGGER IF EXISTS sync_datacenter_station_t1 on datacenter_station;
CREATE TRIGGER sync_datacenter_station_t1 AFTER INSERT OR UPDATE OR DELETE ON datacenter_station
    FOR EACH ROW EXECUTE PROCEDURE sync_datacenter_station_t1();


