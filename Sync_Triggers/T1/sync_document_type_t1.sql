create or replace function sync_document_type_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_document_type integer;
	    name text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_document_type = NEW.id;
            name = NEW.name;

            -- Create query
            value_query = 'INSERT INTO document_type (id, name) VALUES ' ||
                '(' || id_document_type || ', ' || quote_literal(name) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_document_type = OLD.id;
            name = NEW.name;
			value_first = true;

            -- Create query
            -- Added possibility to update ID
            value_query = 'UPDATE document_type SET id=' || NEW.id || ', name=' || quote_literal(name) || ' WHERE id=' || id_document_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_document_type = OLD.id;

            -- Create query
            value_query = 'DELETE FROM document_type WHERE id=' || id_document_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_document_type_t1 on document_type;
CREATE TRIGGER sync_document_type_t1 AFTER INSERT OR UPDATE OR DELETE ON document_type
    FOR EACH ROW EXECUTE PROCEDURE sync_document_type_t1();

    
