create or replace function sync_log_type_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_log_type integer;
	    name text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_log_type = NEW.id;
            name = NEW.name;

            -- Create query
            value_query = 'INSERT INTO log_type (id, name) VALUES ' ||
                '(' || id_log_type || ', ' || quote_literal(name) || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_log_type = OLD.id;
            name = NEW.name;

            -- Create query
            value_query = 'UPDATE log_type SET id=' || NEW.id || '';

            -- Handle NULL values
            IF NEW.name IS NOT NULL THEN
                value_query = value_query || ', name=' || quote_literal(name);
            END IF;

             -- Create query
            value_query = value_query || ' WHERE id=' || id_log_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_log_type = OLD.id;

            -- Create query
            value_query = 'DELETE FROM log_type WHERE id=' || id_log_type || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_log_type_t1 on log_type;
CREATE TRIGGER sync_log_type_t1 AFTER INSERT OR UPDATE OR DELETE ON log_type
    FOR EACH ROW EXECUTE PROCEDURE sync_log_type_t1();

