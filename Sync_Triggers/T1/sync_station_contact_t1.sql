create or replace function sync_station_contact_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
        value_first boolean;

	    id_station_contacts integer;
	    id_station integer;
	    id_contact integer;
	    role text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_station_contacts = NEW.id;
            id_station = NEW.id_station;
            id_contact = NEW.id_contact;
            role = NEW.role;

            -- Create query
            value_query = 'INSERT INTO station_contact (id, id_station, id_contact';

            IF role IS NOT NULL THEN
                value_query = value_query || ', role';
            END IF;

            value_query = value_query || ') VALUES (' || id_station_contacts || ', ' || id_station || ', ' || id_contact;

            IF role IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(role);
            END IF;

            value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

         -- Handle update
         IF (TG_OP = 'UPDATE') THEN
            id_station_contacts = OLD.id;
            id_station = NEW.id_station;
            id_contact = NEW.id_contact;
            role = NEW.role;
            value_first = true;

            -- Create query
            value_query = 'UPDATE station_contact SET ';

            -- Handle NULL values
            IF NEW.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || id_station;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF NEW.id_contact IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_contact =' || id_contact;
            END IF;

            -- Handle NULL values
            IF NEW.role IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'role =' || quote_literal(role);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_station_contacts || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = id_station;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;

         -- Handle delete
         IF (TG_OP = 'DELETE') THEN
            id_station_contacts = OLD.id;

            -- Create query
            -- By setting value_stationID to NULL we need to ensure that the correct entry is removed
            -- in the remote node (not use only ID)
            -- Handle NULL values

            value_query = 'DELETE FROM station_contact WHERE id=' || id_station_contacts || ' AND ';

            -- Handle NULL values
            IF OLD.id_station IS NOT NULL THEN
                value_query = value_query || 'id_station=' || OLD.id_station;
                value_first = false;
            END IF;

            -- Handle NULL values
            IF OLD.id_contact IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_contact=' || OLD.id_contact;
            END IF;

            -- Handle NULL values
            IF OLD.role IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ' AND ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'role=' || quote_literal(OLD.role);
            END IF;

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            -- We can't use station_id anymore in the new version of the database
            -- because now station_id is a foreign key, in table queries, that belongs to table station
            -- We can't use a key that was previously removed from the station table

            -- value_stationID = old.id_station;
            value_stationID = NULL;
            -- raise notice '%', value_query; debug only

            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
         END IF;
    END;
$$
;

DROP TRIGGER IF EXISTS sync_station_contact_t1 on station_contact;
CREATE TRIGGER sync_station_contact_t1 AFTER INSERT OR UPDATE OR DELETE ON station_contact
    FOR EACH ROW EXECUTE PROCEDURE sync_station_contact_t1();

