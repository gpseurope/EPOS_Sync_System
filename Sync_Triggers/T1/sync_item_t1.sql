create or replace function sync_item_t1() returns trigger
	language plpgsql
as $$
DECLARE
	    value_query text;
        value_metadata text;
        value_stationID integer;
		value_first boolean;

	    id_item integer;
	    id_item_type integer;
	    id_contact_as_producer integer;
	    id_contact_as_owner integer;
	    comment text;

    BEGIN
        -- Handle insert
        IF (TG_OP = 'INSERT') THEN
            id_item = NEW.id;
            id_item_type = NEW.id_item_type;
            id_contact_as_producer = NEW.id_contact_as_producer;
            id_contact_as_owner = NEW.id_contact_as_owner;
            comment = NEW.comment;

			 -- Create query
            value_query = 'INSERT INTO item (id, id_item_type';

			IF id_contact_as_producer IS NOT NULL THEN
                value_query = value_query || ', id_contact_as_producer';
            END IF;

			IF id_contact_as_owner IS NOT NULL THEN
                value_query = value_query || ', id_contact_as_owner';
            END IF;

			IF comment IS NOT NULL THEN
                value_query = value_query || ', comment';
            END IF;

			value_query = value_query || ') VALUES (' || id_item || ', ' || id_item_type;

			IF id_contact_as_producer IS NOT NULL THEN
                value_query = value_query || ', ' || id_contact_as_producer;
            END IF;

			IF id_contact_as_owner IS NOT NULL THEN
                value_query = value_query || ', ' || id_contact_as_owner;
            END IF;

			IF comment IS NOT NULL THEN
                value_query = value_query || ', ' || quote_literal(comment);
            END IF;

			value_query = value_query || ');';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle update
        IF (TG_OP = 'UPDATE') THEN
            id_item = OLD.id;
			id_item_type = NEW.id_item_type;
            id_contact_as_producer = NEW.id_contact_as_producer;
            id_contact_as_owner = NEW.id_contact_as_owner;
            comment = NEW.comment;
			value_first = true;

			-- Create query
            value_query = 'UPDATE item SET ';

			-- Handle NULL values
            IF NEW.id_item_type IS NOT NULL THEN
                value_query = value_query || 'id_item_type=' || id_item_type;
                value_first = false;
            END IF;

			-- Handle NULL values
			-- Added possibility to edit id
            IF NEW.id IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id=' || NEW.id;
            END IF;

			-- Handle NULL values
            IF NEW.id_contact_as_producer IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_contact_as_producer =' || id_contact_as_producer;
            END IF;

			-- Handle NULL values
            IF NEW.id_contact_as_owner IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'id_contact_as_owner =' || id_contact_as_owner;
            END IF;

			-- Handle NULL values
            IF NEW.comment IS NOT NULL THEN
                IF value_first IS false THEN
                    value_query = value_query || ', ';
                ELSE
                    value_first = false;
                END IF;
                value_query = value_query || 'comment =' || quote_literal(comment);
            END IF;

            -- Create query
            value_query = value_query || ' WHERE id=' || id_item || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Avoid duplicate queries that can happen in cascade update
            -- Remove the duplicate
            DELETE FROM queries WHERE query ILIKE 'UPDATE item % WHERE id=141%';

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;

        -- Handle delete
        IF (TG_OP = 'DELETE') THEN
            id_item = OLD.id;

            -- Create query
            value_query = 'DELETE FROM item WHERE id=' || id_item || ';';

            -- Set metadata type
            value_metadata = 'T1';

            -- Set station code
            value_stationID = NULL;

            -- Insert data to queries
            INSERT INTO queries (query, metadata, station_id) VALUES (value_query, value_metadata, value_stationID);

            RETURN NULL;
        END IF;
    END;
$$
;


DROP TRIGGER IF EXISTS sync_item_t1 on item;
CREATE TRIGGER sync_item_t1 AFTER INSERT OR UPDATE OR DELETE ON item
    FOR EACH ROW EXECUTE PROCEDURE sync_item_t1();

