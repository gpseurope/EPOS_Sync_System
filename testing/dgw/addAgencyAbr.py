#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import join
from os import walk
import json
import codecs



import config

conf_dict = config.read()

def run():
    for root, dirs, files in walk(join(conf_dict['DATA_DIR'], 'test_node_T1')):
        for name in files:
                filename = join(root, name)
                try:
                    if filename.split('.')[-1] == 'json':
                        jsn = json.load(codecs.open(filename, 'r', encoding='utf8'))
                        for contact in jsn['station_contacts']:
                            # print contact['contact']['agency']['name']
                            if contact['contact']['agency']['name'] == u"Observatoire de la Côte d'Azur":
                                contact['contact']['agency']['abbreviation'] = 'OCA'
                            if contact['contact']['agency']['name'] == u"National Observatory of Athens":
                                contact['contact']['agency']['abbreviation'] = 'NOA'
                            if contact['contact']['agency']['name'] == u"Direcao-Geral do Territorio":
                                contact['contact']['agency']['abbreviation'] = 'RENEP'
                            if contact['contact']['agency']['name'] == u"National Institute for Earth Physics":
                                contact['contact']['agency']['abbreviation'] = 'NIEP'
                            if contact['contact']['agency']['name'] == u"Royal Observatory of Belgium":
                                contact['contact']['agency']['abbreviation'] = 'EPN_HDC'

                            if contact['contact']['agency']['address'].find('CZ-251 65') >=0 :
                                contact['contact']['agency']['abbreviation'] = 'CzechGeo'
                            
                            if contact['contact']['agency']['address'].find('Grottaminarda') >=0 :
                                contact['contact']['agency']['abbreviation'] = 'INGV'
                except Exception as e:
                    print filename, ' :', e.message
                json.dump(jsn, open(filename, 'w'),sort_keys=True,indent=4, separators=(',', ': '))
        
   



# update agency set abbreviation = 'OCA' where id = (select id from agency where name = 'Observatoire de la Côte d'Azur');
# update agency set abbreviation = 'CzechGeo' where id = (select id from agency where address LIKE '%CZ-251 65');
# update agency set abbreviation = 'INGV' where id = (select id from agency where name like '%Grottaminarda%');
# update agency set abbreviation = 'NOA' where id = (select id from agency where name ='National Observatory of Athens');
# update agency set abbreviation = 'RENEP' where id = (select id from agency where name ='Direcao-Geral do Territorio');
# update agency set abbreviation = 'NIEP' where id = (select id from agency where name = 'National Institute for Earth Physics');
# update agency set abbreviation = 'EPN_HDC' where id = (select id from agency where name = 'Royal Observatory of Belgium');