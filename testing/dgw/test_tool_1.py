import requests, argparse, json, glob
from os import path as pth
from urlparse import urljoin
import requests 

import uploadT1
import addAgencyAbr
import uploadDatacenter
import config


result = []
conf_dict = config.read()

DIR = pth.join(conf_dict['DATA_DIR'], 'test_node_T1/')
FILETEST = pth.join(pth.join(conf_dict['DATA_DIR'], 'test_results'), 'uploadT1.json')
print "testing T1 upload"
print "====================="
status_code_list = uploadT1.run(DIR)
if uploadT1.test_status_code(status_code_list):
    msg = 'T1 upload  is a success'
    print msg
    result.append(msg)
else:
    msg =  'T1 upload is a failure'
    print msg
    result.append(msg)
    import sys
    sys.exit()
print "====================="

print "testing Add Agencies and Datacenters"
print "====================="
addAgencyAbr.run()

DC_FILE = pth.join(conf_dict['DATA_DIR'],"test_node_datacenter","datacenter.json")
uploadDatacenter.run(DC_FILE)


