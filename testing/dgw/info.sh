#!/bin/bash
g1='grep -v row'
g2='grep -v count'
g3='grep -v \-\-'

echo "nodes"
sudo su - postgres -c "psql -d "gnss-europe" -c 'select id,name,host from node'" | $g1|$g2|$g3
echo -n "sync queries  "
sudo su - postgres -c "psql -d "gnss-europe" -c 'select count(*) from queries'"  | $g1|$g2|$g3
echo -n "sync failed queries  "
sudo su - postgres -c "psql -d "gnss-europe" -c 'select count(*) from failed_queries'" | $g1|$g2|$g3

