#!/bin/bash

cdir=$(pwd)


sudo service postgresql-10.service restart

sleep 5

/home/vagrant/epos/vm-scripts/resetdatabase.tcsh T1
./nodes.sh

echo "db restarted, reset, nodes added. read x"; read x

echo "stoping and starting fwss. Enter"; read x
pkill python
cd /home/vagrant/epos/fwss;
python web_server.py >/dev/null 2>/dev/null &
cd $cdir


echo "Adding stations, agencias and DC. Enter"; read x
python  test_tool_1.py
echo "7 stations, agencies and datacenteres added. Enter" ; read x

echo "Making connections.  Enter"; read x
./connections.sh
echo "connections made 4 to node 1 3 to node. Enter"; read x


echo "Ready to modify a station. Enter" ; read x
python  test_tool_2.py

echo "Ready to delete a station. Enter"; read x
python test_tool_3.py 


