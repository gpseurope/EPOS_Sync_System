import requests, argparse, json, glob
from os import path as pth
from urlparse import urljoin
import requests 

import uploadT1
import addAgencyAbr
import uploadDatacenter
import config


result = []
conf_dict = config.read()

print "testing T1 modification"
print "====================="
DIR = pth.join(conf_dict['DATA_DIR'], 'test_node_T1_modif/')
# FILETEST = pth.join(pth.join(conf_dict['DATA_DIR'], 'test_results'), 'uploadT1_modif.json')
status_code_list = uploadT1.run(DIR)
if uploadT1.test_status_code(status_code_list):
    msg =  'T1 Update is a success'
    print msg
    result.append(msg)
else:
    msg =  'T1 Update is a failure'
    print msg
    result.append(msg)
    import sys
    sys.exit()
print "====================="


