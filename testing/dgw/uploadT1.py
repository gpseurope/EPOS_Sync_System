import requests, argparse, json, glob
from os import path as pth
from urlparse import urljoin
import requests 
import config

conf_dict = config.read()

URL_FWSS = conf_dict['URL_FWSS']
URL_SITELOG = urljoin(URL_FWSS,'gps/sitelog')
URL_CONTACTS = urljoin(URL_FWSS,'gps/contact')
URL_DELETE_STATION = urljoin(URL_FWSS,'gps/station')

URL_GLASSAPI_T1 = conf_dict['URL_GLASSAPI_T1']

def run(DIR):
    status_code_list = []
    files = glob.glob(pth.join(DIR, '*'))    
    for file_path in files:
        if pth.exists(file_path):
            jsn = json.load(open(file_path))
            contact_list = json.dumps(jsn['station_contacts'])
            # print URL_SITELOG
            res_contact = requests.post(URL_CONTACTS, data=contact_list)
            res = requests.post(URL_SITELOG, data=open(file_path, 'rb'))
            status_code_list.append(res.status_code)
            # print "File", file_path, "posted with result", res.status_code
        else:
            print file_path, "does not exist"
    
    return status_code_list

        
def delete(code):
    
    res = requests.delete(URL_DELETE_STATION+'/'+code)
    print "Station", code, "deleted with result", res.status_code
    return [res.status_code]


def test_status_code(status_code_list):
    for i in status_code_list:
        if i not in [200, 201]:
            return False
    return True

def test(FILETEST):
    jsn = requests.get(URL_GLASSAPI_T1)
    if jsn.status_code not in [200, 201]:
        print "unable to reach", URL_GLASSAPI_T1
        # import sys
        # sys.exit()
    else:
        jt = jsn.json()
        rf = json.load(open(FILETEST))
        jt_station = [x['marker_long_name'] for x in jt]
        
        jt_station.sort()
        rf_station = [x['marker_long_name'] for x in rf]
        rf_station.sort()
        diff = []
        for s in rf_station:
            if s not in jt_station:
                diff.append(s) 
        # if diff == []:
        #     print "T1 successfully imported"
        # else:
        #     print "failure to import T1" 
        #     print "missing stations", diff
        if jt == rf:
            return True
         


# if __name__ == '__main__':

#     run()
    

	

