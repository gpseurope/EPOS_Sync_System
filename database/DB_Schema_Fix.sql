ALTER TABLE public.rinex_file DROP CONSTRAINT uq_rinex_file_md5checksum;
ALTER TABLE public.rinex_file  ADD CONSTRAINT uq_rinex_file_md5checksum_data_center_structure UNIQUE(id_data_center_structure, md5checksum);
ALTER TABLE public.gnss_obsnames ADD CONSTRAINT gnss_obsnames_unique UNIQUE (name);