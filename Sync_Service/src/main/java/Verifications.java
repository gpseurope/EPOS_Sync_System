
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Verifications {

    public static boolean checkTrigger(Build build, Configuration conf, Log cLog) {
        boolean valid = true;
        List<Node> nodes = new ArrayList<>();
        Statement s_localNode;
        ResultSet rs_localNode;

        // Get all nodes in the network from the local node
        try {
            Node node; // current node;
            s_localNode = conf.getLocalConnection().createStatement();
            // Selects local node metadata from database
            rs_localNode = s_localNode.executeQuery("SELECT * FROM node");

            // Check if returns results
            while (rs_localNode.next()) {
                node = new Node(
                        rs_localNode.getInt("id"),
                        rs_localNode.getString("name"),
                        rs_localNode.getString("host"),
                        rs_localNode.getString("port"),
                        rs_localNode.getString("dbname"),
                        rs_localNode.getString("username"),
                        rs_localNode.getString("password"),
                        rs_localNode.getString("url"),
                        conf, cLog);
                nodes.add(node);
            }
            // Close connections
            rs_localNode.close();
            s_localNode.close();

        } catch (SQLException e) {
            //Important message always sent to standard output
            System.err.println("Error on local node: " + e.getClass().getName() + ": " + e.getMessage());
            cLog.write("Error on local node: " + e.getClass().getName() + ": " + e.getMessage(), Log.Type.error);
            System.exit(2);
        }

        if (nodes.isEmpty()) {
            //Important message always sent to standard output
            System.out.println("ERROR  > Nodes are not configured correctly ");
            cLog.write("Nodes are not configured correctly", Log.Type.error);
            System.exit(2);
        }

        // Connect to the nodes and check the minimum version of the sync tool supported
        // This information is available on the service table
        //Static FLAG to on error to either skipNode or exit application. This is useful for debugging
        final boolean exitApp = false;

        for (int i = 0; i < nodes.size(); i++) {

            Node node = nodes.get(i);
            Integer nodeId = node.id;

            if (!node.isOnline()) {
                System.out.println("Node is offline " + node.name);
                continue;
            }

            Connection remoteConnection = null;

            Properties props = new Properties();
            props.setProperty("user", node.username);
            props.setProperty("password", node.password);
            props.setProperty("logUnclosedConnections", "true");
            String connectionString = "jdbc:postgresql://" + node.host + ":" + node.port + "/" + node.dbname;

            try {
                Class.forName("org.postgresql.Driver");
                remoteConnection = DriverManager.getConnection(connectionString, props);
                remoteConnection.setAutoCommit(true);

                // Checks if currently logged user has permissions on database
                if (SharedMethods.doesntHasPermissions(remoteConnection, node.username, conf, cLog)) {
                    //Important message always sent to standard output
                    System.out.println(" > Exiting... Reason: The configured user doesn't Have permissions to access the database.");
                    cLog.write("Exiting, the configured user doesn't Have permissions to access the database.", Log.Type.error);
                    if (exitApp) {
                        System.exit(2);
                    } else {
                        continue; //skip node
                    }
                }
                //Important message always sent to standard output
                System.out.println("\n> Trigger check in node (" + node.name + ") " + node.host + ": ");

                // Query the node's database and get info from the service table
                Statement s_Node;
                ResultSet rs_Node;
                String queryStr = "";
                try {
                    s_Node = remoteConnection.createStatement();

                    String query = "SELECT  md5(pg_get_functiondef(tgfoid)) AS trigger_md5 FROM pg_trigger WHERE tgname = 'sync_rinex_file_t2';";
                    rs_Node = s_Node.executeQuery(query);
                    rs_Node.next();
                    String resultMD5=rs_Node.getString(1);
                    System.out.println("" + node.dbname + ") md5 trigger sync_rinex_file_t2 on node is " + resultMD5 );
                    
                    query = "SELECT  pg_get_functiondef(tgfoid) AS trigger_text FROM pg_trigger WHERE tgname = 'sync_rinex_file_t2';";
                    rs_Node = s_Node.executeQuery(query);
                    rs_Node.next();
                    String result=rs_Node.getString(1);
                    System.out.println("" + node.dbname + ") version trigger sync_rinex_file_t2 on node is " + extractLine(result) );

                   
                } catch (SQLException e) {
                    //Important message always sent to standard output
                    System.err.println("> L147a For node " + node.host + " - " + e.getClass().getName() + ": " + e.getMessage() + " " + queryStr);
                    cLog.write("L147a Error on node " + node.host + " - " + e.getClass().getName() + ": " + e.getMessage() + " " + queryStr, Log.Type.error);

                    if (exitApp) {
                        System.exit(2);
                    } else {
                        continue; //skip node
                    }
                }

            } catch (ClassNotFoundException | SQLException e) {
                //e.printStackTrace();
                //Important message always sent to standard output
                System.err.println("> L156a For node " + node.host + " - " + e.getClass().getName() + ": " + e.getMessage());
                cLog.write("L156a Error on node " + node.host + " - " + e.getClass().getName() + ": " + e.getMessage(), Log.Type.error);

                if (exitApp) {
                    System.exit(2);
                } else {
                    continue; //skip node
                }
            } //End of try for remote node
            finally {
                if (remoteConnection != null) {
                    try {
                        remoteConnection.close();
                    } catch (SQLException s) {
                        System.err.println("Can't Close an Opened Connection: " + s);
                        System.err.println("Node Finished");
                    }
                }
            }
        } //end of for loop

        //End of main function
        return (valid);
    }

    // Method to extract a specific line from a multi-line string
    private static String extractLine(String input) {
        String[] arrOfstr = input.split("\n");
        if (arrOfstr.length >= 0){
            for (int i=0;i<arrOfstr.length;i++){
                if ( arrOfstr[i].contains("Version") )
                    return arrOfstr[i];
             }
        }
        return "";
    }
    
}
