import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.io.File;
public class Log {
    // ! By setting this to true the file generated will be huge
    // Instead set the flag debug to true in the SyncService.cfg file
    private boolean writeInfoMessages=false;
    private boolean firstTimeE=true;
    private boolean firstTimeO=true;
    private boolean firstTimeF=true;
    private boolean firstTimeD=true;

    private String filename;
    private Configuration configuration;
    /**
     * Log Types
     * failed       - Failed queries related messages
     * offline      - Offline nodes related messages
     * error        - Generic or structure related errors
     * info         - Generic information messages
     * decrypting   - Password decrypting related errors
     * forced       - Message will be forced to logFile
     */
    public enum Type {info, error, offline, failed, decryption, forced}

    Log(){}

    public void setConfiguration(Configuration configuration){
        this.configuration = configuration;
        this.filename = this.configuration.getLogfile();
        if (!isFileEmpty()) newLine(); // new line for each run of the sync app except when the file is empty
    }

    /**
     * Check if the log file is empty
    **/
    public boolean isFileEmpty(){
        try {
            File myObj = new File(filename);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()){
                String data = myReader.nextLine();
                if (data.length() == 0){
                    myReader.close();
                    return true;
                }
            }
            myReader.close();
        } catch (Exception e) {
            System.out.println("FileName : "+ filename+ " Exception in function log: isfileEmpty. File may not exist" );
            System.out.println( e.getMessage())  ;
            return true;
        }
        return false;
    }

    /**
     * Simply add a new line to the log file
     */
    public void newLine(){
        try(
            FileWriter fw = new FileWriter(filename, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw)){
            // Add content to log file
            out.println("");
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
            e.printStackTrace();
            System.out.println("Fatal Error in function log:newLine");
            System.exit(2);
        }
    }

    /**
     * Creates new configuration
     * @param line log line
     * @param type type of log
     */
    public void write(String line, Type type) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss ");
        Date date = new Date();
        switch (type){
            case info:
                //if (!writeInfoMessages)  return;
                //break;
                if ( configuration.isMaxdebug())
                    break;
                else
                    return;
            case error:
                if (!configuration.getDebug()) {
                    if (firstTimeE) {
                        System.out.println("> An error has occurred, please see the log file for more details. " + this.filename) ;
                        firstTimeE = false;
                    }
                }
                break;
            case offline:
                if (!configuration.getDebug()) {
                    if (firstTimeO) {
                        System.out.println("> One or more nodes are offline please see the log file for more details.");
                        firstTimeO = false;
                    }
                }
                break;
            case failed:
                if (!configuration.getDebug()) {
                    if (firstTimeF) {
                        System.out.println("> There are one or more failed queries please see the log file for more details.");
                        firstTimeF=false;
                    }
                }
                break;
            case decryption:
                if (!configuration.getDebug()) {
                    if (firstTimeD) {
                        System.out.println("> There was an issue when decrypting the password please see the log file for more details.");
                        firstTimeD = false;
                    }
                }
                break;
            case forced:
                break;
        }

        // Log size control
        // Before writing to it, delete the log file if it is too large
        File file =new File(filename);
        if(file.exists()){
            double megabytes = file.length() / (1024 * 1024);
            //if (configuration.isMaxdebug() )  System.out.println("Log File size ~=" + megabytes +" MB");
            // Delete files bigger that 200 megabytes
            if (megabytes >= 200) {
                System.out.println("> The log file will be removed (its size exceeds 200 MBs).");
                file.delete();
            }
        }
        file =new File(filename);
        // Write to log
        try(
            FileWriter fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw)){
            // Reformat log line
            line = line.replace("\n", " ");
            line = line.trim().replaceAll(" +", " ");
            // Add content to log file
            out.println(dateFormat.format(date) + type.toString().toUpperCase() + " : " + line);
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
            e.printStackTrace();
            System.out.println("Fatal Error in function log:write");
            System.exit(2);
        }

    }
}
