import java.io.IOException;
import java.util.Properties;

public class Build {
    public String version;
    public String artifactId;
    public String buildNumber;
    public String buildDate;

    Build(){
        getBuildData();
    }

    /**
     * Returns string with build data
     * @return string
     */
    public String toString(){
        return "Running "+artifactId+" - v"+version+"."+buildNumber+" ("+buildDate+")";
    }

    /**
     * GETS BUILD DATA FROM BUILD DATA FILE
     * This method will read the project properties from a file
     */
    public void getBuildData(){
        Properties properties = new Properties();
        try {
            // Loads project build properties file
            properties.load(this.getClass().getResourceAsStream("/project.properties"));
            // Assigns build variables
            this.version = properties.getProperty("version");
            this.artifactId = properties.getProperty("artifactId");
            this.buildNumber = properties.getProperty("buildNumber");
            this.buildDate = properties.getProperty("buildDate");
            //
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
