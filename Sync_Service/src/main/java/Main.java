import java.sql.*;
import java.util.List;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

public class Main {

    public static boolean passwordIsEncrypted = false;
    public static boolean includeList = false;
    public static ArrayList <String> includeListNodes= new ArrayList();
    public static ArrayList <Integer> includeListDest= new ArrayList();
    
    private static final String OS = System.getProperty("os.name").toLowerCase();
    
    public static boolean checkVersion(Build build, Configuration conf, Log cLog){
        boolean valid=true;
        List<Node> nodes = new ArrayList<>();
        Statement s_localNode;
        ResultSet rs_localNode;

        // Get all nodes in the network from the local node
        try {
            Node node; // current node;
            s_localNode = conf.getLocalConnection().createStatement();
            // Selects local node metadata from database
            rs_localNode = s_localNode.executeQuery("SELECT * FROM node");

            // Check if returns results
            while (rs_localNode.next()) {
                node = new Node(
                        rs_localNode.getInt("id"),
                        rs_localNode.getString("name"),
                        rs_localNode.getString("host"),
                        rs_localNode.getString("port"),
                        rs_localNode.getString("dbname"),
                        rs_localNode.getString("username"),
                        rs_localNode.getString("password"),
                        rs_localNode.getString("url"),
                        conf, cLog);
                nodes.add(node);
            }
            // Close connections
            rs_localNode.close();
            s_localNode.close();

        } catch (SQLException e ){
            //Important message always sent to standard output
            System.err.println("Error on local node: " + e.getClass().getName()+": "+ e.getMessage() );
            cLog.write("Error on local node: "+ e.getClass().getName()+": "+ e.getMessage(), Log.Type.error);
            System.exit(2);
        }

        if (nodes.isEmpty()) {
            //Important message always sent to standard output
            System.out.println("ERROR  > Nodes are not configured correctly ");
            cLog.write("Nodes are not configured correctly", Log.Type.error);
            System.exit(2);
        }

        // Connect to the nodes and check the minimum version of the sync tool supported
        // This information is available on the service table
        
        //Static FLAG to on error to either skipNode or exit application. This is useful for debugging
        final boolean exitApp=false;
        
        for(int i=0; i < nodes.size(); i++) {
            
            Node node = nodes.get(i);
            Integer nodeId = node.id;
            
            /*
            If an Include List has been given check that the node is in list
            if not then contine
            */
            if (includeList==true &&    
                     //( ! includeListNodes.contains(node.name) ) ){
                       ( !SharedMethods.containsIgnoreCase(includeListNodes,node.name) ) ) {
                System.out.println("Skipping CheckVersion for Node "+ node.name);
                continue;
            }
            
            if (!node.isOnline()) {
                System.out.println("Node is offline "+ node.name);
                continue;
            }
            // Create connection with the node's database
            
            if (includeList==true &&  ( 
                    //includeListNodes.contains(node.name) ) ){
                    SharedMethods.containsIgnoreCase(includeListNodes,node.name) ) ){
                    
                includeListDest.add(nodeId);
            }
            
            Connection remoteConnection = null;

            Properties props = new Properties();
            props.setProperty("user", node.username);
            props.setProperty("password", node.password);
            props.setProperty("logUnclosedConnections", "true");
            String connectionString = "jdbc:postgresql://" + node.host + ":" + node.port + "/" + node.dbname;

            try {
                Class.forName("org.postgresql.Driver");
                remoteConnection = DriverManager.getConnection(connectionString, props);
                remoteConnection.setAutoCommit(true);

                // Checks if currently logged user has permissions on database
                if (SharedMethods.doesntHasPermissions(remoteConnection, node.username, conf, cLog)) {
                   //Important message always sent to standard output
                    System.out.println(" > Exiting... Reason: The configured user doesn't Have permissions to access the database.");
                    cLog.write("Exiting, the configured user doesn't Have permissions to access the database.", Log.Type.error);
                    if (exitApp) System.exit(2);
                    else continue; //skip node
                }
                //Important message always sent to standard output
                System.out.println("> Version check in node (" + node.name + ") " + node.host + ": ");

                // Query the node's database and get info from the service table
                Statement s_Node;
                ResultSet rs_Node;
                String queryStr="";
                try {
                    s_Node = remoteConnection.createStatement();
                          
                    //Paul: Bug Fix use the remote database name
                    String query = "select description from pg_shdescription join pg_database on objoid = pg_database.oid where datname = \'"+node.dbname+"\'";
                    rs_Node = s_Node.executeQuery( query );
                    rs_Node.next();
                    System.out.println("Database (" +node.dbname+ ") version on node is " + rs_Node.getString(1));

                    queryStr = "SELECT COUNT(id) as n FROM service WHERE name=\'SyncService\' and version <="+ build.version ;
                    rs_Node = s_Node.executeQuery(queryStr);
                    // Is the current version of the Sync valid against the database ?
                    rs_Node.next();
                    if (rs_Node.getInt("n") == 0) valid=false;
                    //
                    if (valid){
                        //Important message always sent to standard output
                        System.out.println("Sync service compatibility: valid");
                    }
                    else {
                        //Important message always sent to standard output
                        System.out.println("Sync service compatibility: invalid");
                        cLog.write(" Invalid Version For node (" + node.name + ") " + node.host, Log.Type.error);
                    }
                    //
                }catch (SQLException e){
                    //Important message always sent to standard output
                    System.err.println("> L147 For node " + node.host + " - " + e.getClass().getName()+": "+ e.getMessage() + " " + queryStr );
                    cLog.write("L147 Error on node " + node.host + " - " + e.getClass().getName()+": "+ e.getMessage() + " " + queryStr , Log.Type.error);

                    if (exitApp) System.exit(2);
                    else continue; //skip node
                }
                
            }catch (ClassNotFoundException | SQLException e) {
                //e.printStackTrace();
                //Important message always sent to standard output
                System.err.println("> L156 For node " + node.host + " - " + e.getClass().getName()+": "+ e.getMessage() );
                cLog.write("L156 Error on node " + node.host + " - " + e.getClass().getName()+": "+ e.getMessage(), Log.Type.error);
                
                if (exitApp) System.exit(2);
                else continue; //skip node
            
            } //End of try for remote node
            finally{
                if(remoteConnection!=null){
                    try{
                        remoteConnection.close();
                    }catch(SQLException s){
                        System.err.println("Can't Close an Opened Connection: "+ s);
                        System.err.println("Node Finished");
                    }
                }
            }
        } //end of for loop
        
        //End of main function
        return (valid);
    }

    public static void main(String[] args){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        System.out.println("SYNCHRONIZATION SERVICE STARTING: "+ OS +" "+ dtf.format(now) );
        System.out.println("> Working Directory = " + System.getProperty("user.dir"));
        Log cLog = new Log();
        // Get current build data
        Build build = new Build();

        checkRunningProcess();
        
        String fileName = "./SyncService.cfg";
        if (isWindows())
                fileName = ".\\SyncService.cfg";
        
        //set the configuration file and optional list of nodes to include
        
        // java sync.jar
        // java sync.jar filename 
        // java sync.jar filename -I node1 node2 nodeN
        // java sync.jar -I node1 node2 nodeN
        
        if (args.length>0) {
            
            if (args[0].equalsIgnoreCase("-v")){
                System.out.println("> The current build " + build );
                System.exit(0);
            }
            
            if (args[0].equalsIgnoreCase("-h")){
                System.out.println("> The current build " + build );
                System.out.println("> The default config file is "+fileName);
                System.out.println("");
                System.out.println("> The default startup is simply: ");
                System.out.println("> java -jar SyncService.jar");
                System.out.println("");
                System.out.println("> To print the version and exit:");
                System.out.println("> java -jar SyncService.jar -v");
                System.out.println("");
                System.out.println("> To specify a config file simple add the filename");
                System.out.println("> java -jar SyncService.jar filename"); 
                System.out.println("");
                System.out.println("> To specify a list of nodes to include use -I flag.");
                System.out.println("> Queries to the exlcuded nodes are simply placed in the failed queries.");
                System.out.println("> The names of the nodes must be correct else the program will crash");
                System.out.println("> java -jar SyncService.jar -I node1 node2 nodeN");
                System.out.println("> java -jar SyncService.jar  filename -I node1 node2 nodeN");
                System.exit(0);
            }
            
            int index=1;
            if (! args[0].equals("-I")){
                fileName = args[0];
                index=2;
            }
            if (args.length>1){
                if (args[0].equals("-I") || args[1].equals("-I")){
                    includeList=true;
                    for (int k=index;k<args.length;k++){
                        System.out.println("Node Included " + args[k]);
                        includeListNodes.add( args[k] );
                    }   
                }
            }
        }


        // Load and create connection with the local database
        Configuration configuration = new Configuration(cLog, fileName);
        cLog.setConfiguration(configuration);

        // Check if log directory exists, if not exit
        configuration.existsLogDirectory();

        if (configuration.isCheckPermissionsOnly())
        {
            System.out.println("Defined Log File is "+configuration.getLogfile());
            configuration.runVersion_check();
            if (checkVersion(build, configuration, cLog)) {
                System.out.println(">");
                System.out.println("> The current " + build.artifactId + " version is valid on all (online) nodes, continuing.");
            }
            else {
                    System.out.println(">");
                    System.out.println("> The current " + build.artifactId + " version is not valid on all nodes, exiting.");
                    System.exit(2);
            }

            System.out.println("All checks finished. Version, DataBase Accesses on (online) Remotes and Log File Access seem ok");
         
            
            System.out.println("\n\nAdditionaly Checking Triggers. If a node does not have the trigger an exception will be raised for that node."); 
            Verifications.checkTrigger(build, configuration, cLog);
            
            System.out.println("\n\nExiting Tool as CheckPermissionsOnly flag is true");
            
            System.exit(0);
        }

        System.out.println("> The " + build.artifactId + " is started");
        cLog.write("> The " + build.artifactId + " is started", Log.Type.forced);

        try {
            if (configuration.getDebug())
                System.out.println("> Working Directory = " + System.getProperty("user.dir"));
            cLog.write("Working Directory = " + System.getProperty("user.dir"), Log.Type.info);

            final HandleQueries handleQueries = new HandleQueries(configuration, cLog);
            
            HandleFailedQueries handleFailedQueries=null;
            if ( configuration.isProcessFailedQueries() ){
                 handleFailedQueries = new HandleFailedQueries(configuration, cLog);
            }
            else {
                //Important message always sent to standard output
                System.out.println("Not Handling Failed Queries");
            }

            if (includeList) {
                handleQueries.includeList=includeList;
                //handleQueries.includeListDest=includeListDest;
                handleQueries.includeListNodes=includeListNodes;
                
                handleFailedQueries.includeList=includeList;
                handleFailedQueries.includeListDest=includeListDest;

            }
            
            
            boolean runT1;
            boolean runT2;

            boolean continue_working = true;
            while (continue_working) {

                // Check version against the local and remote databases
                if (configuration.getDebug())
                    System.out.println("> Checking the minimum supported version of the synchronization system for the local and remote databases.");
                if (configuration.runVersion_check()){
                    if (checkVersion(build, configuration, cLog)) {
                        if (configuration.getDebug())
                            System.out.println("> The current " + build.artifactId + " version is valid on all nodes, continuing.");
                    }else {
                        if (configuration.getDebug()) System.out.println("> The current " + build.artifactId + " version is not valid on all nodes, exiting.");
                        cLog.write("The current " + build.artifactId + " version is not valid on all nodes, exiting.", Log.Type.error);
                        System.exit(2);
                    }
                }

                runT1 = Node.checkIfSync("T1", configuration, cLog);
                runT2 = Node.checkIfSync("T2", configuration, cLog);

                //search for failed queries
                if ( configuration.isProcessFailedQueries() ){
                    handleFailedQueries.readQueries();
                }

                if (runT2){ //search for new queries
                    handleQueries.readQueries("T2");
                }

                if (runT1){ //search for new queries
                    handleQueries.readQueries("T1");
                }

                //Checks if synchronization service should stop or not
                if (!configuration.isDaemon()) {
                    continue_working = false;
                } else {
                    Thread.sleep(configuration.getIteration_interval() * 1000);
                }
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Closes connection to local database
        configuration.close();

    }


    /*
       Check to detect if multiple sync services are running.
       If more than one SyncService is detected this process will exit.
    */
    static void checkRunningProcess() {
        try {
            System.out.println("> Operating System is " + OS  + ":  Linux = " + isLinux() + "  Windows = " + isWindows() );
            String line;
            Process p ;
            if (isWindows()) {
                p = Runtime.getRuntime().exec(System.getenv("windir") + "\\system32\\" + "tasklist.exe");
            } else {
                p=Runtime.getRuntime().exec("ps -ef");
            }
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            int cnt = 0;
            while ((line = input.readLine()) != null) {
                if (line.contains("java") && line.contains("SyncService")) {
                    System.out.println(line);
                    cnt++;
                }
            }
            input.close();
            if (cnt > 1) {
                System.out.println("> Failed to startup. Sync Service is already running (ps -ef) ");
                System.exit(2);
            }
        } catch (Exception err) {
            System.out.println("> Failed to startup. Something Went Wrong");
            err.printStackTrace();
            System.exit(2);
        }
    }


    public static boolean isWindows() {
        return (OS.contains("win"));
    }
    public static boolean isMac() {
        return (OS.contains("mac"));
    }
    public static boolean isUnix() {
        return (OS.contains("nix") || OS.contains("nux") || OS.contains("aix") );
    }
    public static boolean isLinux() {
        return (OS.contains("linux"));
    }
    public static boolean isSolaris() {
        return (OS.contains("sunos"));
    }
    
}
