import java.io.File;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.*;
import java.util.Enumeration;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * Created by rgcouto on 28/3/17.
 * Altered by P.Crocker 28/3/19 ReadFile accepts file name as input
 * 
 */
public class Configuration extends SharedMethods{


    // ###########################################################################
    // THIS IS STATIC INFORMATION THAT SHOULD BE UPDATED ACCORDINGLY
    // or use the SyncService.cfg file
    // ###########################################################################
    private String db_username  = "postgresepos";               // Username for the EPOS database
    private String db_password  = "epos1234";                   // Password for the EPOS database
    private String db_port      = "5432";                       // Port for the connection with EPOS database
    private String db_name      = "gnss-europe";        // Name of the EPOS database
    private String db_nodeurl   = "";                   // url of the glass node

    private boolean daemon              = false;       // Working mode of Sync Service
    private Integer iteration_interval  = 1;           // Iteration interval
    private boolean debug               = false;       // Output mode
    private String  logfile = "/home/vagrant/epos/logs/Epos_Sync_Log.txt";

    /*
        Optional Parameters
     */
    public Integer getNumberOfQueriesToProcess() {
        return NumberOfQueriesToProcess;
    }

    public void setNumberOfQueriesToProcess(Integer numberOfQueriesToProcess) {
        NumberOfQueriesToProcess = numberOfQueriesToProcess;
    }

    public boolean isProcessFailedQueries() {
        return ProcessFailedQueries;
    }

    public void setProcessFailedQueries(boolean processFailedQueries) {
        ProcessFailedQueries = processFailedQueries;
    }

    public Integer getNumberOfFailedQueriesToProcess() {
        return NumberOfFailedQueriesToProcess;
    }

    public void setNumberOfFailedQueriesToProcess(Integer numberOfFailedQueriesToProcess) {
        NumberOfFailedQueriesToProcess = numberOfFailedQueriesToProcess;
    }

    public boolean isCheckPermissionsOnly() {
        return checkPermissionsOnly;
    }

    public boolean isMaxdebug() {
        return maxdebug;
    }

    private Integer NumberOfQueriesToProcess = -1;
    private boolean   ProcessFailedQueries=true;
    private Integer NumberOfFailedQueriesToProcess=-1;
    private boolean maxdebug=false;

    
    private Connection c_localNode;     // Local node information
    private String myIP = null;         // This String will store the ip address of the machine
    private Enumeration nis = null;     // Used to get the IP address
    private boolean version_check       = true;

    private boolean checkPermissionsOnly = false;

    private Log cLog;
    /**
     * Creates new configuration
     */
    Configuration(Log cLog, String fileName) {
        this.cLog = cLog;

        System.out.println("> Reading database credentials from file");
        readFromFile(fileName);

        // Create connection with the local database
        try {
            Class.forName("org.postgresql.Driver");
            Properties props = new Properties();
            props.setProperty("user",this.getDb_nodeurl());
            props.setProperty("user",this.getDb_username());
            props.setProperty("password",this.getDb_password());
            props.setProperty("logUnclosedConnections","true");
            String connUrl="jdbc:postgresql://" + this.myIP+ ":" + this.getDb_port() + "/" + this.getDb_name();
            System.out.println("Connecting with localdatabase : " + connUrl);
            c_localNode = DriverManager.getConnection(connUrl, props);
            c_localNode.setAutoCommit(true);

            // Checks if currently logged user has permissions on database
            if (doesntHasPermissions(c_localNode, this.db_username, this, cLog)) {
                if (this.getDebug())
                    System.out.println(" > Exiting... Reason: The configured user doesn't Have permissions to access the database.");
                cLog.write("Exiting... Reason:  the configured user doesn't Have permissions to access the database.", Log.Type.error);
                System.exit(2);
            }
        } catch (Exception e) {
            if (this.getDebug())
                System.out.println(" > Exiting... Reason: Access to DB Failed.");
            cLog.write("Exiting... Reason: Access to DB Failed..", Log.Type.error);
            System.exit(2);
        }
    }


    /**
     * Creates new configuration
     * @param db_username database username
     * @param db_password database password
     * @param db_port database port
     * @param db_name database name
     * @param myIP local IP
     * @param version_check Check version against database
     * @param daemon daemon mode
     * @param iteration_interval seconds between db checks
     * @param debug output mode
     */
    public Configuration(String db_username, String db_password, String db_port, String db_name, String myIP, boolean version_check, boolean daemon, Integer iteration_interval, boolean debug) {
        this.db_username = db_username;
        this.db_password = db_password;
        this.db_port = db_port;
        this.db_name = db_name;
        this.myIP = myIP;
        this.daemon = daemon;
        this.iteration_interval = iteration_interval;
        this.debug = debug;
    }

    public void existsLogDirectory(){
        String logDirectory = this.getLogfile();

        // Check if the parsed log directory exists
        int pos;
        if ( Main.isWindows() )
            pos = logDirectory.lastIndexOf("/");
        else
            pos = logDirectory.lastIndexOf("/");
        if (pos == -1) return;
        logDirectory = logDirectory.substring(0, pos+1);
        System.out.println("Log Directory is ---<" + logDirectory+">---");

        if (!(new File(logDirectory).exists())){
            System.out.println("> The log file directory cannot be found ('" + logDirectory + "').");
            System.out.println("> Please, create the directory or change it on the configuration file.");
            System.exit(2);
        }
    }

    public String getDb_nodeurl() {
        return db_nodeurl;
    }

    public void setDb_nodeurl(String db_nodeurl) {
        this.db_nodeurl = db_nodeurl;
    }
    
    public String getLogfile() {
        return logfile;
    }

    public void setLogfile(String logfile) {
        this.logfile = logfile;
    }
    
    private String getDb_username() {
        return db_username;
    }

    private String getDb_password() {
        return db_password;
    }

    private String getDb_port() {
        return db_port;
    }

    private String getDb_name() {
        return db_name;
    }

    String getMyIP() {
        return myIP;
    }

    Connection getLocalConnection() {
        return c_localNode;
    }

    boolean isDaemon() {
        return daemon;
    }

    Integer getIteration_interval() {
        return iteration_interval;
    }

    boolean getDebug() {
        return debug;
    }

    public boolean runVersion_check() {
        return version_check;
    }

    /**
     * SETS MY IP ADDRESS
     * This method will find local IP address of machine
     */
    @SuppressWarnings("unused")
    private String findMyIP() {
        try {
            nis = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        while (nis.hasMoreElements()) {
            NetworkInterface ni = (NetworkInterface) nis.nextElement();
            Enumeration ias = ni.getInetAddresses();

            InetAddress ia;

            while (ias.hasMoreElements()) {
                ia = (InetAddress) ias.nextElement();
                if (ia.getHostAddress().contains("127.")) {
                    return ia.getHostAddress();
                }
            }
        }

        return "";
    }

    /**
     * READ DB CONFIGURATION FROM FILE
     * This method will read the database properties from a file
     */
    private void readFromFile(String fileName) {

        // Tries to read database credentials configuration file
        try (InputStream is = new FileInputStream(fileName)) {

            Properties prop = new Properties();

            // Loads configuration file
            prop.load(is);

            is.close();

            // Checks if configuration file data is OK
            if (    prop.getProperty("database_ip") != null &&
                    prop.getProperty("database_port") != null &&
                    prop.getProperty("database_name") != null &&
                    prop.getProperty("database_username") != null &&
                    prop.getProperty("database_password") != null &&
                    prop.getProperty("daemon") != null &&
                    prop.getProperty("interval") != null &&
                    prop.getProperty("version_check") != null &&
                    prop.getProperty("debug") != null &&
                    prop.getProperty("logfile") != null
                ) {

                this.db_name = prop.getProperty("database_name");
                this.myIP = prop.getProperty("database_ip");
                this.db_port =  prop.getProperty("database_port");
                this.db_username  = prop.getProperty("database_username");
                this.db_password = prop.getProperty("database_password");

                this.version_check = Boolean.parseBoolean(prop.getProperty("version_check"));
                this.daemon = Boolean.parseBoolean(prop.getProperty("daemon"));
                this.iteration_interval = Integer.parseInt(prop.getProperty("interval"));

                this.debug = Boolean.parseBoolean(prop.getProperty("debug"));
                
                this.logfile = prop.getProperty("logfile");

                //Add optional
                if (    prop.getProperty("NumberOfQueriesToProcess") != null ){
                    int v = Integer.parseInt (prop.getProperty("NumberOfQueriesToProcess"))  ;
                    if (v>0) {
                        this.NumberOfQueriesToProcess = v;
                        System.out.println("Debug Processing Mode : Queries to be processed in batches of " + this.NumberOfQueriesToProcess);
                    }
                }
                if (    prop.getProperty("NumberOfFailedQueriesToProcess") != null ){
                    int v = Integer.parseInt (prop.getProperty("NumberOfQueriesToProcess"))  ;;
                    if (v>0) {
                        this.NumberOfFailedQueriesToProcess = v;
                        System.out.println("Debug Processing Mode : Failed Queries to be loaded in batches of " + this.NumberOfFailedQueriesToProcess);
                    }
                }
                if (    prop.getProperty("ProcessFailedQueries") != null ){
                    this.ProcessFailedQueries = Boolean.parseBoolean (prop.getProperty("ProcessFailedQueries"))  ;
                    System.out.println("Debug Processing Mode : Failed Queries processing " + this.ProcessFailedQueries );
                }
                if (    prop.getProperty("maxdebug") != null ){
                    this.maxdebug = Boolean.parseBoolean (prop.getProperty("maxdebug"))  ;
                    System.out.println("Debug Processing Mode : maxdebug " + this.maxdebug );
                }
                if (    prop.getProperty("checkPermissionsOnly") != null ){
                    this.checkPermissionsOnly = Boolean.parseBoolean (prop.getProperty("checkPermissionsOnly"))  ;
                    System.out.println("Debug Processing Mode : checkPermissionsOnly " + this.checkPermissionsOnly );
                }

                //String DBConnectionString = "jdbc:postgresql://" + myIP + ":" + prop.getProperty("dbport") + "/" + prop.getProperty("database");
                //System.out.println(DBConnectionString);
            }
            else {
                System.out.println("FATAL ERROR: Can't Read form config file");
                System.exit(2);
            }

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(2);
        }
    }

    /**
     * Closes connection to local node
     */
    public void close() {
        try {
            this.c_localNode.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    protected void finalize()
    {
        close();
    }
}
