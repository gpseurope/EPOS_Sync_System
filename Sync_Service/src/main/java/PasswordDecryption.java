import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileInputStream;
import java.util.Base64;

/**
 * Created by José Manteigueiro on 22/01/2018
 * Last changed by José Manteigueiro on 22/02/2018
 */

public class PasswordDecryption {

    public static String decrypt(String encryptedIvTextBytes, Configuration conf, Log cLog) throws Exception {

        byte key[];
        String masterKeyPath = "./key.dat";
        File masterkey = new File(masterKeyPath);

        if(masterkey.exists() && !masterkey.isDirectory()) {
            FileInputStream fs = new FileInputStream(masterKeyPath);
            key = new byte[32]; // 256 bits are converted to 32 bytes;
            int check = fs.read(key);
        }
        else {
            if (conf.getDebug())
            System.out.println("No master key file found. You need to have the master key in this folder.");
            cLog.write("No master key file found. You need to have the master key in this folder.", Log.Type.decryption);
            return "Error.";
        }

        byte[] decoded = Base64.getDecoder().decode(encryptedIvTextBytes);

        int ivSize = 16;
        int keySize = 32;

        // Extract IV.
        byte[] iv = new byte[ivSize];
        System.arraycopy(decoded, 0, iv, 0, iv.length);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

        // Extract encrypted part.
        int encryptedSize = decoded.length - ivSize;
        byte[] encryptedBytes = new byte[encryptedSize];
        System.arraycopy(decoded, ivSize, encryptedBytes, 0, encryptedSize);

        // Hash key.
        byte[] keyBytes = new byte[keySize];
        /*
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(key);
        System.arraycopy(md.digest(), 0, keyBytes, 0, keyBytes.length);
        */
        System.arraycopy(key, 0, keyBytes, 0, keyBytes.length);
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");

        // Decrypt.
        Cipher cipherDecrypt = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipherDecrypt.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] decrypted = cipherDecrypt.doFinal(encryptedBytes);

        return new String(decrypted);
    }

}


