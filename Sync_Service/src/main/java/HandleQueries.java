import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * HANDLE QUERIES
 * This class is responsible for handling the TX queries on the EPOS Glass system.
 */
class HandleQueries extends SharedMethods {

    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private ArrayList<Query> queries;                   // List of all queries
    private HashMap<Node, ArrayList<Query>> nodes;      // List of all nodes
    private Node localhost;                             // Localhost node info
    private Configuration conf;                         // Configuration info
    private Connection c_localNode;                     // Local node configuration
    private Log cLog;                                   // Log control
    public boolean includeList = false;
    //public static ArrayList <Integer> includeListDest;
    public static ArrayList <String> includeListNodes;

    HandleQueries(Configuration conf, Log cLog) {
        this.conf = conf;
        this.cLog = cLog;
        this.c_localNode = conf.getLocalConnection();
        queries = new ArrayList<>();
        nodes = new HashMap<>();
    }

    /**
     * READ QUERIES
     * This function is responsible for handling the new TX queries on the EPOS Glass system.
     */
    void readQueries(String tier)
    {
        if (conf.getDebug())
            System.out.println("> Handle "+tier+"");
        cLog.write("> Handle "+tier+"", Log.Type.info);

        // Get info of the localhost node
        Statement s_localNode;
        ResultSet rs_localNode;
        try {

            s_localNode = c_localNode.createStatement();
            // Selects local node metadata from database
            rs_localNode = s_localNode.executeQuery("SELECT * FROM node WHERE host='" + conf.getMyIP() + "';");

            // Check if returns results
            if (rs_localNode.next()) {
                localhost = new Node(
                        rs_localNode.getInt("id"),
                        rs_localNode.getString("name"),
                        rs_localNode.getString("host"),
                        rs_localNode.getString("port"),
                        rs_localNode.getString("dbname"),
                        rs_localNode.getString("username"),
                        rs_localNode.getString("password"),
                        rs_localNode.getString("url"),
                        conf, cLog
                );
            } else {
                    //Important message always sent to standard output
                    System.out.println(" > ERROR - Local node credentials not present");
                    System.out.println(" > Exiting...");

                    cLog.write("Local node credentials not present", Log.Type.error);
                    System.exit(2);
            }

            s_localNode = c_localNode.createStatement();


            // Selects all queries from database
            // Except those that do not have a destination
            // The default action is to select all the queries - if not we shall select only a certain number of rows
            String query="";

            if (tier.equals("T1")){
                query = "SELECT * FROM queries WHERE metadata='T1' ";
               /* if (includeList){
                    String where=" AND ( destiny IN ( ";
                    for (int i=0;i<includeListDest.size();i++){
                        if (i!=0)
                            where = where +" , ";
                        where = where + includeListDest.get(i);
                    }
                    where = where + " ) OR destiny IS null ) ";
                    query = query + where ;
                }*/
                query = query + " ORDER BY id ASC;";
                System.out.println("> Handled Qstring " + query);
                //Scanner sc = new Scanner(System.in);
                //sc.next();
            }
            else {
                if (tier.equals("T2")) {
                    //query = "SELECT * FROM queries WHERE destiny IS NOT NULL AND metadata = 'T2' ORDER BY  id ASC";
                    query = "SELECT * FROM queries WHERE metadata = 'T2' ORDER BY  id ASC";
                    if (conf.getNumberOfQueriesToProcess() <= 0)
                        query = query + ";";
                    else {
                        String firstNrows = " FETCH FIRST " + conf.getNumberOfQueriesToProcess() + " ROW ONLY";
                        query = query + firstNrows + " ;";
                    }
                }
                else {
                    System.out.println("Unknown Tier Tx is being called ::"+tier);
                    System.exit(1);
                }
            }

            /*
                Count Queries
                "SELECT * FROM queries WHERE metadata = '"+ tier + "';"
            */
            Statement s = c_localNode.createStatement();
            ResultSet r = s.executeQuery("SELECT COUNT(*) AS rowcount FROM queries WHERE metadata = '"+ tier + "';");
            r.next();
            int qcount = r.getInt("rowcount") ;
            r.close() ;
            //System.out.println("Table queries has " + qcount + " row(s).");
            s.close();

            //System.out.println("Fetching Queries : query string is " + query);
            rs_localNode = s_localNode.executeQuery( query );

            // Check if there are any queries
            if (rs_localNode.next() )
            {
                //Count

                // Get all queries
                if (conf.getDebug())
                    System.out.println(" > Get queries: There are a Total of" + qcount +": Not all may have destinations.");
                cLog.write(" > Get queries. There are " + qcount, Log.Type.info);
                handleQueries( query );

                //al queries are now in an array list called queries

                // Find the destines for the queries
                //System.out.println("  > Find queries destination");
                //findQueryRemoteNodes(c_localNode);

                // Process queued queries
                if (conf.getDebug())
                    System.out.println(" > Process queries");
                cLog.write(" > Process queries", Log.Type.info);
                processQueries();

                // Kill lost child
//                for (Query query : queries) {
//                    System.out.println("  > Delete lost childs");
//                    deleteLostChild(query, c_localNode);
//                }

                // Clear data
                nodes.clear();
                queries.clear();

            } else {
                if (conf.getDebug())
                    System.out.println(" > No queries to update");
                cLog.write(" > No queries to update", Log.Type.info);
            }

            // Close connections
            rs_localNode.close();
            s_localNode.close();

        } catch (Exception e ) {
            //Important message always sent to standard output
            System.err.println( "Error on reading queries - " + e.getClass().getName()+": "+ e.getMessage() );
            cLog.write("Error on reading queries - " + e.getClass().getName()+": "+ e.getMessage(), Log.Type.error);
        }

    }

    /**
     * HANDLE QUERIES
     * This method will get any queries and will try to send the queries to the destination.
     */
    private void handleQueries( String query)
    {

        Statement s_localNode;
        ResultSet rs_localNode;

        // Get all queries on hold from the database.
        try {

            s_localNode = c_localNode.createStatement();
            // Selects all queries from database
            rs_localNode = s_localNode.executeQuery( query  );

            // Loops through all the queries.
            while (rs_localNode.next()) {

                // Store query info
                Query q = new Query(
                                    rs_localNode.getInt("id"),
                                    rs_localNode.getString("query"),
                                    rs_localNode.getString("metadata"),
                                    rs_localNode.getInt("station_id"),
                                    rs_localNode.getInt("destiny")
                                    );

                if (conf.isMaxdebug()) {
                    System.out.println("  > ID: " + q.id);
                }
                cLog.write("  > ID: " + q.id, Log.Type.info);

                // Find query remote nodes
                q = findQueryRemoteNodes(q);

                // Add query to list
                queries.add(q);

            }

            rs_localNode.close();
            s_localNode.close();

        } catch ( Exception e ) {
            if (conf.getDebug())
                System.err.println( "Error on sending queries to destiny - "+e.getClass().getName()+": "+ e.getMessage() );
            cLog.write("Error on sending queries to destiny - " + e.getClass().getName()+": "+ e.getMessage() , Log.Type.error);
        }
    }

    /**
     * FIND QUERY REMOTE NODES
     * This method will find the remote node(s) of the query.
     */
    @SuppressWarnings("ConstantConditions")
    private Query findQueryRemoteNodes(Query query)
    {
        Statement s_localNode = null;
        ResultSet rs_localNode = null;

        try {

            if (query.station == 0 && query.destiny.id == 0) { // Check if this is going to be sent to all nodes
                s_localNode = c_localNode.createStatement();
                //Selects destinations where to send data if no station is assigned
                rs_localNode = s_localNode.executeQuery("SELECT * FROM connections WHERE metadata='" + query.metadata + "' AND station IS NULL AND destiny IS NOT NULL AND source=" + localhost.id + ";");

                // Add all destinies to the query
                query = addRemoteNodes(rs_localNode, query);

            } else if (query.station != 0 && query.destiny.id == 0) { // Check if this is going to be sent to a specific node without specifying which
                s_localNode = c_localNode.createStatement();
                // Selects destinations where to send data if a station is assigned
                // Changed destiny is null to NOT NULL because in the new database destiny in table connections can't be NULL!
                rs_localNode = s_localNode.executeQuery("SELECT * FROM connections WHERE metadata='" + query.metadata + "' AND station='" + query.station + "' AND destiny IS NOT NULL AND source=" + localhost.id + ";");
                // Add all destinies to the query
                query = addRemoteNodes(rs_localNode, query);

            } else if (query.station != 0 && query.destiny.id != 0) { // Specific Node and Specific Station
                s_localNode = c_localNode.createStatement();
                //Selects destinations where to send data if a station is assigned
                rs_localNode = s_localNode.executeQuery("SELECT * FROM connections WHERE destiny = " + query.destiny.id + " AND metadata='" + query.metadata + "' AND station='" + query.station + "' AND source=" + localhost.id + ";");

                // Add all destinies to the query
                query = addRemoteNodes(rs_localNode, query);

            } else if (query.station == 0 && query.destiny.id != 0) { // Specific Node without assigned station
                s_localNode = c_localNode.createStatement();
                //Selects destinations where to send data if a station is assigned
                rs_localNode = s_localNode.executeQuery("SELECT * FROM connections WHERE destiny = " + query.destiny.id + " AND metadata='" + query.metadata + "' AND station IS NULL AND source=" + localhost.id + ";");
                // Add all destinies to the query
                query = addRemoteNodes(rs_localNode, query);
            }

            if (rs_localNode != null) {
                rs_localNode.close();
                s_localNode.close();
            }

        } catch (Exception e) {
            if (conf.getDebug())
                System.err.println("Error finding remote node(s) to execute queries - " + e.getClass().getName() + ": " + e.getMessage());
            cLog.write("Error finding remote node(s) to execute queries - " + e.getClass().getName() + ": " + e.getMessage(), Log.Type.error);
        }

        return query;

    }

    /**
     * Auxiliary method to findQueryRemoteNodes method
     *
     * @param rs_localNode local node resultSet
     * @param query query object
     * @throws SQLException SQLException
     */
    @SuppressWarnings("Duplicates")
    private Query addRemoteNodes(ResultSet rs_localNode, Query query) throws SQLException {

        while (rs_localNode.next()) {
            // Get and fill node information
            Node n = getRemoteNode(rs_localNode.getInt("destiny"), c_localNode, conf, cLog);
   
            // Adds destiny to query
            query.destinies.add(n);

            //Adds query to nodes list
            if (!nodes.containsKey(n)) {
                ArrayList<Query> queryArrayList = new ArrayList<>();
                queryArrayList.add(query);
                nodes.put(n, queryArrayList);
            } else {
                nodes.get(n).add(query);
            }
        }

        return query;
    }

    /**
     * PROCESS QUERIES IN REMOTE NODE
     * This method will process the queries in destination remote node.
     */
    private void processQueries()
    {
        Connection c_remoteNode = null;     // Connection to the remote EPOS database
        Query query;                        // Query to be executed in remote node
        int sucessful=0;
        int ignored=0;
        //Scanner in = new Scanner(System.in);

        //Loop through all nodes
        for (Map.Entry<Node, ArrayList<Query>> remoteNode : nodes.entrySet()) {

            if ( conf.getDebug() )
                System.out.println("  >   > processing "+remoteNode.getValue().size()+" queries for Remote Node: " + remoteNode.getKey().host);
            cLog.write("  >   > processing  "+remoteNode.getValue().size()+"queries for Node: " + remoteNode.getKey().host, Log.Type.info);

            //check include list
            if (includeList){
                String remoteNodeName = remoteNode.getKey().name;
                //if ( ! includeListNodes.contains( remoteNodeName ) ){
                if ( ! SharedMethods.containsIgnoreCase(includeListNodes,remoteNodeName) ){
                    System.out.println("IncludeList");
                      System.out.println("  > excluding node in processing queries "+remoteNodeName );
                      addsNodesAllQueriesToFailedQueries(remoteNode, 0, "Specific Exclusion");
                      continue;
                }
            }
            
            try {

                //Open database connection on remote node
                Class.forName("org.postgresql.Driver");
                c_remoteNode = DriverManager.getConnection("jdbc:postgresql://" + remoteNode.getKey().host + ":" + remoteNode.getKey().port + "/" + remoteNode.getKey().dbname, remoteNode.getKey().username, remoteNode.getKey().password);
                c_remoteNode.setAutoCommit(true);

                //Checks if user has database permissions
                if (doesntHasPermissions(c_remoteNode, remoteNode.getKey().username, conf, cLog)) {
                    //Adds to failed queries
                    addsNodesAllQueriesToFailedQueries(remoteNode, 0, permissionRefuseReason); //Variable permissionRefuseReason is inherited
                    continue;
                }

                // Loop through queries
                for (int i = 0; i < remoteNode.getValue().size(); i++) {

                    //Gets query
                    query = remoteNode.getValue().get(i);
                    if (conf.getDebug())
                        System.out.println("   > " + (i+1) + "/" + remoteNode.getValue().size() + " - Executing " + query.query);
                    cLog.write("   > " + (i+1) + "/" + remoteNode.getValue().size() + " - Executing " + query.query, Log.Type.info);

                    //System.out.printf("Ready to execute on Node " + remoteNode.getKey().host ); 
                    //String userChoice = in.nextLine();
                    try {

                        // Run the query
                        Statement s_remoteNode = c_remoteNode.createStatement();
                        int affectedrows =s_remoteNode.executeUpdate(query.query);
                        s_remoteNode.close();

                        if (0 == affectedrows)
                            if (conf.isMaxdebug()) {
                                System.out.println("No error on remote query but no row is affected");
                            }
                        sucessful++;

                    } catch (SQLException e) {

                        //Checks if connection is closed
                        if (c_remoteNode.isClosed()) {
                            if (conf.getDebug())
                                System.out.println("   > ERROR - " + remoteNode.getKey().host + " is offline");
                            cLog.write(remoteNode.getKey().host + " is offline", Log.Type.offline);
                            //Adds to failed queries
                            addsNodesAllQueriesToFailedQueries(remoteNode, i, "PSQL - Connection Lost");
                            break;
                        } else {
                            //If it already exists in destiny DB
                            if (e.getSQLState().equals("23505")) {
                                if (conf.getDebug())
                                    System.out.println("    > Already exists in destiny");
                                cLog.write("Already exists in destiny", Log.Type.info);
                                ignored++;
                            } else {
                                if (conf.getDebug()) {
                                    //Some other error has occurred
                                    System.err.println(e.getClass().getName() + ": " + e.getMessage());
                                    System.out.println("   > ERROR - " + remoteNode.getKey().host + " an error occurred");
                                }
                                cLog.write(e.getClass().getName() + ": " + e.getMessage(), Log.Type.error);
                                cLog.write(remoteNode.getKey().host + " an error occurred", Log.Type.error);

                                //Adds to failed queries
                                addToFailedQueries(query.query, remoteNode.getKey(), "PSQL - " + e.getSQLState()+" - "+e.getMessage());
                            }
                        }

                    }

                    //Delete from Queries if current remote node is equals to the last one for query
                    if (remoteNode.getKey().equals(query.destinies.get(query.destinies.size()-1)))
                        deleteQueryFromQueue(query);

                }

            } catch (SQLException e) {
                switch (e.getSQLState()) {
                    // If exception is related to inexisting user in remote database
                    case "28000":
                        if (conf.getDebug())
                        System.out.println("   > ERROR - " + remoteNode.getKey().username + " is not a valid user on " + remoteNode.getKey().host);
                        cLog.write(remoteNode.getKey().username + " is not a valid user on " + remoteNode.getKey().host, Log.Type.error);
                        // Adds current node queries to failed queries
                        addsNodesAllQueriesToFailedQueries(remoteNode, 0, "PSQL - Invalid Authorization Specification");
                        break;

                    // If exception is related to connection status with remote database
                    case "08001":
                        if (conf.getDebug())
                        System.out.println("   > ERROR - " + remoteNode.getKey().host + " is offline");
                        cLog.write(remoteNode.getKey().host + " is offline", Log.Type.offline);
                        // Adds current node queries to failed queries
                        addsNodesAllQueriesToFailedQueries(remoteNode, 0, "PSQL - Host is offline");
                        break;

                    //Any other SQL exception happened
                    default:
                        if (conf.getDebug()) {
                            System.err.println(e.getClass().getName() + ": " + e.getMessage());
                            System.out.println("   > ERROR - " + remoteNode.getKey().host + " an error occurred");
                        }
                        cLog.write(e.getClass().getName() + ": " + e.getMessage(), Log.Type.error);
                        cLog.write(remoteNode.getKey().host + " an error occurred", Log.Type.error);
                        // Adds current node queries to failed queries
                        addsNodesAllQueriesToFailedQueries(remoteNode, 0, "PSQL - " + e.getSQLState() + " - " + e.getMessage());
                        break;
                }
            } catch (ClassNotFoundException e) {
                // Problem with packaging of project
                if (conf.getDebug())
                    System.err.println(e.getClass().getName() + ": " + e.getMessage());
                cLog.write(e.getClass().getName() + ": " + e.getMessage(), Log.Type.error);
            } finally {
                //Closes remote connection
                try {
                    if (c_remoteNode != null && !c_remoteNode.isClosed()) {
                        c_remoteNode.close();

                        //Update remote GLASS-API cache
                        if (conf.getDebug())
                            System.out.println("   > Updating remote node GLASS-API Cache");
                        cLog.write("Updating remote node GLASS-API Cache", Log.Type.info);
                        updateRemoteGlassApiCache(remoteNode.getKey(), conf, cLog);

                    }
                } catch (Exception e) {
                    System.err.println("error on a finally in process queries");
                    e.printStackTrace();
                }
            }


        }

        System.out.println( " Number of queries successfuly processed " + sucessful+ " ignored = "+ignored);
        if (conf.getDebug() )
            cLog.write(" Number of queries successfuly processed " + sucessful+ " ignored = "+ignored  , Log.Type.info);

    }

    /**
     * ADDS QUERIES TO FAILED QUERIES
     * @param node remote node
     * @param i which position to start
     */
    private void addsNodesAllQueriesToFailedQueries(Map.Entry<Node, ArrayList<Query>> node, int i, String reason)
    {

        Query query; //Query to be added to failed queries

        //Loop through node queries
        for (int j = i; j < node.getValue().size(); j++) {
            //if (conf.getDebug())
            //    System.out.println("     > " + (j+1) + "/" + node.getValue().size() + " - Adding to Failed Queries");
            cLog.write((j+1) + "/" + node.getValue().size() + " - Adding to Failed Queries", Log.Type.failed);
            query  = node.getValue().get(j);
            addToFailedQueries(query.query, node.getKey(), reason);

            //Delete from Queries if current remote node is equals to the last one for query
            if (node.getKey().equals(query.destinies.get(query.destinies.size()-1)))
                deleteQueryFromQueue(query);
        }

    }

    /**
     * ADD TO FAILED QUERIES
     * This method will add a query to the failed queries queue
     */
    @SuppressWarnings("SqlResolve")
    private void addToFailedQueries(String query, Node remoteNode, String reason)
    {

        Statement s_local;

        try {
            if (conf.getDebug())
                System.out.println("    > Add query to failed queries queue - (" + reason + ")");
            cLog.write("Add query to failed queries queue - (" + reason + ")", Log.Type.failed);

            s_local = c_localNode.createStatement();
            query = query.replace("'", "''");
            s_local.executeUpdate("INSERT INTO failed_queries (query, destiny, reason) VALUES ('" + query + "', " + remoteNode.id + ", '"+ reason + "');");

            s_local.close();

        } catch ( Exception e ) {
            if (conf.getDebug())
                System.err.println( "Error1 on adding failed queries to queue - " + e.getClass().getName()+": "+ e.getMessage() );
            cLog.write("Error1 on adding failed queries to queue - " + e.getClass().getName()+": "+ e.getMessage(), Log.Type.error);
        }

    }

    /**
     * DELETE QUERY FROM QUEUE
     * This method will add a query to the failed queries queue
     */
    private void deleteQueryFromQueue(Query query)
    {
        Statement s_local; // Statement for queries on the EPOS database

        try {

            if (conf.getDebug())
                System.out.println("    > Delete query from queries queue...");
            cLog.write("Delete query from queries queue...", Log.Type.info);

            s_local = c_localNode.createStatement();
            s_local.executeUpdate("DELETE FROM queries WHERE id=" + query.id + ";");
            s_local.close();

        } catch ( Exception e ) {
            if (conf.getDebug())
                System.err.println( "Error2 on deleting failed queries from queue - " + e.getClass().getName()+": "+ e.getMessage() );
            cLog.write("Error2 on deleting failed queries from queue - " + e.getClass().getName()+": "+ e.getMessage(), Log.Type.error);
        }
    }

    /**
     * GET STATION CHAR CODE
     * This method will add the char code form the station
     */
    @SuppressWarnings("unused")
    private String getStationCharCode(int stationID)
    {

        Statement s_localNode;
        ResultSet rs_localNode;

        String stationMarker = "";						  // Station marker code

        try {

            if (conf.getDebug())
                System.out.println("Searching for Station char code...");
            cLog.write("Searching for Station char code...", Log.Type.info);

            s_localNode = c_localNode.createStatement();
            rs_localNode = s_localNode.executeQuery("SELECT marker FROM station WHERE id=" + stationID);

            // Find the marker code
            if (rs_localNode.next()) {
                // Fill node information
                stationMarker = rs_localNode.getString("marker");
            }

            rs_localNode.close();
            s_localNode.close();

            return stationMarker;

        } catch ( Exception e ) {
            if (conf.getDebug())
                System.err.println( "Error getting station char code - " + e.getClass().getName()+": "+ e.getMessage() );
            cLog.write("Error getting station char code - " + e.getClass().getName()+": "+ e.getMessage(), Log.Type.error);
            return "null";
        }
    }

    /**
     * DELETE LOST CHILD
     * This method will find and delete lost child queries.
     */
    @SuppressWarnings("unused")
    private void deleteLostChild(Query query)
    {
        // If the query is a lost child
        if (0 == query.destinies.size() )
        {
            // Kill the lost child
            deleteQueryFromQueue(query);
        }
    }

    
}
