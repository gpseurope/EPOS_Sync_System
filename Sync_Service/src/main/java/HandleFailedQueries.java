import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * INSERT FAIL
 * This class is responsible for handling the failed queries on the EPOS Glass system.
 */
class HandleFailedQueries extends SharedMethods {

    private HashMap<Node, ArrayList<Query>> nodes;      // List of all remote Nodes
    private Connection c_localNode;                     // Local node DB connection
    private Configuration conf;
    private Log cLog;
    public boolean includeList = false;
    public static ArrayList <Integer> includeListDest;

    HandleFailedQueries(Configuration conf, Log cLog) {
        this.conf = conf;
        this.cLog = cLog;
        this.c_localNode = conf.getLocalConnection();
    }

    /**
     * INSERT FAIL
     * This method will check if there are any failed queries and will try to send the queries to the proper
     * destination.
     */
    void readQueries() {
        if (conf.getDebug()) {
            System.out.println("> Handle failed queries");
        }
        cLog.write("> Handle failed queries", Log.Type.info);

        Statement s_localNode;
        ResultSet rs_localNode;

        try {

            s_localNode = c_localNode.createStatement();
            // Select all failed queries
            // The default action is to select all the queries - if not we shall select only a certain number
            String query = "SELECT * FROM failed_queries";
            if (conf.getNumberOfQueriesToProcess() <= 0)
               query=query+";" ;
            else{
                /* this is simple try to deal with failed queries in blocks
                    but it wont work properly
                    i.e if I ask to deal with 10 FQ's and they all FAIL again they will still be in the table the next time I run this

                 */
                //String firstNrows = " ORDER BY  id FETCH FIRST " + conf.getNumberOfQueriesToProcess() + " ROW ONLY";
                //query = query + firstNrows ;
                query=query+";" ;
            }
            //System.out.println("Fetching Failed Queries : query string is " + query);

            rs_localNode = s_localNode.executeQuery( query );

            // Check if there are any failed queries
            if (rs_localNode.next())
            {
                // Initialize remote rodes ArrayList
                nodes = new HashMap<>();

                // Get info from all remoteNodes
                if (conf.getDebug()) System.out.println(" > Getting all failed queries...");
                cLog.write(" > Getting all failed queries...", Log.Type.info);
                getAllRemoteNodesWithRespectiveQueries();

                // Handle failed queries
                if (conf.getDebug()) System.out.println(" > Process all failed queries...");
                cLog.write(" > Process all failed queries...", Log.Type.info);
                processFailedQueries();

            } else {
                if (conf.getDebug()) System.out.println(" > No failed queries");
                cLog.write(" > No failed queries", Log.Type.info);
            }

            // Close connections
            rs_localNode.close();
            s_localNode.close();

        } catch ( Exception e ) {
            if (conf.getDebug())
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            cLog.write(e.getClass().getName()+": "+ e.getMessage(), Log.Type.error);
        }
    }


    /**
     * GET ALL REMOTE NODES WITH RESPECTIVE QUERYES
     * This method will get the information from all possible remote nodes.
     */
    @SuppressWarnings("Duplicates")
    private void getAllRemoteNodesWithRespectiveQueries()
    {
        Statement s_localNode;
        ResultSet rs_localNode;

        // Clear nodes data
        nodes.clear();

        try {

            s_localNode = c_localNode.createStatement();
            // Selects all failed queries
            String queryStr = "SELECT * FROM failed_queries ";
            if (includeList) {
                    String where=" where destiny IN ( ";
                    for (int i=0;i<includeListDest.size();i++){
                        if (i!=0)
                            where = where +" , ";
                        where = where + includeListDest.get(i);
                    }
                    //note there should be no failed queries with destiny null
                    where = where + "  ) ";
                    queryStr = queryStr + where ;
            }

            queryStr = queryStr + " ORDER BY id ASC;";
            System.out.println("> Failed Qstring " + queryStr);
            rs_localNode = s_localNode.executeQuery( queryStr );

            // Loop through failed queries
            while (rs_localNode.next())
            {

                // Creates query object
                Query query = new Query();
                query.id = rs_localNode.getInt("id");
                query.query = rs_localNode.getString("query");

                if (conf.isMaxdebug()) {
                    System.out.print("FQ  > ID: " + query.id);
                }
                cLog.write("FQ  > ID: " + query.id, Log.Type.info);

                // Gets and fills node information
                Node remoteNode = getRemoteNode(rs_localNode.getInt("destiny"), c_localNode, conf, cLog);

                // Adds query to nodes list
                if (!nodes.containsKey(remoteNode)) {
                    ArrayList<Query> queryArrayList = new ArrayList<>();
                    queryArrayList.add(query);
                    nodes.put(remoteNode, queryArrayList);
                } else {
                    nodes.get(remoteNode).add(query);
                }

            }

            // Close connections
            rs_localNode.close();
            s_localNode.close();

        } catch ( Exception e ) {
            if (conf.getDebug())
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            cLog.write(e.getClass().getName()+": "+ e.getMessage(), Log.Type.error);
        }
    }

    /**
     * PROCESS FAILED QUERIES IN REMOTE NODE
     * This method will process the failed queries in destination remote node.
     */
    private void processFailedQueries()
    {
        Connection c_remoteNode = null;     // Connection to the remote EPOS database
        Query query;                        // Query to be executed in remote node

        //Loop through all nodes
        int queriesProcessedOk=0;

        for (Map.Entry<Node, ArrayList<Query>> remoteNode : nodes.entrySet()) {

            if (conf.getDebug()) System.out.println("  > Node: " + remoteNode.getKey().host);
            cLog.write("  > Node: " + remoteNode.getKey().host, Log.Type.info);
            try {

                //Open database connection on remote node
                Class.forName("org.postgresql.Driver");
                c_remoteNode = DriverManager.getConnection("jdbc:postgresql://" + remoteNode.getKey().host + ":" + remoteNode.getKey().port + "/" + remoteNode.getKey().dbname, remoteNode.getKey().username, remoteNode.getKey().password);
                c_remoteNode.setAutoCommit(true);

                //Checks if user has remote database permissions
                if (doesntHasPermissions(c_remoteNode, remoteNode.getKey().username, conf, cLog)) {
                    continue;
                }

                // Loop through failed queries
                for (int i = 0; i < remoteNode.getValue().size(); i++) {

                    //Gets failed query
                    query = remoteNode.getValue().get(i);

                    // Flag to check if failed query should be deleted based on success of query execution
                    boolean deleteQuery = true;

                    if (conf.getDebug())
                        System.out.println("   > " + (i+1) + "/" + remoteNode.getValue().size() + " Executing " + query.query);
                    cLog.write("   > " + (i+1) + "/" + remoteNode.getValue().size() + " Executing " + query.query, Log.Type.info);

                    try {
                        // Run failed query
                        Statement s_remoteNode = c_remoteNode.createStatement();
                        s_remoteNode.executeUpdate(query.query);
                        s_remoteNode.close();
                    } catch (SQLException e) {
                        //If it already exists in remote DB

                        if (e.getSQLState().equals("23505")) {
                            if (conf.getDebug()) System.out.println("    > Already exists in destiny");
                        } else {
                            //Any other error has occurred
                           //if (conf.getDebug()) System.out.println("    >  getSQLstate "+e.getSQLState());
                            if (conf.getDebug()) {
                                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                                System.out.println("    > " + remoteNode.getKey().host + " an error occurred");
                            }
                            cLog.write(e.getClass().getName() + ": " + e.getMessage(), Log.Type.error);

                            deleteQuery = false;

                            /*
                                    BUG
                                    In this case the delete query should be updated with the CURRENT (and possibly new
                                    reason for the failure
                                    i.e it was offline ... but now a new error has been detected !!

                             */
                        }
                    } finally {
                        if (deleteQuery) {
                            //Delete failed query from failed queries table
                            deleteFailedQueries(query);
                            queriesProcessedOk++;
                        }
                    } //end finally
                } //end loop thru failed queries

            } catch (SQLException e) {
                // Checks if exception is related to inexisting user in remote database
                if (e.getSQLState().equals("28000")) {
                    if (conf.getDebug())
                    System.out.println("   > ERROR - " + remoteNode.getKey().username + " is not a valid user on " + remoteNode.getKey().host);
                    cLog.write(remoteNode.getKey().username + " is not a valid user on " + remoteNode.getKey().host, Log.Type.error);
                    continue;
                }
                // Any other error has occurred
                if (conf.getDebug()) {
                    System.err.println(e.getClass().getName() + ": " + e.getMessage());
                    System.out.println("    > " + remoteNode.getKey().host + " is offline");
                }
                cLog.write(e.getClass().getName() + ": " + e.getMessage(), Log.Type.offline);
                cLog.write(remoteNode.getKey().host + " is offline", Log.Type.offline);
            } catch (ClassNotFoundException e) {
                // Problem with packaging of project
                e.printStackTrace();
            } finally {
                //Closes remote connection
                try {
                    if (c_remoteNode != null && !c_remoteNode.isClosed()) {
                        c_remoteNode.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            //
            //Update remote GLASS-API cache
            //moved this update cache to the end of the node failed query looop
            //instead of updateing after EACH     deleteFailedQueries(query);  call above
            updateRemoteGlassApiCache(remoteNode.getKey(), conf, cLog);


        }  //end for for Map.Entry<Node i.e the node loop



        System.out.println("We processed successfully " + queriesProcessedOk +" failed queries");

    }


    /**
     * DELETE FAILED QUERIES
     * This method deletes the failed queries.
     */
    private void deleteFailedQueries(Query query)
    {
        Statement s_local;

        try {
            if (conf.getDebug())
            System.out.println("    > Delete query from failed queries queue...");
            cLog.write("Delete query from failed queries queue...", Log.Type.info);

            s_local = c_localNode.createStatement();
            s_local.executeUpdate("DELETE FROM failed_queries WHERE id = " + query.id + ";");
            query.query = query.query.replace("'", "''");
            String q = "DELETE FROM queries WHERE query LIKE '" + query.query+"'";
            s_local.executeUpdate(q);
            s_local.close();

        } catch ( Exception e ) {
            if (conf.getDebug())
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            cLog.write("Error on deleting failed queries - " + e.getClass().getName()+": "+ e.getMessage(), Log.Type.error);
        }
    }

}
