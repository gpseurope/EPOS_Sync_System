import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by rgcouto on 8/2/18.
 */
class SharedMethods {


    static String permissionRefuseReason; // Stores permission refuse reason when asserts permissions on node database

    /**
     * GET REMOTE NODE DATA
     * This method returns a node with remote node data.
     */
    Node getRemoteNode(int remoteNodeID, Connection c_localNode, Configuration conf, Log cLog)
    {

        Statement s_localNode;
        ResultSet rs_localNode;
        Node remoteNode = null;

        try {

            s_localNode = c_localNode.createStatement();
            // Selects remote node information
            rs_localNode = s_localNode.executeQuery( "SELECT * FROM node WHERE id=" + remoteNodeID + ";" );

            // Assigns remote node information to Node object
            if (rs_localNode.next())
            {
                remoteNode = new Node(rs_localNode.getInt("id"),
                        rs_localNode.getString("name"),
                        rs_localNode.getString("host"),
                        rs_localNode.getString("port"),
                        rs_localNode.getString("dbname"),
                        rs_localNode.getString("username"),
                        rs_localNode.getString("password"),
                        rs_localNode.getString("url"),
                        conf,
                        cLog);

               // if (conf.getDebug()) System.out.println("   > " + remoteNode.host);
               // cLog.write(remoteNode.host, Log.Type.info);

            }

            // Close connections
            rs_localNode.close();
            s_localNode.close();

            return remoteNode;

        } catch ( Exception e ) {
            //Important Message always printed
            System.err.println( "Error on getting node information with id " + remoteNodeID + " - " + e.getClass().getName()+": "+ e.getMessage() );
            cLog.write("Error on getting node information with id " + remoteNodeID + " - " + e.getClass().getName()+": "+ e.getMessage(), Log.Type.error);
            return null;
        }
    }

    /**
     * CHECKS IF SPECIFIED USER HAS PERMISSIONS ON NODE DATABASE
     * @return true if user has permissions, false otherwise
     */
    static boolean doesntHasPermissions(Connection c_node, String username, Configuration conf, Log cLog) {

        int existing_tables = 0;       // Number of tables found in current schema
        Statement s_node;
        ResultSet rs_node;

        try {

            s_node = c_node.createStatement();
            // Selects amount of tables in current schema
            rs_node = s_node.executeQuery("SELECT COUNT(*) AS n_tables FROM information_schema.tables WHERE table_schema = 'public';");

            // Gets number of tables as result of the executed query
            if (rs_node.next())
                existing_tables = rs_node.getInt("n_tables");

            // If DB is empty return error
            if (existing_tables == 0) {

                if (conf.getDebug())
                System.out.println(" > ERROR - Node doesn't has any tables or user isn't assigned to database!");
                cLog.write("Node doesn't has any tables or user isn't assigned to database!", Log.Type.error);

                permissionRefuseReason = "PSQL - Database w/out Relations or Missing Assignment User->Database";
                return true;

            } else { // Check for amount of tables with permissions

                int allowed_tables = 0;     // Number of tables with required permissions

                s_node = c_node.createStatement();
                // Selects from pg_catalog the amount tables where specified user has CRUD and Trigger permissions
                rs_node = s_node.executeQuery(    "SELECT COUNT(schemaname||'.'||tablename) AS allowed_tables " +
                        "FROM pg_tables " +
                        "WHERE has_table_privilege ('"+username+"', schemaname||'.'||tablename, 'select') " +
                        " AND has_table_privilege ('"+username+"', schemaname||'.'||tablename, 'insert') " +
                        " AND has_table_privilege ('"+username+"', schemaname||'.'||tablename, 'delete')" +
                        " AND has_table_privilege ('"+username+"', schemaname||'.'||tablename, 'update') " +
                        " AND has_table_privilege ('"+username+"', schemaname||'.'||tablename, 'trigger') " +
                        " AND schemaname NOT IN ('pg_catalog', 'information_schema');"
                );

                // Gets number of allowed tables as a result of the executed query
                if (rs_node.next())
                    allowed_tables = rs_node.getInt("allowed_tables");

                // Returns the result of comparision between allowed and existing tables
                if (allowed_tables != existing_tables) {
                    //Important Message always output
                    System.out.println("    > ERROR - Remote node user without sufficient permissions!");
                    cLog.write("Remote node user without sufficient permissions!", Log.Type.error);
                    permissionRefuseReason = "PSQL - Invalid User Permissions";
                    return true;
                } else {
                    permissionRefuseReason = "OK";
                    return false;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }
    
    public static String removeLastChar(String s) {
    return (s == null || s.length() == 0)
      ? null 
      : (s.substring(0, s.length() - 1));
    }

    /**
     * UPDATES REMOTE GLASS-API CACHE
     * @param node remote node to be updated
     */
    
    void updateRemoteGlassApiCache(Node node, Configuration conf, Log cLog) {

        String url = node.nodeurl.trim();
        if ( url!=null && url.length()>0 && url.charAt(url.length()-1)=='/' )
            url= removeLastChar( url );
        if (node.nodeurl.contains("GlassFramework"))
            url = url + "/webresources/stations/stations-json-dictionary/all";
        else
            url = url + "/GlassFramework/webresources/stations/stations-json-dictionary/all";

        URL obj = null;
        try {
            obj = new URL(url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            con.setConnectTimeout(5000);

            //add request header
            con.setRequestProperty("User-Agent", "Mozilla/5.0");

            int responseCode = con.getResponseCode();
            if (conf.getDebug())
            System.out.println("    > Response Code: " + responseCode);
            cLog.write("Response Code: " + responseCode, Log.Type.info);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            if (conf.getDebug()) System.out.println("    > Response String: "+response.toString());
            cLog.write("Response String: "+response.toString(), Log.Type.info);

        } catch (IOException e) {
            if (conf.getDebug()) 
                System.out.println("     > ERROR - Couldn't perform request Glass API cache update " + url);
            cLog.write("Error on updating remote Glass API cache - Couldn't perform request", Log.Type.error);
			cLog.write("URL is " +url , Log.Type.error);
        }

    }

     public static boolean containsIgnoreCase(ArrayList<String> list, String soughtFor) {
        for (String current : list) {
            if (current.equalsIgnoreCase(soughtFor)) {
                return true;
            }
        }
        return false;
    }
}
