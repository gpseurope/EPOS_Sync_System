import java.util.ArrayList;

/**
 * QUERY
 * This class will store the data from a query
 */
public class Query {
    int id;                                  // ID of the query
    String query;                            // Instruction of the query
    String metadata;                         // Type of metadata of the query
    int station;                             // ID of the station
    ArrayList<Node> destinies;               // ArrayList with query destinies
    Node destiny;                            // ArrayList with destiny

    /**
     * QUERY CONSTRUCTOR 1
     * This is the class constructor.
     */
    @SuppressWarnings("unused")
    public Query()
    {
        id = 0;
        query = "";
        metadata = "";
        station = 0;
        destinies = new ArrayList<>();
        destiny = new Node();
    }

    /**
     * QUERY CONSTRUCTOR 2
     * This is the class constructor.
     */
    Query(int id, String query, String metadata, int station, int destiny)
    {
        this.id = id;
        this.query = query;
        this.metadata = metadata;
        this.station = station;
        this.destinies = new ArrayList<>();
        this.destiny = new Node();
        this.destiny.id = destiny;
    }
}