import java.sql.*;
/**
 * NODE
 * This class will store the data from a node
 */
public class Node{

    int id;                                          // ID of the node
    @SuppressWarnings("WeakerAccess")
    String name;                                     // Name do the node
    String host;                                     // IP of the node
    String port;                                     // Port used on the node
    String dbname;                                   // Database name
    String username;                                 // Database username
    String password;                                 // Password username
    String nodeurl;                                  // url for glass frameowrk
    private Configuration conf;
    private Log cLog;

    /**
     * NODE CONSTRUCTOR 1
     * This is the class constructor.
     */
    Node()
    {
        id = 0;
        name = "";
        host = "";
        port = "";
        dbname = "";
        username = "";
        password = "";
        nodeurl = "";
    }

    /**
     * Check if the current node is online
     */
    public boolean isOnline(){
        Connection connection;
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://" + this.host + ":" + this.port + "/" + this.dbname + "?loggerLevel=OFF", this.username, this.password);
            connection.setAutoCommit(true);
        }catch (SQLException e){
            switch (e.getSQLState()) {
                case "08001": // Node offline
                    if (conf.getDebug())
                        System.out.println("> Version check in node (" + this.name + ") " + this.host + ": Node offline");
                    cLog.write(" Version check in node (" + this.name + ") " + this.host + ": Node offline", Log.Type.offline);
                    return (false);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return(false);
        }
        return(true);
    }

    /**
     * NODE CONSTRUCTOR 2
     * This is the class constructor.
     */
    @SuppressWarnings("WeakerAccess")
    public Node(int id, 
                String name, String host, String port, String dbname, 
                String username, String password, 
                String nodeurl, Configuration conf, Log cLog)
    {
        this.id = id;
        this.name = name;
        this.host = host;
        this.port = port;
        this.dbname = dbname;
        this.username = username;
        this.nodeurl = nodeurl;
        this.conf = conf;
        this.cLog = cLog;

        if(Main.passwordIsEncrypted){
            try {
                this.password = PasswordDecryption.decrypt(password, conf, cLog);
            } catch (Exception e) {
                e.printStackTrace();
                if (conf.getDebug())
                System.out.println("Error occurred while decrypting the password. Check if key.dat is present.");
                cLog.write("Error occurred while decrypting the password. Check if key.dat is present.", Log.Type.decryption);
                System.exit(1);
            }
        }else {
            this.password = password;
        }


    }

    /**
     * PRINT NODE
     * This method will print the node info.
     */
    @SuppressWarnings("unused")
    public void printNode()
    {
        if (conf.getDebug()) {
            System.out.println("----------------------------------");
            System.out.println("ID: " + id);
            System.out.println("Name: " + name);
            System.out.println("Host: " + host);
            System.out.println("Port: " + port);
            System.out.println("DBname: " + dbname);
            System.out.println("Username: " + username);
            System.out.println("Password: " + password);
            System.out.println("Glass Node Url: " + nodeurl);
            System.out.println("----------------------------------");
        }
    }

    /**
     * CHECK IF NODE IS SYNCING TO T1, TO T2 OR BOTH
     * @param tier metadata value
     * @return true if syncing to passed by param tier
     */
    static boolean checkIfSync(String tier, Configuration conf, Log cLog)
    {
        if (conf.getDebug())
        System.out.println("> Check if synchronizing "+tier+" metadata");
        cLog.write("Check if synchronizing "+tier+" metadata", Log.Type.info);

        // Get info of the localhost node
        Connection c_localNode = conf.getLocalConnection();
        Statement s_localNode;
        ResultSet rs_localNode;

        try {

            // get node ID
            String queryString = "SELECT id FROM node WHERE host ='" + conf.getMyIP() + "';";
            s_localNode = c_localNode.createStatement();
            rs_localNode = s_localNode.executeQuery( queryString );

            int nodeID;

            if(rs_localNode.next()) {
                nodeID = rs_localNode.getInt("id");
            } else {
                if (conf.getDebug())
                System.out.println("ERROR  > Local node ("+conf.getMyIP() +") is not configured correctly:  This failed " +queryString);
                cLog.write("Local node ("+conf.getMyIP() +") is not configured correctly:  This failed " +queryString , Log.Type.error);
                c_localNode.close();
                System.exit(2);
                return false;

            }
            
            // get the connections of current node for specific tier
            s_localNode = c_localNode.createStatement();
            rs_localNode = s_localNode.executeQuery( "SELECT id FROM connections WHERE source ='" + nodeID + "' AND metadata = '"+ tier +"';" );

            return rs_localNode.next();

        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(2);
        }


        return false;
    }

    @Override
    public boolean equals(Object other){
        if (other instanceof Node) {
            Node n = (Node) other;
            return (    this.id == n.id &&
                        this.name.equals(n.name) &&
                        this.host.equals(n.host) &&
                        this.port.equals(n.port) &&
                        this.dbname.equals(n.dbname) &&
                        this.username.equals(n.username) &&
                        this.password.equals(n.password)
                    );
        }
        return false;
    }

    @Override
    public int hashCode()
    {
        return name.hashCode();
    }


}