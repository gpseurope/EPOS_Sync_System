--
-- Fixes small issues with DB gnss-europe v0.2.12
-- Created on 22. January 2018
-- Last change 9. February 2018
--

-- FIX QUERIES, NODES AND CONNECTIONS ID SERIALIZATION AND MANDATORY ISSUES

	-- Changes queries.id to be serial
	ALTER TABLE queries DROP COLUMN id;
	ALTER TABLE queries ADD COLUMN id SERIAL PRIMARY KEY;
	-- Changes queries.station_id to be nullabe
	ALTER TABLE queries ALTER COLUMN station_id DROP NOT NULL ;
	-- Adds queries.destiny for scynchronization purposes
	ALTER TABLE queries ADD COLUMN destiny int;

	-- Changes node.id to be serial
	ALTER TABLE node DROP COLUMN id CASCADE;
	ALTER TABLE node ADD COLUMN id SERIAL PRIMARY KEY; 
	-- Changes node.password to have 45 characters (mandatory for encrypted password)
	ALTER TABLE node ALTER COLUMN password TYPE VARCHAR(45);

	-- Changes connections.id to be serial
	ALTER TABLE connections DROP COLUMN id;
	ALTER TABLE connections ADD COLUMN id SERIAL PRIMARY KEY; 
	-- Changes connections.station to be nullable
	ALTER TABLE connections ALTER COLUMN station DROP NOT NULL ;

	-- Changes failed_queries.id to be serial
	ALTER TABLE failed_queries DROP COLUMN id;
	ALTER TABLE failed_queries ADD COLUMN id SERIAL PRIMARY KEY;
	-- Adds failed_queries.timestamp for monitoring purposes
	ALTER TABLE failed_queries ADD COLUMN timestamp timestamp DEFAULT NOW();
	-- Adds failed_queries.comments to store failed query reason for monitoring purposes
	ALTER TABLE failed_queries ADD COLUMN reason TEXT NOT NULL;

	-- Changes datacenter_station.id to be serial
	ALTER TABLE datacenter_station DROP COLUMN id;
	ALTER TABLE datacenter_station ADD COLUMN id SERIAL PRIMARY KEY;

	-- Changes rinex_errors.id to be serial
	ALTER TABLE rinex_errors DROP COLUMN id;
	ALTER TABLE rinex_errors ADD COLUMN id SERIAL PRIMARY KEY;

	-- Changes rinex_error_types.id to be serial
	ALTER TABLE rinex_error_types DROP COLUMN id CASCADE;
	ALTER TABLE rinex_error_types ADD COLUMN id SERIAL PRIMARY KEY;	


-- UPDATES ID SEQUENCES 
	
	-- Function to update ID Sequences
	CREATE OR REPLACE FUNCTION updateIDSequences()
	RETURNS VOID
	AS $$
	DECLARE
	    my_row    RECORD;
		aux 	text;
	    aux_tablename text;
	BEGIN       
	    FOR my_row IN 
	        SELECT table_name
	        FROM   information_schema.tables
	        WHERE  table_schema = 'public'
	        ORDER BY table_name ASC
	    LOOP
	        RAISE NOTICE 'Updating sequence ID on %', my_row.table_name;
			aux := my_row.table_name || '_id_seq';
			aux_tablename := my_row.table_name;
			if exists (select * from information_schema.sequences where information_schema.sequences.sequence_name = aux)
			then
				execute format('SELECT setval(%L, COALESCE((SELECT MAX(id)+1 from %I), 1), false)', aux, aux_tablename);
			else
				raise notice '  Could not run on %', aux;
			end if;
	    END LOOP;
	END;
	$$ LANGUAGE plpgsql;

	-- Calls previously created function updateIDSequences
	SELECT updateIDSequences();